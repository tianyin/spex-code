/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Tianyin Xu,                                                  */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#ifndef CONTROLDEPAGGREGATOR_H_
#define CONTROLDEPAGGREGATOR_H_

#include <map>
#include "Utils.h"
#include "Common.h"
#include "Container.h"
#include "MyCallGraph.h"

using namespace std;

/**
 *******************************************************************************************************
 ** This piece of comments decides how we identify the control dependency patterns
 *******************************************************************************************************
 * This structure describes the dependency informaion in a single function:
 *
 * declare foo() {
 *     if(A = 5) {
 *     		use B;
 *     		call bar();
 *     		call foobar();
 *     }
 * }
 *
 * In the above example, the value in this structure is as follows:
 *
 * PerBlockControlDepInfo {
 *     domingReference = 1
 *     domingFunction = foo()
 *
 *     dominatedFunctionCalls = { bar(), foobar()}
 *     dominatedParameters = { B }
 * }
 *
 * ===== [NOTE] =====
 * Such dependency might be "fake dependency" (that's why we first record them
 * towards furture analysis), for example,
 *
 * if we have the following code block:
 *
 * if(A) {
 *     use B
 * } else {
 *     use B
 * }
 *
 * then we have both A->B && !A->B, which means B does not rely on A
 *
 ********************************************************************************************************
 */
struct PerBlockControlDepInfo {

	~PerBlockControlDepInfo() {
		domedCallees.clear();
		domedParameters.clear();
	}

	void print();

	set<BasicBlock*> domingBB;
	set<Function*> domedCallees;
	set<string>    domedParameters;
};

class ControlDepAggregator {

public:

	void add2List(PerBlockControlDepInfo* pbdi) {
		if(pbdi) {
//			domingFunctions.insert(pbdi->domingFunctions);
			copySet(&domingBB,     &(pbdi->domingBB));
			copySet(&domedCallees, &(pbdi->domedCallees));
			copySet(&domedParameters, &(pbdi->domedParameters));
		}
	}

	void checkConflicts(PerBlockControlDepInfo* pbdi_t, PerBlockControlDepInfo* pbdi_f);

	void getRealDepParams(
			ConfUsageInfo* confUsages,
			Func2ConfUsageMap* f2cMap,
			MiniCallGraph* mcg,
			set<string>* results);

	bool isSelfContained(string param,
			set<Function*>* fset,
			set<Function*>* coverageSet, set<BasicBlock*>* bbSet,
			Func2ConfUsageMap* f2cMap, MiniCallGraph* ccg);

	bool isRecursiveUsageContained(string param,
			Function* F,
			set<Function*>* coverageSet, set<BasicBlock*>* bbSet,
			Func2ConfUsageMap* f2cMap, MiniCallGraph* ccg,
			set<Function*>* cache);

//	void print();

private:
//	void preprocess();

	set<BasicBlock*> domingBB;
	set<Function*> domedCallees;
	set<string>    domedParameters;

	//these are parameters we will definitely exclude
	set<string> mustNotParams;
};

#endif /* CONTROLDEPAGGREGATOR_H_ */
