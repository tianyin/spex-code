/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the container structures and store the configurations   */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#ifndef CONTAINER_H_
#define CONTAINER_H_

#include <sys/stat.h>
#include <list>
#include <string>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <iostream>

#include "llvm/Instructions.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Constants.h"
#include "llvm/Support/CFG.h"

#include "Utils.h"
#include "Common.h"

using namespace std;
using namespace llvm;

struct ConfnameGvarPair {

	ConfnameGvarPair(string cn, string gv) {
		confName = cn;
		gvarName = gv;
	}

	bool isEqual(ConfnameGvarPair* cgp) {
		if(cgp->confName.compare(confName) == 0
			&& cgp->gvarName.compare(gvarName) == 0) {
			return true;
		} else {
			return false;
		}
	}

	void println() {
		errs() << confName << " " << gvarName << "\n";
	}

	string confName;
	string gvarName;
};

struct ConfnameGvarPairs {
	list<ConfnameGvarPair*> confGvPairs;

	bool isExist(ConfnameGvarPair* cgp) {
		list<ConfnameGvarPair*>::iterator iter = confGvPairs.begin(), end = confGvPairs.end();
		for(; iter != end; iter++) {
			if((*iter)->isEqual(cgp)) {
				return true;
			}
		}
		return false;
	}

	void add(ConfnameGvarPair* cgp) {
		if(!isExist(cgp)) {
			confGvPairs.push_back(cgp);
		}
	}

	list<ConfnameGvarPair*>::iterator begin() {
		return confGvPairs.begin();
	}

	list<ConfnameGvarPair*>::iterator end() {
		return confGvPairs.end();
	}

	size_t size() {
		return confGvPairs.size();
	}

	//Reading global variable file to make the pair
	int readGlobalVariables(StringRef fileName) {
		struct stat buffer;

		if(stat(fileName.str().c_str(), &buffer)) {
			cout << "global variable file " << fileName.str() << " does not exist.\n";
			return -1;
		}

		//cout<<"reading file "<<fileName.str()<<endl;
		ifstream infile(fileName.str().c_str());
		string line;
		string entryName, confVar;

		while(getline(infile, line)) {
			istringstream iss(line);
			iss>>entryName>>confVar;
			ConfnameGvarPair* tmp = new ConfnameGvarPair(entryName, confVar);
			confGvPairs.push_back(tmp);
		}
		return 0;
	}
};

struct FuncArgPair {

private:
	string _func;
	unsigned _argPos;

public:
	bool _checkReturn;
	bool _goInside;
//	bool _isOrigin;

	FuncArgPair(string func, unsigned argPos, bool cr, bool gi):
		_func(func),
		_argPos(argPos),
		_checkReturn(cr),
		_goInside(gi) {}

	string getFuncName() {
		return _func;
	}

	unsigned getArgPosition() {
		return _argPos;
	}

	bool FACompare(string f, unsigned p) {
		if (_func.compare(f) == 0 &&
				_argPos == p) {
			return true;
		}
		return false;
	}

	bool FACompare(const FuncArgPair& rhs) {
		if (_func.compare(rhs._func) == 0 &&
				_argPos == rhs._argPos) {
			return true;
		}
		return false;
	}

//	bool operator == (const FuncArgPair& rhs){
//		if (_func.compare(rhs._func) == 0 &&
//				_argPos == rhs._argPos){
//			return true;
//		}
//		return false;
//	}

	void print() {
		errs() << "[_func: " << _func << ", _arg: " << _argPos << "]\n";
	}

	void printAll() {
		errs() << "[_func: " << _func << ", _arg: " << _argPos << "], checkReturn="
			   << _checkReturn << ", goInside=" << _goInside << "\n";
	}
};

/**
 * There are two cases:
 * 1. The configuration variable corresponds to a field of a structure with a basic type, e.g.,
 *    In lighttpd, the "server.max-worker" directive corresponds to "unsigned short max_worker" in the "server_config" structure
 *    In this case, the <_sName, _field> pair can describe it accurately.
 *
 * 2. The field the configuration variable corresponds to is not a basic type but a struct (or class?), e.g.,
 *    In lighttpd, the "server.pid-file" directive corresponds to "buffer *pid_file" in server_config,
 *    the buffer is a common structure defined in the code to represent a string
 *    	typedef struct {
 *     		char *ptr;
 *
 *        	size_t used;
 *         	size_t size;
 * 		} buffer;
 * 	  in this case, we are only interested in "char *ptr", this field can be defined by _sfield
**/
struct StructFieldPairs {

private:
	list<string> _structs;
	list<unsigned> _fields;

public:
	StructFieldPairs() {}

	StructFieldPairs(string sName, unsigned field) {
		_structs.push_front(sName);
		_fields.push_front(field);
	}

	StructFieldPairs(list<string>* sName, list<unsigned>* fields) {
//		_fields.merge(*fields);
//		_structs.merge(*sName);

		copyList(&_structs, sName);
		copyList(&_fields, fields);
	}

	StructFieldPairs(StructFieldPairs* sfp) {
		if(sfp != NULL) {
//			_fields.merge(*(sfp->getFields()));
//			_structs.merge(*(sfp->getStructs()));

			copyList(&_structs, sfp->getStructs());
			copyList(&_fields,  sfp->getFields());
		}
	}

	StructFieldPairs(StructFieldPairs* sfp, unsigned index) {
		if(sfp != NULL) {
			copyList(&_structs, sfp->getStructs(), index);
			copyList(&_fields,  sfp->getFields(), index);
		}
	}

	~StructFieldPairs() {
		_structs.clear();
		_fields.clear();
	}

	list<string>* getStructs() {
		return &_structs;
	}

	list<unsigned>* getFields() {
		return &_fields;
	}

	string getLastStructName() {
		list<string>::reverse_iterator rit = _structs.rbegin();
		return *rit;
	}

	string getFirstStructName() {
		list<string>::iterator iter = _structs.begin();
		return *iter;
	}

	string popFirstStructName() {
		string fsn = _structs.front();
		_structs.pop_front();
		return fsn;
	}

	void pushBack(StructFieldPairs* sfp) {
		list<string>* stlist = sfp->getStructs();
		list<unsigned>* idxlist = sfp->getFields();

		copyList(&_structs, stlist);
		copyList(&_fields,  idxlist);
	}

	unsigned popFirstField() {
		unsigned idx = _fields.front();
		_fields.pop_front();
		return idx;
	}

	unsigned getFirstField() {
		list<unsigned>::iterator iter = _fields.begin();
		return *iter;
	}

	unsigned getLastField() {
		list<unsigned>::reverse_iterator rit = _fields.rbegin();
		return *rit;
	}

	string getStructName(unsigned idx) {
//		if(_structs.size() == 0)
//			return "";

		errs() << "***" << idx << "\n";
		errs() << "***" << _structs.size() << "\n";

		assert(idx < _structs.size());

		list<string>::iterator iter = _structs.begin();
		for(unsigned i=0; i<idx; i++) {
			iter++;
		}

		return *iter;
	}

	unsigned getField(unsigned idx) {
//		if(_structs.size() == 0)
//			return -1;

		assert(idx < _fields.size());

		list<unsigned>::iterator iter = _fields.begin();
		for(unsigned i=0; i<idx; i++) {
			iter++;
		}

		return *iter;
	}

	/*
	 * Check if there exists a match of the current <st, field> pair in the list
	 * Return 1: both the struct and the field match! Perfect match
	 *        2: the struct is found, but the field cannot match
	 *        3: even the struct name cannot match
	 */
	int exist(string st, unsigned field);

	void update(StructFieldPairs* sfpair);

	bool anywhereMatch(StructFieldPairs* sfpair);

	bool prefixMatch(StructFieldPairs* sfpair);

	void test();

//	bool backMatch(StructFieldPairs* sfpair);

	bool isEmpty() {
		assert(_structs.size() == _fields.size());
		return (_structs.size() == 0);
	}

	bool operator == (StructFieldPairs& rhs) {
		if (listStringEqual(_structs, rhs._structs)
				&& listEqual(_fields, rhs._fields)) {
			return true;
		}
		return false;
	}

	void update(string sName, unsigned field) {
		_structs.push_front(sName);
		_fields.push_front(field);
	}

	void print();

	void printStructs();
	void printFields();

	int size() {
		assert(_structs.size() == _fields.size());
		return _structs.size();
	}
};

/**
 * This should have the ability to describe the two types of global mapping
 * 1. global variable or function as a single type
 * 2. global variable as a structure
 */
struct GlobalConfValue {

	//this could be either a variable or a function
	GlobalValue* gvar;
	StructFieldPairs* sfp;

	GlobalConfValue(GlobalValue* gv) {
		gvar = gv;
		sfp = NULL;
	}

	GlobalConfValue(GlobalValue* gv, list<string>* varnames, list<unsigned>* varindices) {
		gvar = gv;
		sfp = new StructFieldPairs(varnames, varindices);
	}

	GlobalConfValue(GlobalValue* gv, StructFieldPairs* sfpp) {
		gvar = gv;
		sfp = new StructFieldPairs(sfpp);
	}

	~GlobalConfValue() {
		if(sfp) {
			delete sfp;
		}
	}

	void print() {
		if(gvar) {//used for testing
			errs() << "GlobalValue: " << gvar->getNameStr() << "\n";
		}

		if(sfp != NULL) {
			sfp->print();
		}
	}

	void pushBackSFP(StructFieldPairs* input_sfp) {
		if(sfp) {
			sfp->pushBack(input_sfp);

		} else {
			sfp = new StructFieldPairs(input_sfp);
		}
	}

	//return
	//-1: not the same gvar
	//0:  Redundant
	//1:  same gvar, but not match, new stuff!!!
	//2:  the input infomation is more accurate than the previous, need update
	int update(GlobalConfValue* gcv) {
		if(gcv->gvar != gvar) {
			return -1;
		}

		if(!(gcv->sfp) || gcv->sfp->isEmpty()) {
			return 0;
		}

		if(!sfp) { //here we know gcv's sfp is not empty
			sfp = new StructFieldPairs(gcv->sfp);
			return 2;
		}

		if(sfp->size() > gcv->sfp->size()) {
			if(sfp->prefixMatch(gcv->sfp)) {
				return 0;
			} else {
				return 1;
			}

		} else if(sfp->size() == gcv->sfp->size()) {
			if(sfp->operator ==(*(gcv->sfp))) {
				return 0;
			} else {
				return 1;
			}

		} else { //sfp->size() < gcv->sfp->size()
//			errs() << "before prefix match\n";
//			gcv->sfp->print();
//			sfp->print();

			if(gcv->sfp->prefixMatch(sfp)) {
				errs() << "after prefix match\n";

//				gcv->sfp->update(sfp);
				sfp->update(gcv->sfp);

				return 2;
			} else {
				return 1;
			}
		}
	}
};

struct LocalConfValues {
	set<StructFieldPairs*> localConfValues;

	set<StructFieldPairs*>::iterator begin() {
		return localConfValues.begin();
	}

	set<StructFieldPairs*>::iterator end() {
		return localConfValues.end();
	}

	set<StructFieldPairs*>* getStructFieldPairs() {
		return &localConfValues;
	}

	void insert(StructFieldPairs* sfp) {
		localConfValues.insert(sfp);
	}

	/**
	 * check whether the sfp is included in the localConfValues
	 */
	bool isIncluded_update(StructFieldPairs* isfp) {
		set<StructFieldPairs*>::iterator iter = localConfValues.begin(), end = localConfValues.end();
		for(; iter != end; iter++) {
			StructFieldPairs* lsfp = *iter;
			if(lsfp->size() >= isfp->size()) {

				if(lsfp->anywhereMatch(isfp)) {
					return true;
				}

			} else {//the input isfp is larger

				if(isfp->anywhereMatch(lsfp)) {
					lsfp->update(isfp);
					return true;
				}
			}
		}
		return false;
	}

	void deleteSFP(StructFieldPairs* sfp) {
//		list<StructFieldPairs*> tmplist;
//
//		set<StructFieldPairs*>::iterator iter = localConfValues.begin(), end = localConfValues.end();
//		for(; iter != end; iter++) {
//			tmplist.push_back(*iter);
//		}
//
//		while(tmplist.size() > 0) {
//			StructFieldPairs* tsfp = tmplist.front();
//			tmplist.pop_front();
//
//			if(sfp->operator ==(*tsfp)) {
//				localConfValues.erase(tsfp);
//			}
//		}
		localConfValues.erase(sfp);
	}

	void print() {
		set<StructFieldPairs*>::iterator iter = localConfValues.begin(), end = localConfValues.end();
		for(; iter != end; iter++) {
			errs() << "----------------- " << *iter << "\n";
			(*iter)->print();
		}
	}

};

struct GlobalConfValues {
	set<GlobalConfValue*> globalConfValues;

	~GlobalConfValues() {
		set<GlobalConfValue*>::iterator iter = globalConfValues.begin(), end = globalConfValues.end();
		for(; iter != end; iter++) {
			delete *iter;
		}

		globalConfValues.clear();
	}

	set<GlobalConfValue*>::iterator begin() {
		return globalConfValues.begin();
	}

	set<GlobalConfValue*>::iterator end() {
		return globalConfValues.end();
	}

	/**
	 * check whether the sfp is included in the localConfValues
	 * we do not update here.
	 */
	bool isIncluded(StructFieldPairs* isfp) {
		set<GlobalConfValue*>::iterator iter = globalConfValues.begin(), end = globalConfValues.end();

		for(; iter != end; iter++) {
			StructFieldPairs* lsfp = (*iter)->sfp;

			if(lsfp && lsfp->size() >= isfp->size()) {

				if(lsfp->anywhereMatch(isfp)) {
					return true;
				}
			}
		}

		return false;
	}

	/**A Safe Insert: Ignore the redundant ones
	 * Return: whether the value is already there
	 * 1 -- isNew == true
	 * 2 -- isNew == false
	 */
	bool insert(GlobalConfValue* gcv) {
		set<GlobalConfValue*>::iterator gcvIter = globalConfValues.begin(), gcvEnd = globalConfValues.end();

		bool isNew = true;
		for(; gcvIter != gcvEnd; gcvIter++) {
//			(*gcvIter)->print();

			int ret = (*gcvIter)->update(gcv);
			if(ret == 0 || ret == 2) {
				isNew = false;
				break;
			}
		}

		if(isNew == true) {
			globalConfValues.insert(gcv);
		}

		return isNew;
	}

	void print() {
		set<GlobalConfValue*>::iterator iter = globalConfValues.begin(), end = globalConfValues.end();
		for(; iter != end; iter++) {
			(*iter)->print();
			errs() << "------------------------\n";
		}
	}
};

/**
 * we use this data structure trying to store all
 * the gv, or gep instructions (lv) related to _sfp
 */
struct ValueUsage {
	string _directive;
	StructFieldPairs* _sfp;
	Value* _lUsage;

	ValueUsage(string d, StructFieldPairs* sfp, Value* i) {
		_directive = d;
		_sfp = sfp;
		_lUsage = i;
	}
};

struct ValueUsageMap {
	map<string, list<ValueUsage*>*> _lvUsageMap;

	~ValueUsageMap() {
		map<string, list<ValueUsage*>*>::iterator iter = _lvUsageMap.begin(), end = _lvUsageMap.end();
		for(; iter != end; iter++) {
			list<ValueUsage*>* lvuSet = (*iter).second;

			list<ValueUsage*>::iterator sIter = lvuSet->begin(), sEnd = lvuSet->end();
			for(; sIter != sEnd; sIter++) {
				delete (*sIter);
			}
			delete (*iter).second;
		}
	}

	void insert(ValueUsage* lvu) {
		string directive = lvu->_directive;

		if(_lvUsageMap.count(directive)) {
			list<ValueUsage*>* tmplvu = _lvUsageMap[directive];
			tmplvu->push_back(lvu);

		} else {
			list<ValueUsage*>* lvuList = new list<ValueUsage*>();
			lvuList->push_back(lvu);
			_lvUsageMap[directive] = lvuList;
		}
	}

	void insert(list<ValueUsage*>* lvus) {
		list<ValueUsage*>::iterator lvuIter = lvus->begin(), lvuEnd = lvus->end();
		for(; lvuIter != lvuEnd; lvuIter++) {
			insert(*lvuIter);
		}
	}
};

/**
 * This is a very complex data structure, what we want to record here is
 * a mapping from configuration parameter to its usage information
 *
 * the usage information includes the usage itself, i.e., Value*, and
 * the call chain that reach this usage.
 *
 * for global basic variable, there's no usage information.
 * usage information is only for structure based configuration variables
 */
typedef map<Value*, list<Context* >* > UsageInfo;

struct ConfUsageInfo {

	void addConfUsageInfo(string directive, Value* usage, Context* callchain);

	~ConfUsageInfo();

	void print();

	void statistics();

	bool getConfParamSet(Value* v, set<string>* cpset);

	void getUsageSet(string confPar, set<Value*>* usages);

//	//this should be used cautiously because it's quite expensive
//	Func2ConfUsageMap* constructFunc2ConfUsageMap(Module* mod);

	map<string, UsageInfo* > _confUsageInfo;

	void getExistingFunction2ParamMapping(Func2ConfUsageMap* f2pmap);

private:
	void get1stStopInstructionUsage(Value* v, set<Instruction*>* iset);
};

//struct MapFunc2CVD {
//
//	void addFunction(Function* f) {
//		if(funcMap.find(f) == funcMap.end()) {
//			map<string, set<int>*>* cvd = new map<string, set<int>*>();
//			funcMap[f] = cvd;
//		}
//	}
//
//	//here we've already made sure that the Function* f is in the map!
//	void insert2MapFunc2CVD(Function* f, string& directive, set<int>* rSet) {
//		map<string, set<int>*>* m = funcMap[f];
//		if(m->find(directive) == m->end()) {
//			set<int>* s = new set<int>();
//			m->operator[](directive) = s;
//		}
//		m->operator[](directive)->insert(rSet->begin(), rSet->end());
//	}
//
//	void insert2MapFunc2CVD(Function* f, string& directive, int rf) {
//		map<string, set<int>*>* m = funcMap[f];
//		if(m->find(directive) == m->end()) {
//			set<int>* s = new set<int>();
//			m->operator[](directive) = s;
//		}
//		m->operator[](directive)->insert(rf);
//	}
//
//	void mergeMapFunc2CVD(Function* caller, Function* callee) {
//		map<string, set<int>*>* mCaller = funcMap[caller];
//		map<string, set<int>*>* mCallee = funcMap[callee];
//		map<string, set<int>*>::iterator iter = mCallee->begin(), end = mCallee->end();
//
//		for(; iter != end; iter++) {
//			string dd = (*iter).first;
//			set<int>* rlist = (*iter).second;
//
//			if(mCaller->find(dd) == mCaller->end()) {
//				mCaller->operator [](dd) = new set<int>();
//			}
//			mCaller->operator [](dd)->insert(rlist->begin(), rlist->end());
//		}
//	}
//
//	bool hasFunction(Function* f) {
//		if(funcMap.find(f) != funcMap.end()) {
//			return true;
//		}
//		return false;
//	}
//
//	map<string, set<int>* >* getCVDMap(Function* f) {
//		return funcMap[f];
//	}
//
//	map<Function*, map<string, set<int>*>* > funcMap;
//};

//actually this is designed to replace FuncArgPairs because of its stronger expression
//but there are too many work to keep back compatibility, so we do it generally
struct ConfContainer {

	enum ContainerType {
	  NO_INTEREST = 0,
	  GLOBAL_BASIC_VAR,
	  LOCAL_BASIC_VAR,
	  GLOBAL_STRUCTURE_VAR,
	  LOCAL_STRUCTURE_VAR
	};

	ContainerType type;

	GlobalValue* gv;
	Value* lastLocalVar;

	list<string> varNames;
	list<unsigned> varIndices;

	ConfContainer() {
		type = NO_INTEREST;
		gv = NULL;
		lastLocalVar = NULL;
	}

	void add2front(string varname, int index) {
		varNames.push_front(varname);
		varIndices.push_front(index);
	}

//	//this means it's not a structure but only a variable
//	void add2front(string varname) {
//		//note: UINT_MAX means any!!!!!!
//		add2front(varname, UINT_MAX);
//	}

	string getGlobalValueStr() {
		assert(type == GLOBAL_BASIC_VAR || type == GLOBAL_STRUCTURE_VAR);
		return gv->getNameStr();
	}

	GlobalConfValue* getGlobalValue() {
		assert(type == GLOBAL_BASIC_VAR || type == GLOBAL_STRUCTURE_VAR);

		if(type == GLOBAL_BASIC_VAR) {
			return new GlobalConfValue(gv);

		} else {
			varNames.pop_front();
			varIndices.pop_front();
			return new GlobalConfValue(gv, &varNames, &varIndices);
		}
	}

	StructFieldPairs* getLVStructFieldPairs() {
		assert(type == LOCAL_STRUCTURE_VAR);

		return new StructFieldPairs(&varNames, &varIndices);
	}

	void getContainer(Value* v, ValueCache* vCache);

	void print();

private:
	bool getTMPConfContainer(Value* v, ValueCache* vCache);

	void trim();
};

bool getStructNField(GetElementPtrInst* geptr, StructFieldPairs* sfpair);

map<string, LocalConfValues* >::iterator findStructField(string structName,
		unsigned field, map<string, LocalConfValues* >* m);

int checkStructField(string fileName, int field, list<StructFieldPairs*>* m);

bool isFromStruct(Value* v, StructFieldPairs* sfpair);

#endif /* CONTAINER_H_ */
