/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#include "Constraints.h"

void dumpConstraint(Constraint* constraint) {
	constraint->dump();
}

void dumpConstraints(Constraints* constraints) {

	list<Constraint*>::iterator iter = constraints->begin(), end = constraints->end();

	errs() << "Total number of constraints: " << constraints->size() << "\n";

	for (; iter != end; iter++) {
		errs() << "^^^^^^^Constraint^^^^^^\n";
		(*iter)->dump();
	}
}
