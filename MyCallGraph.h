/*
 * MyCallGraph.h
 *
 *  Created on: Aug 30, 2012
 *      Author: tianyin
 */

#ifndef MYCALLGRAPH_H_
#define MYCALLGRAPH_H_

//#include "llvm/Analysis/CallGraph.h"

#include "llvm/Module.h"

#include "Utils.h"
#include "Common.h"

using namespace std;
using namespace llvm;

///*
// * Stealing from llvm's BasicCallGraph which is a module pass
// */
//class MyCallGraph: public CallGraph {
//
//public:
//	// Root is root of the call graph, or the external node if a 'main' function
//	// couldn't be found.
//	//
//	CallGraphNode *Root;
//
//	// ExternalCallingNode - This node has edges to all external functions and
//	// those internal functions that have their address taken.
//	CallGraphNode *ExternalCallingNode;
//
//	// CallsExternalNode - This node has edges to it from all functions making
//	// indirect calls or calling an external function.
//	CallGraphNode *CallsExternalNode;
//
//public:
//
//	MyCallGraph() :
//		Root(0),
//		ExternalCallingNode(0),
//		CallsExternalNode(0) {}
//
//	// runOnModule - Compute the call graph for the specified module.
//	virtual bool runOnModule(Module &M) {
//		CallGraph::initialize(M);
//
//		ExternalCallingNode = getOrInsertFunction(0);
//		CallsExternalNode = new CallGraphNode(0);
//		Root = 0;
//
//		// Add every function to the call graph.
//		for (Module::iterator I = M.begin(), E = M.end(); I != E; ++I)
//			addToCallGraph(I);
//
//		// If we didn't find a main function, use the external call graph node
//		if (Root == 0)
//			Root = ExternalCallingNode;
//
//		return false;
//	}
//
//	virtual void getAnalysisUsage(AnalysisUsage &AU) const {
//		AU.setPreservesAll();
//	}
//
//	virtual void print(raw_ostream &OS, const Module *) const {
//		OS << "CallGraph Root is: ";
//		if (Function *F = getRoot()->getFunction())
//			OS << F->getName() << "\n";
//		else {
//			OS << "<<null function: 0x" << getRoot() << ">>\n";
//		}
//
//		CallGraph::print(OS, 0);
//	}
//
//	virtual void releaseMemory() {
//		destroy();
//	}
//
//	/// getAdjustedAnalysisPointer - This method is used when a pass implements
//	/// an analysis interface through multiple inheritance.  If needed, it should
//	/// override this to adjust the this pointer as needed for the specified pass
//	/// info.
//	virtual void *getAdjustedAnalysisPointer(AnalysisID PI) {
//		if (PI == &CallGraph::ID)
//			return (CallGraph*) this;
//		return this;
//	}
//
//	CallGraphNode* getExternalCallingNode() const {
//		return ExternalCallingNode;
//	}
//	CallGraphNode* getCallsExternalNode() const {
//		return CallsExternalNode;
//	}
//
//	// getRoot - Return the root of the call graph, which is either main, or if
//	// main cannot be found, the external node.
//	//
//	CallGraphNode *getRoot() {
//		return Root;
//	}
//
//	const CallGraphNode *getRoot() const {
//		return Root;
//	}
//
//	void PrintTest() {
//		errs() << "----------------------------------------------------\n";
//		errs() << "we have " << this->FunctionMap.size() << " functions in all.\n";
//		errs() << "----------------------------------------------------\n";
//
//		FunctionMapTy::iterator iter = this->begin(), end = this->end();
//		for (; iter != end; iter++) {
//			const Function* f = iter->first;
//			if(f)
//				errs() << f->getNameStr() << "\n";
//		}
//		errs() << "----------------------------------------------------\n";
//	}
//
//private:
//
//	//===---------------------------------------------------------------------
//	// Implementation of CallGraph construction
//	//
//	// addToCallGraph - Add a function to the call graph, and link the node to all
//	// of the functions that it calls.
//	//
//	void addToCallGraph(Function *F) {
//		CallGraphNode *Node = getOrInsertFunction(F);
//
//		// If this function has external linkage, anything could call it.
//		if (!F->hasLocalLinkage()) {
//			ExternalCallingNode->addCalledFunction(CallSite(), Node);
//
//			// Found the entry point?
//			if (F->getName() == "main") {
//				if (Root) // Found multiple external mains?  Don't pick one.
//					Root = ExternalCallingNode;
//				else
//					Root = Node; // Found a main, keep track of it!
//			}
//		}
//
//		// If this function has its address taken, anything could call it.
//		if (F->hasAddressTaken())
//			ExternalCallingNode->addCalledFunction(CallSite(), Node);
//
//		// If this function is not defined in this translation unit, it could call
//		// anything.
//		if (F->isDeclaration() && !F->isIntrinsic())
//			Node->addCalledFunction(CallSite(), CallsExternalNode);
//
//		// Look for calls by this function.
//		for (Function::iterator BB = F->begin(), BBE = F->end(); BB != BBE; ++BB)
//			for (BasicBlock::iterator II = BB->begin(), IE = BB->end(); II != IE; ++II) {
//				CallSite CS(cast<Value>(II));
//				if (CS && !isa<IntrinsicInst>(II)) {
//					const Function *Callee = CS.getCalledFunction();
//					if (Callee)
//						Node->addCalledFunction(CS, getOrInsertFunction(Callee));
//					else
//						Node->addCalledFunction(CS, CallsExternalNode);
//				}
//			}
//	}
//
//	// destroy - Release memory for the call graph
//	virtual void destroy() {
//		/// CallsExternalNode is not in the function map, delete it explicitly.
//		if (CallsExternalNode) {
//			CallsExternalNode->allReferencesDropped();
//			delete CallsExternalNode;
//			CallsExternalNode = 0;
//		}
//		CallGraph::destroy();
//	}
//};

struct MiniGraphNode {

	MiniGraphNode(Function* F) {
		_function = F;
	}

	~MiniGraphNode() {
		_callees.clear();
	}

	Function* _function;
	set<MiniGraphNode*> _callees;
};

struct MiniCallGraph {
	map<Function*, MiniGraphNode*>* _miniCallGraph;

	MiniCallGraph(Module& M) {
		_miniCallGraph = new map<Function*, MiniGraphNode*>();
		runOnModule(M);
	}

	~MiniCallGraph() {
		destroy(_miniCallGraph);
	}

	MiniGraphNode* getCallGraphNode(Function* F) {
		return _miniCallGraph->operator [](F);
	}

	void addEdge(Function* Caller, Function* Callee) {
		addNode(Caller);
		addNode(Callee);

		_miniCallGraph->operator [](Caller)->_callees.insert(_miniCallGraph->operator [](Callee));
	}

	void BFS(set<Function*>* seeds, set<Function*>* results) {
		set<Function*>::iterator sIter = seeds->begin(), sEnd = seeds->end();
		for(; sIter != sEnd; sIter++) {
			set<Function*> tmpresults;
			BFS(*sIter, &tmpresults);

			copySet(results, &tmpresults);
		}
	}

	void BFS(Function* seed, set<Function*>* results) {
		list<Function*> fifoQueue;
		fifoQueue.push_back(seed);

		while(fifoQueue.size() > 0) {
			Function* curr = fifoQueue.front();
			fifoQueue.pop_front();

			if(results->count(curr) == 0) {
				results->insert(curr);

				set<MiniGraphNode*> neighbors = _miniCallGraph->operator [](curr)->_callees;
				set<MiniGraphNode*>::iterator iter = neighbors.begin(), end = neighbors.end();
				for(; iter != end; iter++) {
					fifoQueue.push_back((*iter)->_function);
				}
			}
		}
	}

	void runOnModule(Module& M) {
		Module::iterator I = M.begin(), E = M.end();
		for (; I != E; ++I) {
			addToCallGraph(I);
		}
	}

	void printGraph() {
		printGraph(_miniCallGraph);
	}

	//only functions within the interesting set will be kept
	void subset(set<Function*>* interestingSet) {
		//1. later we swith to this graph
		map<Function*, MiniGraphNode*>* newGraph = new map<Function*, MiniGraphNode*>();

		set<Function*>::iterator iter = interestingSet->begin(), end = interestingSet->end();
		for(; iter != end; iter++) {
			if(_miniCallGraph->count(*iter) == 0) {
				/**
				 * [NOTE] that means the function CONTAINS config variable, BUT
				 * it is not called in CallInst!!!
				 *
				 * SSL_CTX_set_verify(p_ctx, verify_option, ssl_verify_callback);
				 *
				 * here ssl_verify_callback is a function and only used as an argument
				 */
				//TODO
				//first ignore
				errs() << "[error] " << *iter << " is ignored with contains config variable\n";

			} else {

				newGraph->operator [](*iter) = _miniCallGraph->operator [](*iter);
				_miniCallGraph->erase(*iter);
			}
		}

		//2. delete the irrelevant edges in the new graph
		map<Function*, MiniGraphNode*>::iterator xiter = newGraph->begin(), xend = newGraph->end();
		for(; xiter != xend; xiter++) {
			MiniGraphNode* iNode = xiter->second;

			set<MiniGraphNode*> deleteset;
			set<MiniGraphNode*>* ajl = &(iNode->_callees);
			set<MiniGraphNode*>::iterator yiter = ajl->begin(), yend = ajl->end();
			for(; yiter != yend; yiter++) {
				if(newGraph->count((*yiter)->_function) == 0) {
					deleteset.insert(*yiter);
				}
			}

			set<MiniGraphNode*>::iterator diter = deleteset.begin(), dend = deleteset.end();
			for(; diter != dend; diter++) {
				int x = ajl->erase(*diter);
				errs() << "---" << (*diter)->_function << "(" << x << ")\n";
			}
		}

//		printGraph(newGraph);

		//3. switch
		destroy(_miniCallGraph);
		_miniCallGraph = newGraph;
	}

//	void test() {
//		set<string> res;
//
//////		//Graph 1, Fig. 4.14 in Sanjoy's Algorithm book
//////		errs() << "Graph 1: Testing\n";
//////		addEdge("S", "G");
//////		addEdge("S", "A");
//////		addEdge("A", "E");
//////		addEdge("B", "A");
//////		addEdge("B", "C");
//////		addEdge("C", "D");
//////		addEdge("D", "E");
//////		addEdge("E", "B");
//////		addEdge("F", "A");
//////		addEdge("F", "E");
//////		addEdge("G", "F");
//////
//////		errs() << "S as source\n";
//////		res.clear();
//////		BFS("S", &res);
//////		printSet(&res);
////
//		//Graph 2, Fig. 3.9(a) in Sanjoy's book
//		errs() << "Graph 2: Testing\n";
//		addEdge("A", "B");
//		addEdge("B", "C");
//		addEdge("B", "D");
//		addEdge("B", "E");
//		addEdge("C", "F");
//		addEdge("E", "B");
//		addEdge("E", "F");
//		addEdge("E", "G");
//		addEdge("F", "C");
//		addEdge("F", "H");
//		addEdge("G", "H");
//		addEdge("G", "J");
//		addEdge("H", "K");
//		addEdge("I", "G");
//		addEdge("J", "I");
//		addEdge("K", "L");
//		addEdge("L", "J");
//
//		errs() << "B as source\n";
//		res.clear();
//		BFS("B", &res);
////		printSet(&res);
//
//		errs() << "H as source\n";
//		res.clear();
//		BFS("H", &res);
////		printSet(&res);
//
//		errs() << "F as source\n";
//		res.clear();
//		BFS("F", &res);
////		printSet(&res);
//	}

private:

	void addNode(Function* F) {
		if(_miniCallGraph->count(F) == 0) {
			_miniCallGraph->operator [](F) = new MiniGraphNode(F);
		}
	}

	void printGraph(map<Function*, MiniGraphNode*>* graph) {
		errs() << "----------PRINT GRAPH------------\n";

		map<Function*, MiniGraphNode*>::iterator iter = graph->begin(), end = graph->end();
		for(; iter != end; iter++) {
			errs() << iter->first->getNameStr() << " | ";
			MiniGraphNode* node = iter->second;
			set<MiniGraphNode*> neighbors = node->_callees;
			set<MiniGraphNode*>::iterator niter = neighbors.begin(), nend = neighbors.end();
			for(; niter != nend; niter++) {
				errs() << (*niter)->_function->getNameStr() << " ";
			}
			errs() << "\n";
		}
	}

	void addToCallGraph(Function *Caller) {
		// Look for calls by this function.
		for (Function::iterator BB = Caller->begin(), BBE = Caller->end(); BB != BBE; ++BB) {
			for (BasicBlock::iterator II = BB->begin(), IE = BB->end(); II != IE; ++II) {

				if(isa<CallInst>(II) || isa<InvokeInst>(II)) {
					Function* callee;
					if (isa<CallInst>(II)) {
						CallInst *callinst = dyn_cast<CallInst>(II);
						callee = getCalledFunction(callinst);

					} else if (isa<InvokeInst>(II)) {
						InvokeInst *invokeinst = dyn_cast<InvokeInst>(II);
						callee = invokeinst->getCalledFunction();
					}

					StringRef realName;
					if (!callee || !stripIntrinsic(callee, realName)) {
						continue;
					}

					addEdge(Caller, callee);
				}
			}
		}
	}

	void destroy(map<Function*, MiniGraphNode*>* graph) {
		map<Function*, MiniGraphNode*>::iterator iter = graph->begin(), end = graph->end();
		for(; iter != end; iter++) {
			delete iter->second;
		}

		graph->clear();
		delete graph;
	}
};

/*
* From this structure, we want to figure out which parameters are self-contained within this structure,
* and those ones have real control dependency with the one under tracking (A in the above example)
*
* In the example above, if E is self-contained in the structure, we expect all E's usage is covered
* by the functions in this structure, i.e., satisfying the following conditions:
*
* S = {foo, oof | bar, foobar, rab}
*
* 1. all E's usage is within S
* 2. all bar's usage is within S
* 3. all rab's usage is within S
*/

#endif /* MYCALLGRAPH_H_ */
