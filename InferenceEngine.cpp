/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This is the main analysis part of specification inference            */
/*                                                                      */
/* Author: Tianyin Xu, Jiaqi Zhang	                                    */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

//#define HAVE_DECL_BASENAME 1
//#include <demangle.h>

#include "InferenceEngine.h"

using namespace std;

bool Misconfig::runOnModule(Module &M) {
	this->_module = &M;

	int level = 0;

	time_t start = time(NULL);

	/*initializing the semantic database*/
	_initializeParamUsage();
	_initializeParamAffect();
	_initializeFuncAL();
	_initializeStringCmp();
	_initializeMetaDataInfo();

	/* All the Initialization */
	readConfigFile(_conf);

	errs() << "------------------------------Initialization Done--------------------------------------\n";

	errs() << "======================================Function Mapping!!!=======================================\n";
	level ++;

	//fortunately, this is common at least both for ontap and httpd
	if (_conf.funcArgIdx != -1) {
		funcMappingAnalysis(_conf.funcMappingFile);
	}

	this->printGVMap(ORIGIN);
	this->printGVMap(VICTIM);

	getIncubationBB();

	this->_mapVictimGV.clear();

	_initializeGV();
	_initializeLV();

	errs() << "======================================Global First!!!=======================================\n";
	level ++;

	/*
	 * track all the single variable specifications
	 */
	errs() << "====================================trackSpecFromGV()\n";
	trackSpecFromGV(&_mapGV);


	errs() << "-----------_mapVictimGV " << _mapVictimGV.size() << "\n";

	time_t tmp = time(NULL);
	prettyTimePrint(tmp-start);

	int round = 1;
	while (_mapVictimGV.size() > 0) {
		errs() << "====================================trackSpecFromGV (round " << round << ")\n";

		//we need another tmp map!!
		map<string, GlobalConfValues*> tmpMapVictimGV;

		//clone a tmp map and clear the current _mapVictimGVmap.
		_mapVictimGV.swap(tmpMapVictimGV);

		trackSpecFromGV(&tmpMapVictimGV);

		round ++;
	}

	this->printGVMap(ORIGIN);

	analyzeMetadataTypeConstraints(GLOBAL);

	this->_mapVictimLVStructField.clear();

	filterLVRedundency();

	printLVMap(ORIGIN);

	tmp = time(NULL);
	prettyTimePrint(tmp-start);

	//so we can see some tmp results.
	dumpAllConstraints2File(".before.ls.org");

	errs() << "======================================Local Second!!!=======================================\n";
	level ++;

	errs() << "====================================trackSpecFromLVStruct(ORIGIN)\n";

	trackSpecFromLVStruct(&_mapLVStructField);

	printLVMap(VICTIM);

	analyzeMetadataTypeConstraints(LOCAL);

	tmp = time(NULL);
	prettyTimePrint(tmp-start);

	dumpAllConstraints2File(".before.ls.vic");

	errs() << "====================================trackSpecFromLVStruct(VICTIM)\n";

	/**
	 * here I decide to do a pruning. For case like OS, there's way too much tracing and will last forever.
	 */
	if (_mapVictimLVStructField.size() > 0) {
		level ++;

		map<string, LocalConfValues*> tmpMapVictimLV;

		_mapVictimLVStructField.swap(tmpMapVictimLV);

		trackSpecFromLVStruct(&tmpMapVictimLV);

		filterLVRedundency();

		printLVMap(VICTIM);
	}

	tmp = time(NULL);
	prettyTimePrint(tmp-start);

	/* get results and output to file */
	dumpAllConstraints();

	//generagte usage map for each conf parameter!!!
	//here, we do not need the GV-Maps and LV-Maps, we rely on the usage map
	cleanMemory_s1();

/////////////////////////////////////////////////////////////////////////////////////////////////
// Phase Two: Dependency
/////////////////////////////////////////////////////////////////////////////////////////////////

//	_confUsageInfo.print();
	_confUsageInfo.statistics();
	constructFunc2ConfUsageMapping(&_func2ConfUsageMap, &_confUsageInfo);
//	printFunc2ConfUsageMap(&_func2ConfUsageMap);

	//construct the config call graph, i.e., the call graph only contains information related to
	//configuration-related functions
	_ccg = new MiniCallGraph(*_module);
//	MiniCallGraph ccg(*_module);
//	ccg.printGraph();

	set<Function*> functionSet;
	make_key_set(_func2ConfUsageMap, functionSet);
	_ccg->subset(&functionSet);
	functionSet.clear();
	_ccg->printGraph();

	//Control based Semantics, i.e., fork and pthread_create
//	testLoopInfo();
//	getFunc2CVMapping();

	/*
	 * track all the dependency specifications
	 */
	errs() << "====================================trackDepSpec()\n";

	trackDepSpec();

	cleanMemory_s2();

	dumpAllConstraints2File(".dep");

	time_t end = time(NULL);
	prettyTimePrint(end-start);

	return false;
}

void Misconfig::_initializeMetaDataInfo() {
	_metaDataInfo = new MyMetaDataInfo(this->_module);
	_metaDataInfo->_initialize();
}

void Misconfig::_initializeParamUsage() {
	ifstream infile(USAGE_FUNCTION);

	string line;
	string token;
    int value;

	while(getline(infile, line)) {
		line = trim(line);
		if(line.size() == 0 || line.find_first_of('#', 0) == 0 || line.find_first_of('?', 0) == 0
				|| line.find_first_of('!', 0) == 0) {
			continue;
		}

		FuncSemantic fSemantic;

		stringstream ss(line);

        //we know it should be 7 values

		//function name
		getline(ss, token, ',');
        token = trim(token);
        fSemantic.func_name = token;

		//arg index
		getline(ss, token, ',');
        token = trim(token);
        value = atoi(token.c_str());
        fSemantic.arg_idx = value;

		//structure name
		getline(ss, token, ',');
        token = trim(token);
        if(token.size() == 0) {
        	fSemantic.struct_name = "";
        } else {
          	fSemantic.struct_name = token;
        }

		//structure field
		getline(ss, token, ',');
        token = trim(token);
        value = atoi(token.c_str());
        fSemantic.struct_fld = value;

		//semantic string
		getline(ss, token, ',');
        token = trim(token);
		fSemantic.semantics = token;

		//return value
		getline(ss, token, ',');
        token = trim(token);
        value = atoi(token.c_str());
        fSemantic.returnCheck = value;

        //whether still go inside
        getline(ss, token, ',');
        token = trim(token);
        value = atoi(token.c_str());
        fSemantic.goInside = value;

		//addFuncSemantic(fSemantic);
//		fSemantic.print();
//		errs() << "" << funcName << "\n";

		//go to the function constraints
        FuncArgPair* newPair = new FuncArgPair(fSemantic.func_name, fSemantic.arg_idx,
        		fSemantic.returnCheck, fSemantic.goInside);

		_ufcache.insertFuncConstraint(newPair,
				new SysCallConstraint(fSemantic.func_name, fSemantic.arg_idx, fSemantic.semantics));

		if(fSemantic.returnCheck == 1) {
			unsigned r = 0;
			_funcAMap.insert(fSemantic.func_name, fSemantic.arg_idx, r);
		}

		syscallcache.insert(fSemantic.func_name);
	}
}

void Misconfig::_initializeParamAffect() {
	string funcName;

	funcName = "fork";
	FuncArgPair* newPair = new FuncArgPair(funcName, 0, 0, 0);
	_cfcache.insertFuncConstraint(newPair, new SysCallConstraint(funcName, 0, "CHILDPROCESSNUMBER"));

	funcName = "pthread_create";
	FuncArgPair* newPair2 = new FuncArgPair(funcName, 0, 0, 0);
	_cfcache.insertFuncConstraint(newPair2, new SysCallConstraint(funcName, 0, "CHILDTHREADNUMBER"));

}

bool Misconfig::_initializeGV() {
	if (_conf.globalMappingFile.size() != 0) {
		if (_lcngv.readGlobalVariables(_conf.globalMappingFile) != -1) {

			list<ConfnameGvarPair*>::iterator iter = _lcngv.begin(), end = _lcngv.end();
			for (; iter != end; iter++) {
				string gvarName = (*iter)->gvarName;
				string confName = (*iter)->confName;

				StringRef srGvarName(gvarName);
				GlobalVariable *gvar = getGlobalVariable(srGvarName);

				if (!gvar) {
					errs() << "[error] gvar of " << srGvarName.str()
						   << "cannot be found\n";
					continue;
				}

				//TODO: current the format is simple directive + global-variable-name,
				//latter it should be able to support global structure at the mapping stage
				GlobalConfValue* gvs = new GlobalConfValue(gvar);
				addGVmap(confName, gvs, ORIGIN);
			}

			return true;

		} else {
			errs() << "[error] fail to readGlobalVariables\n";
		}

	} else {
		errs() << "[info] there's no globalMappingFile\n";
	}

	return false;
}

bool Misconfig::_initializeLV() {
	if (_conf.localMappingFile.size() != 0) {
		//readLocalVariables("/home/tianyin/llvm/llvm-3.0.src/lib/Transforms/Misconfig/lighttpd.map");
		readLocalVariables(_conf.localMappingFile);
		//readLocalVariables("http.mapping.local", &_mapLCVStructField);
		readPredefinedStruct(_conf.structFile);

		dumpLVs();

		return true;

	} else {
		errs() << "[info] there's no localMappingFile\n";
	}

	return false;
}

void Misconfig::readConfigFile(Config& confs) {
	ifstream infile(CONFIG_FILE);
	string line;
	string directive, cValue;

	while (getline(infile, line)) {
		if (line.find_first_of('#', 0) == 0) {
			continue;
		}

		istringstream iss(line);
		iss >> directive >> cValue;
//		confStruct = confStruct.substr(confStruct.find(".")+1);
		if (directive.compare("globalMapping") == 0) {
			confs.globalMappingFile = cValue;
//			errs() << cValue << "\n";
		} else if (directive.compare("localMapping") == 0) {
			confs.localMappingFile = cValue;
//			errs() << cValue << "\n";
		} else if (directive.compare("outputFile") == 0) {
			confs.outputFile = cValue;
		} else if (directive.compare("structFile") == 0) {
			confs.structFile = cValue;

		} else if (directive.compare("application") == 0) {
			confs.application = cValue;

			if(confs.application.compare("ontap") == 0) {
				nonInterestedStructs.push_back("msg");
				nonInterestedStructs.push_back("wafl_inode");
//				nonInterestedStructs.push_back("runtime");
				nonInterestedStructs.push_back("buf");
				nonInterestedStructs.push_back("anon");
				nonInterestedStructs.push_back("wafl_priv");
				nonInterestedStructs.push_back("session");
				nonInterestedStructs.push_back("datum");

			} else if(confs.application.compare("vsftpd") == 0) {
				nonInterestedStructs.push_back("mystr");
			}

		} else if (directive.compare("structAnnoDB") == 0) {
//			confs.structAnnoDB = cValue;
			_scons._loadStructSemanticDB(cValue);

		} else if (directive.compare("appSpecFile") == 0) {
			confs.appSpecFile = cValue;
		} else if (directive.compare("defineCPP") == 0) {
			DEFINE_CPP = true;
//			demangledGlobalMap = new map<string, string>();
		} else if (directive.compare("funcArgIdx") == 0) {
			confs.funcArgIdx = atoi(cValue.c_str());
		} else if (directive.compare("funcMappingFile.setter") == 0) {
			confs.funcMappingFile = cValue;
//		} else if (directive.compare("funcMappingFile.checker") == 0) {
//			confs.func
		} else {
			errs() << "[error] directive " << directive << " does not exist!";
		}
	}

	if (confs.funcArgIdx != -1 && confs.funcMappingFile.empty()) {
		errs() << "[error] funcArgIdx != -1 && confs.funcMappingFile.empty()!!";
		assert(false);
	}
}

// Detect type constraint at initial stage
void Misconfig::putTypeConstraint(StringRef tstr) {

	Constraint* newCon = new TypeConstraint(tstr.str());

	addConstraintToCurDirective(newCon, NULL);
}

// Add a new specification to the current directive under inspection
void Misconfig::addConstraintToCurDirective(Constraint* newCon,
		Instruction* inst/*used for printing source code info*/) {

	if (inst != NULL) {
		unsigned linenum;
		StringRef fileName;
		getInstSrcLocation(inst, linenum, fileName);
		errs() << "Found Constraint at " << fileName.str() << ":" << linenum << "\n";
	}

//	Constraints* newConstraints;
//	if(!this->_vCache.hasValue(this->_curDirective)) {
//		newConstraints = new Constraints();
//		newConstraints->insert(newCon);
//
//		this->_vCache.insertCache(this->_curDirective, newConstraints);
//
//	} else{
//		newConstraints = this->_vCache.getConstraints(this->_curDirective);
//		newConstraints->insert(newCon);
//	}
//	Constraints newConstraints;
//	newConstraints.insert(newCon);

	this->_mapDirectiveConstraints.mergeConstraint(this->_curDirectiveStr, newCon);

	errs() << DUMP_CONSTRAINTS("AddConstraintToCurDirective");
	dumpConstraint(newCon);
	errs() << FINISH_DUMP_CONS;
}

void Misconfig::addConstraintsToCurDirective(Constraints* constraints, Instruction* inst) {

	if (constraints) {
		list<Constraint*>::iterator iter = constraints->begin(), end = constraints->end();

		for (; iter != end; iter++) {
			addConstraintToCurDirective(*iter, inst);
		}
	}
}

/*
 * Follow the data flow to track the use of a Value (recursively)
 * This is where we need the constraints as a hint to decide
 * where are the interesting places we want to make record. For example,
 * we make want to check whether it is used as a function parameter, so
 * and whether the function is syscall.
 *
 * =====Input=====
 * Value* v: the Value we want to check
 */
void Misconfig::trackDataFlow(Value* v, StructFieldPairs* sfp,
		Constraints* specifications,
		FACache* faCache, ValueCache* vCache,
		Context* ocallchain) {

	if(!v) { //this should never happen but it did happen :-(
		errs() << "[panic] v is null\n";
		return;
	}

	//for debugging
	unsigned linenum;
	StringRef fileName;

	Value::use_iterator u = v->use_begin(), end = v->use_end();
	for (; u != end; u++) {

		/*Check if it is already cached to avoid infinite loop*/
		if (vCache->exist(*u)) {
			continue;
		}
		/*Insert into cache*/
		vCache->insert(*u);

		/**
		 * The Value* v could be a function to support the getter function.
		 * For example, in Data ONTAP, we have abc_get()
		 * actually it's equilavent to the global variable
		 */
		if(isa<Function>(v)) {
			this->isFromReturn = true;
		}

		Context newcallchain(ocallchain);

		if( (isa<GlobalValue>(v) || isa<Constant>(v)) &&
				!(isa<GlobalValue>(*u) || isa<Constant>(*u)) ) {
			assert(isa<Instruction>(*u));

			if(!sfp || sfp->isEmpty()) {
				Instruction* i = dyn_cast<Instruction>(*u);
				newcallchain.pushBack(i);

				/**
				 * It might be problematic to only track the usage of the global variables, e.g.,
				 *
				 *  1906460   %0 = load i8** %variable.addr, align 8, !dbg !4467031
 	 	 	 	 *	1906461   %cmp = icmp eq i8* %0, bitcast (i32* @httpd_log_format to i8*), !dbg !4467031
 	 	 	 	 *	1906462   br i1 %cmp, label %if.then, label %if.else, !dbg !4467031
				 *
				 * 	In the above code snippet, the global variable we are tracking is @httpd_log_format,
				 * 	and the usage is line #1906461. If we put the #1906461 into the addConfUsageInfo,
				 * 	later, we go to #1906462 in trackDataFlowDep(), and we have no idea which value we are tracking
				 *
				 * 	[NOTE] the point here is, for global variables,
				 * 	let's just put the global variables into the _confUsageInfo and later we can get all the usage for sure
				 */

//				_confUsageInfo.addConfUsageInfo(_curDirectiveStr, *u, &newcallchain);
			}
		}

		//debugging information
		if (isa<Instruction>(*u)) {
			Instruction* i = dyn_cast<Instruction>(*u);
			getInstSrcLocation(i, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum << "\n";
		}

		if (isa<CallInst>(*u)) {
			CallInst* inst = dyn_cast<CallInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
					<< " of function call (CallInst)\n";

			if (isFromReturn) {
				isFromReturn = false;
				trackDataFlow(inst, sfp, specifications, faCache, vCache, &newcallchain);

			} else {
				Function *callee = getRealCalledFunction(inst);
				trackDataFlowCallCheck(inst, callee, v, CALLINST,
						sfp, specifications, faCache, vCache, &newcallchain);
			}

		} else if (isa<InvokeInst>(*u)) {
			InvokeInst* inst = dyn_cast<InvokeInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of function call (InvokeInst)\n";

			if (isFromReturn) {
				isFromReturn = false;
				trackDataFlow(inst, sfp, specifications, faCache, vCache, &newcallchain);

			} else {
				Function *callee = inst->getCalledFunction();
				trackDataFlowCallCheck(inst, callee, v, INVOKEINST,
					sfp, specifications, faCache, vCache, &newcallchain);
			}

		} else if (isa<LoadInst>(*u)) {
			LoadInst *inst = dyn_cast<LoadInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of load instruction\n";

			//the load inst itself represents the target
			trackDataFlow(inst, sfp, specifications, faCache, vCache, &newcallchain);

		} else if (isa<StoreInst>(*u)) {
			StoreInst *inst = dyn_cast<StoreInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum << " of store instruction\n";

			Value* tgt = inst->getOperand(1);
			Value* src = inst->getOperand(0);

			if (tgt == v) {
				//The store target is the value we want to track
				//Stop this one and continue with other uses because it is not forward slicing anymore
				//TODO We may still interested in it because from the type of the source,
				//TODO default value
				//we can know its type at least. Later deal with this

				if(isa<Constant>(src)) {
					string defVal = "";

					if(isa<ConstantInt>(src)) {
						defVal = int2string(dyn_cast<ConstantInt>(src)->getSExtValue());

					} else if(isa<ConstantArray>(src)) {
						defVal = getConstantArray(src);
					}

					if(defVal.length() != 0) {
						Constraint* newCon = new DefaultValueConstraint(defVal);

						addConstraintToCurDirective(newCon, inst);

						if(specifications) {
							specifications->insert(newCon);
						}
					}
				}

				continue;
			}

			//TODO
			//****checkUnknownStructNField(tgt);

//			//make it back-compatible
//			if (isa<GetElementPtrInst>(tgt)) {
//				GetElementPtrInst *gepi = dyn_cast<GetElementPtrInst>(tgt);
//
//				if (isNotInterestStructField(gepi, &_listPredefinedStructField)) {
//					continue;
//				}
//
//				//do it here.
//				//This part is to determine the structure constraint
//				//you can check the following example
//				//check sysutil.o.ll:vsf_sysutil_sockaddr_set_port()
//				Constraints* newCons = analyzeStructConstraint(gepi);
//				if (newCons) {
//					//1. add the the global one
//					addConstraintsToCurDirective(newCons, inst);
//
//					//2. merge to the current cons
//					if(specifications) {
//						specifications->insert(newCons);
//					}
//
//					delete newCons;
//
//					continue;
//				}
//			}

			/*
			 * If the target is from getelementptr instruction, then we
			 * go and check the source of the pointer, as well as the
			 * possible index used
			 */
			//This should be trace back much further, it could be A.B.C.D=1, and A is a global variable
			//a good example is found in ontap's tftpd
			//definitely more sophisticated strategy here

			/*
			 * The "Make-Sure" Example:
			 *
			 * Example 1:
			 *
			 * 1786394   %arrayidx = getelementptr inbounds [2 x i32]* @wafl_default_qtree_mode, i32 0, i64 %idxprom, !dbg !1472964
			 * 1786395   store i32 %4, i32* %arrayidx, align 4, !dbg !1472964
			 *
			 * In this case, we store @wafl_default_qtree_mode as a global variable for further trace
			 *
			 *  Example 2:
			 *
			 *   3114   %call34 = call %struct._vfiler* @sk_get_vfiler() noredzone, !dbg !9171
			 *   3115   %ftpd = getelementptr inbounds %struct._vfiler* %call34, i32 0, i32 38, !dbg !9171
			 *   3116   %23 = load %struct.ftpd_vars** %ftpd, align 8, !dbg !9171
			 *   3117   %ftpLogSizeString = getelementptr inbounds %struct.ftpd_vars* %23, i32 0, i32 21, !dbg !9171
			 *   3118   %24 = load i8** %ftpLogSizeString, align 8, !dbg !9171
			 *   3119   call void @kmem_free(i8* %24) noredzone, !dbg !9171
			 *   3120   %arraydecay35 = getelementptr inbounds [21 x i8]* %logSizeString, i32 0, i32 0, !dbg !9172
			 *   3121   %call36 = call i8* @kmem_strdup(i8* %arraydecay35, i32 1) noredzone, !dbg !9172
			 *   3122   %call37 = call %struct._vfiler* @sk_get_vfiler() noredzone, !dbg !9173
			 *   3123   %ftpd38 = getelementptr inbounds %struct._vfiler* %call37, i32 0, i32 38, !dbg !9173
			 *   3124   %25 = load %struct.ftpd_vars** %ftpd38, align 8, !dbg !9173
			 *   3125   %ftpLogSizeString39 = getelementptr inbounds %struct.ftpd_vars* %25, i32 0, i32 21, !dbg !9173
			 *   3126   store i8* %call36, i8** %ftpLogSizeString39, align 8, !dbg !9173
			 *   3127   %26 = load i64* %logsize, align 8, !dbg !9174
			 *   3128   %call40 = call %struct._vfiler* @sk_get_vfiler() noredzone, !dbg !9174
			 *   3129   %ftpd41 = getelementptr inbounds %struct._vfiler* %call40, i32 0, i32 38, !dbg !9174
			 *   3130   %27 = load %struct.ftpd_vars** %ftpd41, align 8, !dbg !9174
			 *   3131   %ftpLogSizeLimit = getelementptr inbounds %struct.ftpd_vars* %27, i32 0, i32 22, !dbg !9174
			 *   3132   store i64 %26, i64* %ftpLogSizeLimit, align 8, !dbg !9174
			 *   3133   br label %return, !dbg !9175
			 */

			ConfContainer cc;
			cc.getContainer(tgt, vCache);
			cc.print();

			if (cc.type != ConfContainer::NO_INTEREST) {
				if (cc.type == ConfContainer::GLOBAL_BASIC_VAR
						|| cc.type == ConfContainer::GLOBAL_STRUCTURE_VAR) {

					/*
					 * @bug-middleman, we only care the last one! for example, the chain is A.B.C.D,
					 * we don't want to record the information when we are in the middle like A.B.C
					 *
					 * one example is get_vfilter(). if we trace in the middle, we will go to trace
					 * all the get_vfiler pointers later
					**/
					GlobalConfValue* gcv = cc.getGlobalValue();
					if(sfp && !sfp->isEmpty()) {
						gcv->pushBackSFP(sfp);
					}

					Constraints* newCons = analyzeStructConstraint(gcv->sfp);
					if (newCons) {
						//1. add the the global one
						addConstraintsToCurDirective(newCons, inst);

						//2. merge to the current cons
						if(specifications) {
							specifications->insert(newCons);
						}

						delete newCons;
//						continue;
					}

					testadd2MapVictimGV(_curDirectiveStr, gcv);

				} else if(cc.type == ConfContainer::LOCAL_BASIC_VAR) {

					//we only further trace the same type ones, for example,
					//store int to int.
					//this is to avoid go to a more general type like big structures
					if(isSameType(tgt->getType(), src->getType())) {
//						errs() << "jjjjjjjjjjjjjjjjjjj" << *(cc.lastLocalVar) << "\n";
						trackDataFlow(cc.lastLocalVar, sfp, specifications, faCache, vCache, &newcallchain);
					}

				} else if(cc.type == ConfContainer::LOCAL_STRUCTURE_VAR) {

					StructFieldPairs* lcv = cc.getLVStructFieldPairs();

					if(sfp && !sfp->isEmpty()) { // see @bug-middleman
						lcv->pushBack(sfp);
					}

					Constraints* newCons = analyzeStructConstraint(lcv);
					if (newCons) {
						//1. add the the global one
						addConstraintsToCurDirective(newCons, inst);

						//2. merge to the current cons
						if(specifications) {
							specifications->insert(newCons);
						}

						delete newCons;
//						continue;
					}

					if(!SFPKeywordFilter(lcv)) {
						testadd2MapVictimLV(_curDirectiveStr, lcv);
					}
				}
			}

			//everything is fine
			trackDataFlow(tgt, sfp, specifications, faCache, vCache, &newcallchain);

		} else if (isa<GetElementPtrInst>(*u)) {
			GetElementPtrInst *inst = dyn_cast<GetElementPtrInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum << " of getElementPtr instruction\n";

			if(inst->getOperand(0) != v) {
				errs() << "inst->getOperand(0) != v\n";
				continue;
			}

			//this is special, we need a new sfp because otherwise the other usage will be messed up
			StructFieldPairs newsfp(sfp);

			if(!(newsfp.isEmpty())) { //check whether it's the one we are tracking

				if(inst->getNumOperands() == 3) {
					string structName = newsfp.getFirstStructName();
					unsigned index = newsfp.getFirstField();

					if(!gepMatch(inst, structName, index)) {
						continue; //we are not interested

					} else {
						if(newsfp.size() == 1) {
//							callchain->push_back(inst->getParent()->getParent());
							this->_confUsageInfo.addConfUsageInfo(_curDirectiveStr, inst, &newcallchain);
						}

						newsfp.popFirstStructName();
						newsfp.popFirstField();
					}

//					//back-compatibility
//					if (!isNotInterestStructField(inst, &_listPredefinedStructField)) {
//						trackDataFlow(inst, &newsfp, specifications, faCache, vCache);
//					}
//
//				} else if(inst->getNumOperands() == 2) {
//					trackDataFlow(inst, &newsfp, specifications, faCache, vCache);

				} else if (inst->getNumOperands() > 3) {
					errs() << "[error] the GetElementPtrInst has more than 3 operands and we didn't check it!!!";
					continue;
				}
			}

			//back-compatibility
			if (!isNotInterestStructField(inst, &_listPredefinedStructField)) {
				trackDataFlow(inst, &newsfp, specifications, faCache, vCache, &newcallchain);
			}

		} else if (isa<Constant>(*u)) {
			Constant *cons = dyn_cast<Constant>(*u);

			if (isa<ConstantExpr>(cons)) {
				ConstantExpr* cexpr = dyn_cast<ConstantExpr>(cons);

				/*stupid qiqi, getelementptr is not the only one!!*/

				if (cexpr->isCast()) { //cast is also easy to handle
//					errs() << "isCast(): " << *cexpr << "\n";

					trackDataFlow(cexpr, sfp, specifications, faCache, vCache, &newcallchain);

				} else if (cexpr->isDereferenceablePointer()) {
					/**
					 *
					 * This check is necessary because you may encounter the following case:
					 *
					 *       if (!cma_preset_load_by_name(cm_archive_preset_fullpathname[lev], true)) { ...
					 *
					 * where v is lev. without the checking, the tracing will think we are tracing a pathname
					 */
					if(cexpr->getOperand(0) != v) {
						continue;
					}

					//This one deals with the operand which is getElementPtrInst.
					//Here the getElementPtr has several operands to get the field, in the form of (for example):
					//getelementptr (%struct.unixd_config_rec* @ap_unixd_config, i32 0, i32 0)
					//TODO: I hate this

					//this is special, we need a new sfp because otherwise the other usage will be messed up
					StructFieldPairs newsfp(sfp);

					if(!(newsfp.isEmpty())) { //check whether it's the one we are tracking

						unsigned skip = 0;
						if(!constantGepMatch(cexpr, &newsfp, skip) > 0) {
							continue;

						} else { //note here we need to pop the matched ones
							if( ((unsigned) newsfp.size()) == skip) {
//								callchain->push_back(cexpr->);
								this->_confUsageInfo.addConfUsageInfo(_curDirectiveStr, cexpr, &newcallchain);
							}

							for(unsigned i=0; i<skip; i++) {
								newsfp.popFirstStructName();
								newsfp.popFirstField();
							}
						}
					}

//					errs() << "ConstantGetElementPtrInst -- isDereferenceablePointer(): " << *cexpr << "\n";

					//back-compatibility
					trackDataFlow(cexpr, &newsfp, specifications, faCache, vCache, &newcallchain);

				} else {
					//Don't know what's this yet...
				}
			}

		} else if (isa<CastInst>(*u)) {
			CastInst *inst = dyn_cast<CastInst>(*u);

			if (isa<TruncInst>(inst)) {
				TruncInst* truncInst = dyn_cast<TruncInst>(inst);

				string sty = getAccurateType(truncInst->getOperand(0)->getType());
				string dty = getAccurateType(truncInst->getDestTy());

				string trunc = sty + "->" + dty;

				addConstraintToCurDirective(new TypeConstraint(trunc), inst);
			}

			/* The cast instruction is the converter, such as
			 * trunc i32 to i16 */
			/* It's a unary instruction, only one parameter */
			/* vsftp' tunable_listen_port needs this */
//			Value* src = inst->getOperand(0);
			trackDataFlow(inst, sfp, specifications, faCache, vCache, &newcallchain);

		} else if (isa<CmpInst>(*u)) {
			CmpInst* inst = dyn_cast<CmpInst>(*u);

			if(isFromString(v)) {
				continue;
			}

			if(!sfp || sfp->isEmpty()) {

				if(newcallchain._doRefCheck == true) {
					//Checking Range Infomation
					Constraints* newCons = analyzeRangeConstraints(v, inst);
					if (newCons) {
						//1. add the the global one
						addConstraintsToCurDirective(newCons, inst);

						//2. merge to the current cons
						if(specifications) {
							specifications->insert(newCons);
						}

						delete newCons;
					}
				}
			}

			//TODO: if the compared value is variable, do you need to trace that one?
			//TODO: the answer should be yes

			//On compare, we are going to check its usage

			trackDataFlow(inst, sfp, specifications, faCache, vCache, &newcallchain);

		} else if (isa<BranchInst>(*u)) {
			BranchInst* inst = dyn_cast<BranchInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of store instruction\n";

			//here we do not care condition cases because these are belongs to dependencies specifications
			//which will be handled later in trackDataFlowDep
			if (!inst->isConditional()) {
				continue;
			}

		} else if (isa<BinaryOperator>(*u)) {
			BinaryOperator *binOper = dyn_cast<BinaryOperator>(*u);

			getInstSrcLocation(binOper, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of binary operator\n";

			/*
			 * To prevent code pattern like:
			 *
			 *     static char *parse_log_misc_string(apr_pool_t *p, log_format_item *it, const char **sa) {
			 *         ....
			 *         it->arg = apr_palloc(p, s - *sa + 1);
			 *         ....
			 *     }
			 *
			 * where sa is the one we are interested in.
			 * In this case, the code would be:
			 *
			 *     %sub.ptr.lhs.cast = ptrtoint i8* %11 to i32, !dbg !152489
			 *     %sub.ptr.rhs.cast = ptrtoint i8* %13 to i32, !dbg !152489
			 *     %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast, !dbg !152489
			 *     %add = add nsw i32 %sub.ptr.sub, 1, !dbg !152489
			 */
			Value* src1 = binOper->getOperand(0);
			Value* src2 = binOper->getOperand(1);

			if (isa<PtrToIntInst>(src1) || isa<PtrToIntInst>(src2)) {
				errs() << "[warn] Binary operation on pointers. Stop tracking!\n";
				continue;
			}

			//find the one we are tracing and the one who is doing the operation with us.
			Value* opval = (v == src1) ? src2 : src1;
			if(isa<ConstantInt>(opval)) {
				//TODO

			} else {
				newcallchain._doRefCheck = false;
			}

			trackDataFlow(binOper, sfp, specifications, faCache, vCache, &newcallchain);

		} else if (isa<SelectInst>(*u)) {
//			SelectInst* selectInst = dyn_cast<SelectInst>(*u);
//
//			if(selectInst->getCondition() == v) {
//				trackDataFlow(selectInst);
//			}

		} else if (isa<ReturnInst>(*u)) {

			errs() << "isFromMiddle -- " << isFromMiddle << "\n";

			if(isFromMiddle) {
				ReturnInst *ret = dyn_cast<ReturnInst>(*u);
				Function* func = ret->getParent()->getParent();

				//important to check the arg_size
				//a previous bug is because of this
				if(func->arg_size() == 0) {
					isFromReturn = true;
//					trackDataFlow(func, sfp, NULL);

					GlobalConfValue* gcv = new GlobalConfValue(func, sfp);
					testadd2MapVictimGV(_curDirectiveStr, gcv);
				}
			}

		} else if (isa<SwitchInst>(*u)) {
			SwitchInst* switchInst = dyn_cast<SwitchInst>(*u);

			if(switchInst->getOperand(0) == v) {
					if(isa<CastInst>(v) && isChar2IntCast(v)) {

					//here we guess this should tell the units
					unsigned numOperands = switchInst->getNumOperands();
					for (unsigned i = 2; i < numOperands; i += 2) {
						Value* tyVal = switchInst->getOperand(i);
						Value* block = switchInst->getOperand(i + 1);

						assert(isa<ConstantInt>(tyVal));
						assert(isa<BasicBlock>(block));

						ConstantInt* cint = dyn_cast<ConstantInt>(tyVal);
						int tVal = cint->getSExtValue();
						char c = (char) tVal;

						Constraint* optionCon = new OptionConstraint("switch", char2string(c), "UNIT");

						this->_mapDirectiveConstraints.mergeConstraint(this->_curDirectiveStr, optionCon);

						if(!isFromMiddle && (!sfp || sfp->isEmpty())) {
							errs() << "[info] find UUUUUUUUUUUUUUUUnit " << c << "\n";
							if(specifications) {
								specifications->insert(optionCon);
							}
						}
					}
				}
			}

		} else if (isa<PHINode>(*u)) {
			//do nothing
			PHINode* phiNode = dyn_cast<PHINode>(*u);

			/*
			 * check whether it's used as the value in the phi node.
			 * The phi instruction format is "<result> = phi <ty> [ <val0>, <label0>], ..."
			 * make sure v is one of the val
			 */
			unsigned numVal = phiNode->getNumIncomingValues();
			bool isVal = false;
			for(unsigned i=0; i<numVal; i++) {
				Value* val = phiNode->getIncomingValue(i);
				if(v == val) {
					isVal = true;
				}
			}

			if(isVal) {
				trackDataFlow(phiNode, sfp, specifications, faCache, vCache, &newcallchain);
			}

		} else if (isa<LandingPadInst>(*u)) {
			//we don't care landing pad, right?

		} else if (isa<Instruction>(*u)) {
			Instruction *inst = dyn_cast<Instruction>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of general Instruction instruction\n";

			//way too much
			//errs() << "[warn] unexpected instruction (do not further track) -- " << *inst << "\n";

		} else if (isa<Operator>(*u)) {
//			Operator *oper = dyn_cast<Operator>(*u);
			/* This part was used to learn how getelementptr works (it is an Operator)
			 * No need this part for now.
			 ConstantExpr* cexpr= dyn_cast<ConstantExpr>(oper);
			 unsigned opc = cexpr->getOpcode();
			 errs()<<"lala"<<opc<<"\n";
			 ArrayRef<unsigned> indices = cexpr->getIndices();
			 unsigned x = indices[0];
			 unsigned y = indices[1];
			 ArrayRef<unsigned>::iterator iter = indices.begin(), end = indices.end();
			 for(;iter!=end;iter++){
			 errs()<<" index number "<<*iter<<"\n";
			 }
			 */
			errs() << "ah!!! Operator!\n";

		}
	}
}

/**
 * Sorry, because of the *invoke* instruction (typically in C++),
 * I have to make this function so as to reuse the code
 */
void Misconfig::trackDataFlowCallCheck(Instruction* inst, Function* callee, Value* v,
		int callorinvoke, StructFieldPairs* sfp,
		Constraints* specifications,
		FACache* faCache, ValueCache* vCache,
		Context* callchain) {

	if (callorinvoke == CALLINST) {
		assert( isa<CallInst>(inst));
	} else if (callorinvoke == INVOKEINST) {
		assert( isa<InvokeInst>(inst));
	}

	StringRef realName;
	if (!callee || !stripIntrinsic(callee, realName)/*callee->isIntrinsic()*/) {
		/*Cannot find callee, or callee is a llvm inserted intrinsic function*/
		return;
	}

	if (callorinvoke == INVOKEINST) {
		errs() << "wooohooo!!!\n";
	}

//	string symbol2name = cplus_demangle(realName.str().c_str(), 0);
//	errs() << "[info] please cplus_demangle " << symbol2name << "\n";

	/*for class member functions in C++, this gives you the real function name*/
	string demangledName = DEFINE_CPP ? getDemangledName(realName.str()) : realName.str();

//	errs() << "++&&&++++" << demangledName << "\n";

	/*omg, [NOTE] here
	 the callee and the caller's number of argument might not be consistent, e.g., in httpd

	 **caller:
	 1916                 ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, ap_server_conf,
	 1917                              "child %" APR_PID_T_FMT " isn't taking over "
	 1918                              "slots very quickly (%d of %d)",
	 1919                              ap_my_pid, threads_created,
	 1920                              threads_per_child);
	 i.e.,
	 call void (i8*, i32, i32, i32, i32, %struct.server_rec*, i8*, ...)* bitcast (void (i8*, i32, i32, i32, i32, %struct.server_rec.11*, i8*, ...)* @ap_log_error_ to void (i8*, i32\
	  , i32, i32, i32, %struct.server_rec*, i8*, ...)*)(i8* getelementptr inbounds ([8 x i8]* @.str9124, i32 0, i32 0), i32 1916, i32 %cond137, i32 7, i32 0, %struct.server_rec* %121,\
	   i8* getelementptr inbounds ([57 x i8]* @.str309161, i32 0, i32 0), i32 %122, i32 %123, i32 %124), !dbg !76966

	 **callee:
	 define void @ap_log_error_(i8* %file, i32 %line, i32 %module_index, i32 %level, i32 %status, %struct.server_rec.11* %s, i8* %fmt, ...) nounwind {
	 */
	// To handle the above case, we use the match to indicate whether the caller's argument is found.
	bool match = false;

	/*Iterate all the arguments of the function to find corresponding argument*/
	//arg idx from caller
	unsigned argPosT = 0;
	unsigned numOpsT = inst->getNumOperands();
	Value* argT;
	for (unsigned ai = 0; ai < numOpsT; ai++) {
		argT = inst->getOperand(argPosT++);
		if (argT == v) {
			errs() << "It is used as the " << argPosT << "th parameter in "
				   << realName.str() << " [caller] \n";
			break;
		}
	}

	//arg idx from callee
	unsigned argPos = 0;
	Value* arg;
	Function::arg_iterator iter = callee->arg_begin(), iter_end = callee->arg_end();
	for (; iter != iter_end; iter++) {
		arg = inst->getOperand(argPos++);
		if (arg == v) {
			errs() << "It is used as the " << argPos << "th parameter in "
					<< realName.str() << " [callee] \n";
			match = true; //that means we get a match!
			break;
		}
	}

	if (argPos != argPosT) { //used for debug!
		errs() << "CALLEE-CALLER INCONSISTENT! [function] " + demangledName
			   << " [argPosT] " << argPosT << "\n";
	}

	if (argPosT > callee->arg_size()) {
		match = false;
		argPos = argPosT; //because the latter part use argPos
	}

	/**
	 * @Let's get rid of the ugly hacker here
	 * Now it's hack but not ugly :-P
	 * Sometimes, the sys-/lib- call is like
	 * declare i32 @pthread_setconcurrency(...)
	 * CallInst:  %call = call i32 (...)* @pthread_setconcurrency(i32 %0), !dbg !412317
	 * so in this case, we directly compare the realName if there is only one
	 **/

	FunctionTrack needsFurtherTrack = KNOW_NOTHING;

	errs() << "+++++++++++++++++++++++++++++++++ before analyzeAPIConstraints" << "\n";

	if(!sfp || sfp->isEmpty()) {
		if (callorinvoke == CALLINST) {
			/*optimization*/
			if (std::string::npos == demangledName.find("::")) {

				/*Try to extract syscall related constraint*/
				needsFurtherTrack = analyzeAPIConstraints(dyn_cast<CallInst>(inst), callee,
						argPos, specifications, false);

			} else {
				errs() << "++&&&++++ you are not a member function " << demangledName << "\n";
			}
		}
	}

	set<unsigned>* affectedArgs = NULL;

	/**
	 * The difference between the two blocks is when match is false, we are tracking an arugment that cannot be found in the function declaration
	 * In this case, we assume this function to be a system or lib call (otherwise we do not handle)
	 *
	 * So, the difference between getSysFuncAffectingList() and getFuncAffectingList() is that the former only check the current known value and
	 * will never go inside that function (because there is no mapping from the argument to the parameter)
	 *
	 * This is to cover cases like
	 *
	 * declare i32 @snprintf(i8*, i64, i8*, ...) noredzone
	 *
	 * %call1 = call i32 (i8*, i64, i8*, ...)* @snprintf(i8* %0, i64 %conv, i8* getelementptr inbounds ([5 x i8]* @.str28854, i32 0, i32 0), i32 %2) noredzone, !dbg !1503604
	 *
	 * so we can know the variable %2 goes to %0, because of the FuncAffectDB entry snprintf,4,1
	 *
	 * TODO
	 * TODO
	 * TODO
	 *
	 * we lost the sfp infomation here.
	 * so here we have the assumption that the input and output are peers, i.e., they have the same level in sfp
	 * because later we still use sfp for further tracing
	 * it cannot handle the following function
	 *
	 * void foo(A.B.C* input, A.B.C.D* output),  or A.B.C.D bar(A.B.C input);
	 *
	 * in the above case, the sfp will be changed, and the further tracing will lost
	**/
	if (match == false) {
		//here we bet the call is a sys-/lib- call, otherwise we have nothing to do
		affectedArgs = getSysAffectedArgumentList(callee, argPos);

	} else {
		set<Function*> entranceFuncCache;
		affectedArgs = getAffectedArgumentList(callee, argPos, &entranceFuncCache);
	}

	errs() << "===== affectedArgs (indices) ======\n";
	errs() << "      " << realName.str() << "\n";
	printSet(affectedArgs);

	bool trackReturn = false;
	bool goInside = false;

	/**[NOTE] **important**
	 * do this before the affected argument, otherwise the _ufcache would be changed!!!
	 */
	if(affectedArgs && affectedArgs->count(0)) {
		trackReturn = true;
	}

	switch(needsFurtherTrack) {
	case NO_GO_INSIDE_NO_TRACK_RETURN:
		//Found that it is constrained by system call
		break;

	case KNOW_NOTHING:
	case GO_INSIDE_NO_TRACK_RETURN: //do inter procedure data flow checking
		goInside = true;
		break;

	case NO_GO_INSIDE_BUT_TRACK_RETURN:
		trackReturn = true;
		break;

	case GO_INSIDE_AND_TRACK_RETURN:
		trackReturn = true;
		goInside = true;
		break;

	default:
		break;
	}

	if(goInside && match) {
		StructFieldPairs newsfp(sfp);
		Constraints* newCons = trackInterProcedureFlow(callee, argPos, inst, &newsfp, faCache, callchain, trackReturn, false);

		if(specifications) {
			specifications->insert(newCons);
		}
	}

	if(trackReturn) {
		//do a sanity check for the type
		//here we want to avoid the dataflow tracking when the input is a basic type and the
		//return value is a struct pointer and in the meantime sfp is none.
		//because in this case, the dataflow tracing will go to all the fields in the structure
		if(!sfp || sfp->isEmpty()) {
			if(isStructType(callee->getReturnType()) && isBasicType(v->getType())) {
				;
			} else {
				trackDataFlow(inst, sfp, specifications, faCache, vCache, callchain);
			}
		}
	}

	//#Comment X
	//here we also need to check whether the value comes from a global variable or function
	//if so, we need to check whether this global variable is stored in _lcngv
	/**
	 * 57386   %arrayidx8 = getelementptr inbounds [2 x [2 x %struct.dirlist]]* @tftpd_dirs, i32 0, i64 %idxprom7, !dbg !57377
	 * 57387   %arrayidx9 = getelementptr inbounds [2 x %struct.dirlist]* %arrayidx8, i32 0, i64 0, !dbg !57377
	 * 57388   %name10 = getelementptr inbounds %struct.dirlist* %arrayidx9, i32 0, i32 0, !dbg !57377
	 * 57389   %arraydecay11 = getelementptr inbounds [513 x i8]* %name10, i32 0, i32 0, !dbg !57377
	 * 57390   %6 = load i8** %rootdir.addr, align 8, !dbg !57377
	 * 57391   %call12 = call i8* @strcpy(i8* %arraydecay11, i8* %6) noredzone, !dbg !57377
	**/

//	bool alreadyTrackedRet = false;

	if (affectedArgs) {
		/* Iterate the affecting list and track all the affected * arguments*/
		set<unsigned>::iterator aaiter = affectedArgs->begin(), aaend = affectedArgs->end();

		for (; aaiter != aaend; aaiter++) {
			if ((*aaiter) == 0) { // 0 is the return value
				//If return value is affected, track the return value
//				trackDataFlow(inst, sfp, specifications, faCache, vCache);
//				alreadyTrackedRet = true;
//				trackReturn = true;
				errs() << "[info] do nothing...\n";

			} else {
				/**
				 * [NOTE] In CallInst, the last operand is the function itself!
				 */
				if (*aaiter < ((int) inst->getNumOperands())) {
					Value* affectedArg = inst->getOperand((*aaiter) - 1);

					//corresponds to #Comment X
					ConfContainer cc;
					cc.getContainer(affectedArg, vCache);
					cc.print();

					if (cc.type != ConfContainer::NO_INTEREST) {
						if (cc.type == ConfContainer::GLOBAL_BASIC_VAR
								|| cc.type == ConfContainer::GLOBAL_STRUCTURE_VAR) {

							GlobalConfValue* gcv = cc.getGlobalValue();

							if(sfp && !sfp->isEmpty()) {
								gcv->pushBackSFP(sfp);
							}

							Constraints* newCons = analyzeStructConstraint(gcv->sfp);
							if (newCons) {
								//1. add the the global one
								addConstraintsToCurDirective(newCons, inst);

								//2. merge to the current cons
								if(specifications) {
									specifications->insert(newCons);
								}

								delete newCons;
//								continue;
							}

							testadd2MapVictimGV(_curDirectiveStr, gcv);

						} else if(cc.type == ConfContainer::LOCAL_BASIC_VAR){
//							errs() << "jjjjjjjjjjjjjjjjjjj" << *(cc.lastLocalVar) << "\n";
							if(isSameType(affectedArg->getType(), v->getType())) {
								trackDataFlow(cc.lastLocalVar, sfp, specifications, faCache, vCache, callchain);
							}

						} else {
							StructFieldPairs* lcv = cc.getLVStructFieldPairs();

							if(sfp && !sfp->isEmpty()) { // see @bug-middleman
								lcv->pushBack(sfp);
							}

							Constraints* newCons = analyzeStructConstraint(lcv);
							if (newCons) {
								//1. add the the global one
								addConstraintsToCurDirective(newCons, inst);

								//2. merge to the current cons
								if(specifications) {
									specifications->insert(newCons);
								}

								delete newCons;
//								continue;
							}

							if(!SFPKeywordFilter(lcv)) {
								testadd2MapVictimLV(_curDirectiveStr, lcv);
							}
						}
					}

					/**
					 * @bug, if it's a constant,
					 * we will go to check a constant value like null to the hell!!!
					 */
					if(!isa<Constant>(affectedArg)) {
						trackDataFlow(affectedArg, sfp, specifications, faCache, vCache, callchain);
					}
				}
			}
		}
	}

	/**
	 * cannot free because it is pointing to the FuncAffectMap!!!
	 * //	if(affectedArgs) delete affectedArgs;
	 */
}

/*
 * trackDataFlowDep() share the similar process as the trackDataFlow() pass,
 * i.e., the same way of recursive def-use tracking.
 *
 * However, there are some fundamental principle and goals between them, which
 * makes them to be two seperate functions.
 *
 * In the trackDataFlowDep() pass, we only cares the dependency specifications, so,
 * we do not check any specs related to semantics, types, or reference values
 *
 * In terms of implementation,
 * 1) In this pass, we do not explore new structure that might store config parameters
 *    (this should be done in the trackDataFlow() stage),
 *    we only do dataflow tracking
 * 2) we only track any semantic, type, range related specifications
 * 3) we do not need to verify sfp everytime because it's assumed to be true
 * 4) we do not need to associate spcifications with functions
 * 5) for inter-procedure analysis, we go as deep as we can, there's nothing to do
 *    with the sys-/lib- call annotation. Even if it falls in an annotated function call,
 *    we go further if we can go.
 */
void Misconfig::trackDataFlowDep(Value* v,
		FACache* faCache, ValueCache* vCache,
		Context* ocallchain) {

	if(!v) {
		errs() << "[panic] v is null in trackDataFlowDep";
		return;
	}

	unsigned linenum;
	StringRef fileName;

	Value::use_iterator u = v->use_begin(), end = v->use_end();
	for ( ;u != end; u++) {

        /*Check if it is already cached to avoid infinite loop*/
        if (vCache->exist(*u)) {
                continue;
        }
        /*Insert into cache*/
        vCache->insert(*u);

        /**
         * The Value* v could be a function to support the getter function.
         * For example, in Data ONTAP, we have abc_get()
         * actually it's equilavent to the global variable
         */
        if(isa<Function>(v)) {
        	isFromReturn = true;
        }

        Context newcallchain(ocallchain);

        //debugging information
        if (isa<Instruction>(*u)) {
        	Instruction* i = dyn_cast<Instruction>(*u);
            getInstSrcLocation(i, linenum, fileName);
            errs() << "Got it in file " << fileName.str() << ":" << linenum << "\n";
        }

		if (isa<CallInst>(*u)) {
			CallInst* inst = dyn_cast<CallInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of call instruction\n";

			if(isFromReturn) {
				isFromReturn = false;
				trackDataFlowDep(inst, faCache, vCache, &newcallchain);

			} else {
				Function *callee = getRealCalledFunction(inst);
				trackDataFlowCallCheckDep(inst, callee, v, CALLINST, faCache, vCache, &newcallchain);
			}

		} else if (isa<InvokeInst>(*u)) {
			InvokeInst* inst = dyn_cast<InvokeInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of invoke instruction\n";

			if(isFromReturn) {
				isFromReturn = false;
				trackDataFlowDep(inst, faCache, vCache, &newcallchain);

			} else {
				Function *callee = inst->getCalledFunction();
				trackDataFlowCallCheckDep(inst, callee, v, INVOKEINST, faCache, vCache, &newcallchain);
			}

		} else if (isa<LoadInst>(*u)) {
			LoadInst *inst = dyn_cast<LoadInst>(*u);
			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of load instruction\n";

			//the load inst itself represents the target
			trackDataFlowDep(inst, faCache, vCache, &newcallchain);

		} else if (isa<StoreInst>(*u)) {
			StoreInst *inst = dyn_cast<StoreInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of store instruction\n";

			 Value* tgt = inst->getOperand(1);
			 Value* src = inst->getOperand(0);

			if (tgt == v) {
				//The store target is the value we want to track
				//here we do not interested in default values
				continue;
			}

			/*
			 * also, we do need to explore new structure any more! wooohooo
			 * directively go to forward tracing
			**/

			ConfContainer cc;
			cc.getContainer(tgt, vCache);
			if (cc.type != ConfContainer::NO_INTEREST) {
				if(cc.type == ConfContainer::LOCAL_BASIC_VAR) {
					 if(isSameType(tgt->getType(), src->getType())) {
						 trackDataFlowDep(cc.lastLocalVar, faCache, vCache, &newcallchain);
					 }
				}
			}

			trackDataFlowDep(tgt, faCache, vCache, &newcallchain);


		} else if (isa<GetElementPtrInst>(*u)) {
			GetElementPtrInst *inst = dyn_cast<GetElementPtrInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
					<< " of getElementPtr instruction\n";

			/**
			 * this could come from the inaccuracy bought by getFuncAffectList.
			 * Let's ignore
			 */
//			errs() << "Theoretically this should not be a gep instruction!!!\n";
//			assert(false);
			continue;


//			/**
//			 * there's nothing to do with sfp any more
//			 */
//
//            if(inst->getOperand(0) != v) {
//                    errs() << "inst->getOperand(0) != v\n";
//                    continue;
//            }
//
//			if (!isNotInterestStructField(inst, &_listPredefinedStructField)) {
//				trackDataFlowDep(inst, faCache, vCache, &newcallchain);
//			}

		} else if (isa<Constant>(*u)) {
			Constant *cons = dyn_cast<Constant>(*u);

			if (isa<ConstantExpr>(cons)) {
				ConstantExpr* cexpr = dyn_cast<ConstantExpr>(cons);

				if (cexpr->isCast()) {
					errs() << "isCast(): " << *cexpr << "\n";
					trackDataFlowDep(cexpr, faCache, vCache, &newcallchain);

				} else if (cexpr->isDereferenceablePointer()) {
					errs() << "isDereferenceablePointer(): " << *cexpr << "\n";

                    /**
                     *
                     * This check is necessary because you may encounter the following case:
                     *
                     *       if (!cma_preset_load_by_name(cm_archive_preset_fullpathname[lev], true)) { ...
                     *
                     * where v is lev. without the checking, the tracing will think we are tracing a pathname
                     */
                    if(cexpr->getOperand(0) != v) {
                            continue;
                    }


					//This one deals with the operand which is getElementPtr.
					//Here the getElementPtr has several operands to get the field, in the form of (for example):
					//getelementptr (%struct.unixd_config_rec* @ap_unixd_config, i32 0, i32 0)
					trackDataFlowDep(cons, faCache, vCache, &newcallchain);
				}

			} else {
				//Don't know what's this yet...
			}

		} else if (isa<CastInst>(*u)) {
			CastInst *inst = dyn_cast<CastInst>(*u);

			/* The cast instruction is the converter, such as
			 * trunc i32 to i16 */
			/* It's a unary instruction, only one parameter */
			/* vsftp' tunable_listen_port needs this */
			trackDataFlowDep(inst, faCache, vCache, &newcallchain);

		} else if (isa<CmpInst>(*u)) {
			CmpInst* inst = dyn_cast<CmpInst>(*u);

			/* This global variable is used very ugly:
			 * it is just used for the branch instruction (if there is one) later
			 * to know which one in this CmpInst is the real Value we are tracing
			 */
			_valueBefore2Steps = v;

			//On compare, we are going to check its usage
			trackDataFlowDep(inst, faCache, vCache, &newcallchain);

		} else if (isa<BranchInst>(*u)) {
			BranchInst* inst = dyn_cast<BranchInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
			errs() << "Got it in file " << fileName.str() << ":" << linenum
				   << " of branch instruction\n";

			if (!inst->isConditional()) {
				continue;
			}

			//backtrace one step we are going to check possible
			Value* cond = inst->getCondition();
			if (isa<CmpInst>(cond)) {
				CmpInst* cmpInst = dyn_cast<CmpInst>(cond);

				Value* cmpsrc1 = cmpInst->getOperand(0);
				Value* cmpsrc2 = cmpInst->getOperand(1);

				errs() << "before the assert\n";
				if(!(cmpsrc1 == _valueBefore2Steps || cmpsrc2 == _valueBefore2Steps)) {
					errs() << "[panic]    cmpsrc1 = " << *cmpsrc1 << "\n"
						   << "           cmpsrc2 = " << *cmpsrc2 << "\n"
					       << "           cmpInst = " << *cmpInst << "\n"
					       << "              inst =    " << *inst << "\n"
						   << "_valueBefore2Steps = " << *_valueBefore2Steps << "\n";

					assert(false);
				}

				Value* theOther = (cmpsrc1 == _valueBefore2Steps) ? cmpsrc2 : cmpsrc1;

				//Constant should be handled in the trackDataFlow() function
				//so here we don't care
				/**
				 * The logic here is deferentiated according to 2 cases:
				 * 1) the compared value is a variable, then we analyze the value dependency
				 * 2) the compared value is a constant, then we analyze the control dependency
				 */
				if (isa<Constant>(theOther)) {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// CONTROL DEPENDENCY ////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

					if(isa<GlobalValue>(theOther)) {
//						assert(isa<GlobalValue>(theOther));
						errs() << *cmpInst << "\n";
						errs() << *inst << "\n";
						assert(false);
					}

					string toCompare = "";
					if (isa<ConstantInt>(theOther)) {
						ConstantInt* cInt = dyn_cast<ConstantInt>(theOther);
						toCompare = int2string(cInt->getSExtValue());
					} else if(isa<ConstantPointerNull>(theOther)) {
//						toCompare = "null";
						toCompare = "0";
					} else if(isa<ConstantArray>(theOther)) {
						toCompare = getConstantArray(theOther);
					} else {
						toCompare = "others";
					}

					/*
					 * Start to do if-dep analysis
					 */
					BasicBlock* currbb = inst->getParent();
					BasicBlock* truebb = inst->getSuccessor(0);
					BasicBlock* falsebb = inst->getSuccessor(1);

					Function* currfunc = currbb->getParent();

					PostDominatorTree pdt;
					pdt.runOnFunction(*currfunc);

					DominatorTree dt;
					dt.runOnFunction(*currfunc);

//					if (dt.dominates(currbb, falsebb)) {
//						errs() << "dt.dominates(currbb, falsebb)\n";
//						//analyzeIfDependencyConstraints(truebb, NULL, &_curDirectiveStr, toCompare);
//					} else {
//						errs() << "! dt.dominates(currbb, falsebb)\n";
//						//analyzeIfDependencyConstraints(truebb, falsebb, &_curDirectiveStr, toCompare);
//					}

//					set<string> params;
					if (pdt.dominates(falsebb, truebb)) {
						errs() << "falsebb post-dominates truebb\n";
//						analyzeIfDependencies(truebb, NULL, &params, &dt, &pdt, &newcallchain);
						controlDepRecording(truebb, NULL, toCompare, &dt, &pdt, &newcallchain);

					} else if(pdt.dominates(truebb, falsebb)) {
						/*@bug (true BB postdominates false BB)
						 * It is not only possible but also common that truebb post-dominates falsebb
						 * e.g., in the following vsftpd example:
						 *
						 *  69     if (!tunable_sslv2)
 	 	 	 	 	 	 *  70     {
 	 	 	 	 	 	 *  71       options |= SSL_OP_NO_SSLv2;
 	 	 	 	 	 	 *  72     }
 	 	 	 	 	 	 *  73     if (!tunable_sslv3)
 	 	 	 	 	 	 *  74     {
 	 	 	 	 	 	 *  75       options |= SSL_OP_NO_SSLv3;
 	 	 	 	 	 	 *  76     }
						 *
						 * the corresponding bitcode is:
						 *
						 *  21794 if.end:                                           ; preds = %if.then3, %if.then
					     *  21795   store i64 4095, i64* %options, align 8, !dbg !11832
						 *  21796   %2 = load i32* @tunable_sslv2, align 4, !dbg !11833
						 *  21797   %tobool4 = icmp ne i32 %2, 0, !dbg !11833
						 *  21798   br i1 %tobool4, label %if.end6, label %if.then5, !dbg !11833
						 *  21799
						 *  21800 if.then5:                                         ; preds = %if.end
						 *  21801   %3 = load i64* %options, align 8, !dbg !11834
  	  	  	  	  	  	 *  21802   %or = or i64 %3, 16777216, !dbg !11834
						 *  21803   store i64 %or, i64* %options, align 8, !dbg !11834
						 *  21804   br label %if.end6, !dbg !11836
						 *  21805
						 *  21806 if.end6:                                          ; preds = %if.then5, %if.end
						 *  21807   %4 = load i32* @tunable_sslv3, align 4, !dbg !11837
						 *  21808   %tobool7 = icmp ne i32 %4, 0, !dbg !11837
						 *  21809   br i1 %tobool7, label %if.end10, label %if.then8, !dbg !11837
						 *
						 *  In this case, the truebb dominates the false bb
						 */
						errs() << "truebb post-dominates falsebb\n";
						controlDepRecording(falsebb, NULL, toCompare, &dt, &pdt, &newcallchain);

					} else {
						errs() << "falsebb not post-dominates truebb\n";
//						analyzeIfDependencies(truebb, falsebb, &params, &dt, &pdt, &newcallchain);
						controlDepRecording(truebb, falsebb, toCompare, &dt, &pdt, &newcallchain);
					}

//					if(params.size() != 0) {
//						mergeIfDependencies(_curDirectiveStr, toCompare, &params);
//					}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// VALUE DEPENDENCY /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

				} else { // theOther is a variable, i.e., the conf variable is compared with another variable
					//don't screw up the _vCache!
					set<Value*> missCache;

					set<string> params;
					if (isFromConfParameter(theOther, &params, &newcallchain, true)) {
						CmpInst::Predicate pred = cmpInst->getPredicate();

						errs() << "_curDirective = " << _curDirectiveStr << "\n";
						errs() << "params.size() = " << params.size() << "\n";
						printSet(&params);

						if (cmpsrc1 == _valueBefore2Steps) {
							string *c1 = &_curDirectiveStr;
							set<string> *c2 = &params;
							mergeMinMaxDependencies(*c1, c2, pred, _curDirectiveStr);

						} else {
							set<string> *c1 = &params;
							string *c2 = &_curDirectiveStr;
							mergeMinMaxDependencies(c1, *c2, pred, _curDirectiveStr);
						}
					}
				}
			}

		} else if (isa<BinaryOperator>(*u)) {
            BinaryOperator *binOper = dyn_cast<BinaryOperator>(*u);

            getInstSrcLocation(binOper, linenum, fileName);
            errs() << "Got it in file " << fileName.str() << ":" << linenum
                   << " of binary operator\n";

            /*
             * To prevent code pattern like:
             *
             *     static char *parse_log_misc_string(apr_pool_t *p, log_format_item *it, const char **sa) {
             *         ....
             *         it->arg = apr_palloc(p, s - *sa + 1);
             *         ....
             *     }
             *
             * where sa is the one we are interested in.
             * In this case, the code would be:
             *
             *     %sub.ptr.lhs.cast = ptrtoint i8* %11 to i32, !dbg !152489
             *     %sub.ptr.rhs.cast = ptrtoint i8* %13 to i32, !dbg !152489
             *     %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast, !dbg !152489
             *     %add = add nsw i32 %sub.ptr.sub, 1, !dbg !152489
             */
            Value* src1 = binOper->getOperand(0);
            Value* src2 = binOper->getOperand(1);

            if (isa<PtrToIntInst>(src1) || isa<PtrToIntInst>(src2)) {
                    errs() << "[warn] Binary operation on pointers. Stop tracking!\n";
                    continue;
            }

			trackDataFlowDep(binOper, faCache, vCache, &newcallchain);

        } else if (isa<PHINode>(*u)) {
			//do nothing
			PHINode* phiNode = dyn_cast<PHINode>(*u);

			/*
			 * check whether it's used as the value in the phi node.
			 * The phi instruction format is "<result> = phi <ty> [ <val0>, <label0>], ..."
			 * make sure v is one of the val
			 */
			unsigned numVal = phiNode->getNumIncomingValues();
			bool isVal = false;
			for (unsigned i = 0; i < numVal; i++) {
				Value* val = phiNode->getIncomingValue(i);
				if (v == val) {
					isVal = true;
				}
			}

			if (isVal) {
				trackDataFlowDep(phiNode, faCache, vCache, &newcallchain);
			}

		} else if (isa<Operator>(*u)) {
			errs() << "ah!!! Operator!\n";
		}
	}
}

/**
 * Sorry, because of the *invoke* instruction (typically in C++),
 * I have to make this function so as to reuse the code
 */
void Misconfig::trackDataFlowCallCheckDep(Instruction* inst, Function* callee, Value* v,
		int callorinvoke,
		FACache* faCache, ValueCache* vCache,
		Context* callchain) {

	if (callorinvoke == CALLINST) {
		assert(isa<CallInst>(inst));
    } else if (callorinvoke == INVOKEINST) {
        assert(isa<InvokeInst>(inst));
    }

    StringRef realName;
    if (!callee || !stripIntrinsic(callee, realName)/*callee->isIntrinsic()*/) {
        /*Cannot find callee, or callee is a llvm inserted intrinsic function*/
        return;
    }

    if (callorinvoke == INVOKEINST) {
        errs() << "wooohooo!!!\n";
    }

    /*for class member functions in C++, this gives you the real function name*/
    string demangledName = DEFINE_CPP ? getDemangledName(realName.str()) : realName.str();

    /*omg, [NOTE] here
     the callee and the caller's number of argument might not be consistent, e.g., in httpd

     **caller:
     1916                 ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, ap_server_conf,
     1917                              "child %" APR_PID_T_FMT " isn't taking over "
     1918                              "slots very quickly (%d of %d)",
     1919                              ap_my_pid, threads_created,
     1920                              threads_per_child);
     i.e.,
     call void (i8*, i32, i32, i32, i32, %struct.server_rec*, i8*, ...)* bitcast (void (i8*, i32, i32, i32, i32, %struct.server_rec.11*, i8*, ...)* @ap_log_error_ to void (i8*, i32\
      , i32, i32, i32, %struct.server_rec*, i8*, ...)*)(i8* getelementptr inbounds ([8 x i8]* @.str9124, i32 0, i32 0), i32 1916, i32 %cond137, i32 7, i32 0, %struct.server_rec* %121,\
       i8* getelementptr inbounds ([57 x i8]* @.str309161, i32 0, i32 0), i32 %122, i32 %123, i32 %124), !dbg !76966

     **callee:
     define void @ap_log_error_(i8* %file, i32 %line, i32 %module_index, i32 %level, i32 %status, %struct.server_rec.11* %s, i8* %fmt, ...) nounwind {
     */
    // To handle the above case, we use the match to indicate whether the caller's argument is found.
    bool match = false;

    /*Iterate all the arguments of the function to find corresponding argument*/
    //arg idx from caller
    unsigned argPosT = 0;
    unsigned numOpsT = inst->getNumOperands();
    Value* argT;
    for (unsigned ai = 0; ai < numOpsT; ai++) {
        argT = inst->getOperand(argPosT++);
        if (argT == v) {
        	errs() << "It is used as the " << argPosT << "th parameter in "
                   << realName.str() << " [caller] \n";
            break;
        }
    }

    //arg idx from callee
    unsigned argPos = 0;
    Value* arg;
    Function::arg_iterator iter = callee->arg_begin(), iter_end = callee->arg_end();
    for (; iter != iter_end; iter++) {
    	arg = inst->getOperand(argPos++);
        if (arg == v) {
        	errs() << "It is used as the " << argPos << "th parameter in "
                           << realName.str() << " [callee] \n";
            match = true; //that means we get a match!
            break;
        }
    }

    if (argPos != argPosT) { //used for debug!
        errs() << "CALLEE-CALLER INCONSISTENT! [function] " + demangledName
        	   << " [argPosT] " << argPosT << "\n";
    }

    if (argPosT > callee->arg_size()) {
    	match = false;
        argPos = argPosT; //because the latter part use argPos
    }

    FunctionTrack needsFurtherTrack = analyzeAPIConstraints(dyn_cast<CallInst>(inst), callee, argPos, NULL, true);

    set<unsigned>* affectedArgs = NULL;

    /**
     * The difference between the two blocks is when match is false, we are tracking an arugment that cannot be found in the function declaration
     * In this case, we assume this function to be a system or lib call (otherwise we do not handle)
     *
     * So, the difference between getSysFuncAffectingList() and getFuncAffectingList() is that the former only check the current known value and
     * will never go inside that function (because there is no mapping from the argument to the parameter)
     *
     * This is to cover cases like
     *
     * declare i32 @snprintf(i8*, i64, i8*, ...) noredzone
     *
     * %call1 = call i32 (i8*, i64, i8*, ...)* @snprintf(i8* %0, i64 %conv, i8* getelementptr inbounds ([5 x i8]* @.str28854, i32 0, i32 0), i32 %2) noredzone, !dbg !1503604
     *
     * so we can know the variable %2 goes to %0, because of the FuncAffectDB entry snprintf,4,1
     *
     * TODO
     * TODO
     * TODO
     *
     * we lost the sfp infomation here.
     * so here we have the assumption that the input and output are peers, i.e., they have the same level in sfp
     * because later we still use sfp for further tracing
     * it cannot handle the following function
     *
     * void foo(A.B.C* input, A.B.C.D* output),  or A.B.C.D bar(A.B.C input);
     *
     * in the above case, the sfp will be changed, and the further tracing will lost
    **/

    if (match == false) {
    	//here we bet the call is a sys-/lib- call, otherwise we have nothing to do
        affectedArgs = getSysAffectedArgumentList(callee, argPos);

    } else {
        set<Function*> entranceFuncCache;
        affectedArgs = getAffectedArgumentList(callee, argPos, &entranceFuncCache);
    }

    errs() << "===== affectedArgs (indices) ======\n";
    errs() << "      " << realName.str() << "\n";
    printSet(affectedArgs);

    bool trackReturn = false;

    /**[NOTE] **important**
     * do this before the affected argument, otherwise the _ufcache would be changed!!!
     */
    if(affectedArgs && affectedArgs->count(0)) {
    	trackReturn = true;
    }

    //here we only care whether to track return
    switch(needsFurtherTrack) {
    case NO_GO_INSIDE_NO_TRACK_RETURN:
    case KNOW_NOTHING:
    case GO_INSIDE_NO_TRACK_RETURN: //do inter procedure data flow checking
    	break;

    case NO_GO_INSIDE_BUT_TRACK_RETURN:
    case GO_INSIDE_AND_TRACK_RETURN:
        trackReturn = true;
        break;

    default:
            break;
    }

    if(match) {
       trackInterProcedureFlow(callee, argPos, inst, NULL, faCache, callchain, trackReturn, true);
    }

    if(trackReturn) {
       //do a sanity check for the type
       //here we want to avoid the dataflow tracking when the input is a basic type and the
       //return value is a struct pointer and in the meantime sfp is none.
       //because in this case, the dataflow tracing will go to all the fields in the structure
       if(isStructType(callee->getReturnType()) && isBasicType(v->getType())) {
    	   ;
       } else {
    	   trackDataFlowDep(inst, faCache, vCache, callchain);
       }
    }

    if (affectedArgs) {
       /* Iterate the affecting list and track all the affected * arguments*/
       set<unsigned>::iterator aaiter = affectedArgs->begin(), aaend = affectedArgs->end();

       for (; aaiter != aaend; aaiter++) {
    	   if ((*aaiter) == 0) { // 0 is the return value
    		   //If return value is affected, track the return value
//             trackDataFlow(inst, sfp, specifications, faCache, vCache);
//             alreadyTrackedRet = true;
//             trackReturn = true;
               errs() << "[info] do nothing...\n";

           } else {
        	   /**
                 * [NOTE] In CallInst, the last operand is the function itself!
                 */
               if (*aaiter < ((int) inst->getNumOperands())) {
            	   Value* affectedArg = inst->getOperand((*aaiter) - 1);

            	   /* go back to the beginning, e.g.,
            	    *
            	    * 764352   %0 = bitcast i32* %value to i8*, !dbg !1033208
            	    * ......
            	    * 764357   call void @opt_set_uint(i8* %0, i8* %1, i32 %2, i64 %3, i8* %4) noredzone, !dbg !1033208
            	    *
            	    * we need to go back to %value because only %value is used in the later part.
            	    * P.S. here we do not care about global var or local var as trackDataFlowCallCheck() do
            	   **/
            	   ConfContainer cc;
            	   cc.getContainer(affectedArg, vCache);
            	   if (cc.type != ConfContainer::NO_INTEREST) {
            		   if(cc.type == ConfContainer::LOCAL_BASIC_VAR){
            			   if(isSameType(affectedArg->getType(), v->getType())) {
            				   trackDataFlowDep(cc.lastLocalVar, faCache, vCache, callchain);
            			   }
            		   }
            	   }

            	   /**
                     * @bug, if it's a constant,
                     * we will go to check a constant value like null to the hell!!!
                     */
                   if(!isa<Constant>(affectedArg)) {
                	   trackDataFlowDep(affectedArg, faCache, vCache, callchain);
                   }
               }
           }
       }
    }
}

/*
 * Check if the encountered call is a system call registered with constraints
 * Actually it checks the function cache _fcache, because syscall constrains
 * are put to the cache at the beginning.
 * If it is, add these constraints to the current value under inspection
 * [note] it should be just checkCall, instead of only syscall. because all
 * 		  the functions are recorded in _fcache, and it is general
 *
 * Parameters:
 * 		unsigned position: the position of the argument. starting from 1
 * 		isDep: true means that it is just a check and does not need to record SYSCALL constraints
 * Return Value:
 *      4 - need to go inside (though find the spec) and need to check the return value
 * 	    3 - need to further go inside but do not need to check the return value, the same as 0
 * 		2 - do not need to go further (find the spec) but need to further track the return value
 * 		1 - stop here!!! no need to track the return value and no need to go inside the function
 * 		0 - nothing found for this function call, i.e., we will further go inside this function to check
 */
Misconfig::FunctionTrack Misconfig::analyzeAPIConstraints(CallInst* inst, Function* func, unsigned position,
		Constraints* specifications, bool isDep) {

	//[IMPORTANT] Get the real func name, because it might be llvm.memset.*
	//            latter, we should use realFuncName.str() instead of func.getNameStr();
	//            Using the latter will cause bugs (we've already encountered)
	//[NOTE]      This is because llvm.memset.* is equivalent to memset
	StringRef realFuncName1;
	stripIntrinsic(func, realFuncName1);

	//I don't know whether it is appropriate, anyway, everything is hacks.
	//could be more aggressive? @@
	string realname = stripFunctionNameSuffix(realFuncName1.str());

//	if(realFuncName.str().compare("memset") == 0) {
//		errs() << "--------position:          " << position << "\n";
//		errs() << "--------func->getNameStr() " << func->getNameStr() << "\n";
//		_ufcache.printFAP();
//	}

	errs() << "----------------------------------------- analyzeAPIConstraints: "
		   << realname << " + " << position << "\n";

	pair<FuncArgPair*, Constraints* >* fcpair = _ufcache.getFapNConstraints(realname, position);
	if (fcpair) {

		errs() << "[info] SYSCALL HIT " << realname << " " << position << "\n";

		if (!isDep) {
			/* get the options */
			if (_strcopycache.find(realname) != _strcopycache.end()) {
				if (inst) {
					for (unsigned i = 0; i < inst->getNumOperands(); i++) {
						Value* op = inst->getOperand(i);
						if (isa<ConstantExpr>(op)) {
							ConstantExpr* cexpr = dyn_cast<ConstantExpr>(op);

							if (cexpr->isDereferenceablePointer()) {
								Constant* co = dyn_cast<Constant>(cexpr->getOperand(0));

//								errs() << *co << "\n";

								Value* op0 = co->getOperand(0);
								if (isa<ConstantArray>(op0)) {
									string constantArgument = getConstantArray(op0);

									errs() << "**********Constant Option***"  << constantArgument << "\n";

									Constraint* optionCon = new OptionConstraint(realname, constantArgument, "OPTION");

									this->_mapDirectiveConstraints.mergeConstraint(this->_curDirectiveStr, optionCon);
								}
							}
						}
					}
				}

			} else if(realname.compare("strtok") == 0 ||
					realname.compare("strtok_r") == 0) {

				Value* v1 = inst->getOperand(1);
				if (isa<ConstantExpr>(v1)) {
					ConstantExpr* cexpr = dyn_cast<ConstantExpr>(v1);

					if (cexpr->isDereferenceablePointer()) {
						Constant* co = dyn_cast<Constant>(cexpr->getOperand(0));
						Value* delim = co->getOperand(0);
						if (isa<ConstantArray>(delim)) {
							string constantArgument = getConstantArray(delim);

							errs() << "**********Constant Delimiter***"  << constantArgument << "\n";

							Constraint* optionCon = new OptionConstraint(realname, constantArgument, "DELIM");

							this->_mapDirectiveConstraints.mergeConstraint(this->_curDirectiveStr, optionCon);
						}
					}
				}

			} else if(realname.compare("strchr") == 0 ||
					  realname.compare("strrchr") == 0 ||
					  realname.compare("strchrnul") == 0) {

				Value* v1 = inst->getOperand(1);
				if(isa<ConstantInt>(v1)) {
					char x = (char) dyn_cast<ConstantInt>(v1)->getZExtValue();
					string s = char2string(x);

					Constraint* optionCon = new OptionConstraint(realname, s, "DELIM");
					this->_mapDirectiveConstraints.mergeConstraint(this->_curDirectiveStr, optionCon);
				}
			}

			errs() << "Found system call function " << realname
				   << " with position " << position << "\n";

			if(realname.find("strto") != string::npos
					&& position == 1) {

				if(isNullPointer(inst->getOperand(1))) {
					//this->_mapDirectiveConstraints.mergeConstraints(this->_curDirectiveStr, cons);
					errs() << "[alarm] this is very suspicious!!!\n";
					string alarm = "strtol with 0 as endptr";

					Constraint* cons = new TypeConstraint(alarm);
					this->_mapDirectiveConstraints.mergeConstraint(this->_curDirectiveStr, cons);
				}

//				errs() << "strto* C-BASE " << *(inst->getOperand(2)) << "\n";
				ConstantInt* cint = dyn_cast<ConstantInt>(inst->getOperand(2));

				if(cint) {
//					errs() << "strto C-BASE " << *(inst->getOperand(2)) << "\n";

					string base = "strto* series C-BASE (oct, hex, dec) " +	int2string(cint->getSExtValue());

					Constraint* cons = new TypeConstraint(base);
					this->_mapDirectiveConstraints.mergeConstraint(this->_curDirectiveStr, cons);
				}
			}

			Constraints* cons = fcpair->second;

			if (cons->size() != 0) {

				//insert to the return parameter
				if(specifications) {
					specifications->insert(cons);
				}

				_mapDirectiveConstraints.mergeConstraints(this->_curDirectiveStr, cons);

				errs() << DUMP_CONSTRAINTS("CHECKSYSCALL");
				dumpConstraints(cons);
				errs() << FINISH_DUMP_CONS;

			} //endif(cons->size()!=0)
		}

		//Check if we need to further go with the return value
		FuncArgPair* fap = _ufcache.getFuncArgPair(realname, position);

		if (fap->_checkReturn && fap->_goInside) {
			return GO_INSIDE_AND_TRACK_RETURN;

		} else if(!fap->_checkReturn && fap->_goInside){
			return GO_INSIDE_NO_TRACK_RETURN;

		} else if(fap->_checkReturn && !fap->_goInside) {
			return NO_GO_INSIDE_BUT_TRACK_RETURN;

		} else if(!fap->_checkReturn && !fap->_goInside) {
			return NO_GO_INSIDE_NO_TRACK_RETURN;
		}
	}

	errs() << "NOT FOUND!!! " << "with [" << realname << ", " << position << "]\n";

	return KNOW_NOTHING;
}

/*
 * Track into a function and checks the data flow from the given formal argument
 *
 * =====Input=====
 *    function:      the function to track into
 * 	  position:      the position of the formal argument
 * 	  sfp:           the struct+field pair telling the interested one
 * 	  faCache:       function+argument cache to prevent infinite loop
 * 	  trackReturn:   the assumption here is we know whether the return value matters
 * ===============
 *
 * === [NOTE] ===
 * StructFieldPairs* sfp will changed inside the function so the caller
 * should initiate a new sfp instance (its the caller's responsibility!)
 */
Constraints* Misconfig::trackInterProcedureFlow(Function *function, unsigned position,
		Instruction* callInst,
		StructFieldPairs* sfp, FACache* faCache,
		Context* callchain,
		bool trackReturn,
		bool isDep) {

	Context newCallChain(callchain);
	newCallChain.pushBack(callInst);

	StringRef realFuncName;
	stripIntrinsic(function, realFuncName);
	string funcName = stripFunctionNameSuffix(realFuncName.str());

	bool ssffpp = (sfp && !sfp->isEmpty())? true : false;

	//to avoid infinite loop
	if(faCache->exist(funcName, position)) {
		return NULL;

	} else { //now it's a new one
		faCache->insert(funcName, position);
	}

	errs() << "======================================== trackInterProcedureFlow: "
		   << funcName << " + " << position// << "(" << function->getNameStr() << ")"
		   << "| ssffpp: " << ssffpp << "\n";
//		   << "| facache.size = " << faCache->size() << "\n";

	//this tells that the following tracing is from trackInterProcedureFlow
	isFromMiddle = false;

//	Function::arg_iterator iarg = function->arg_begin(); //, iend = func->arg_end();
//	Value *valArg;
//	unsigned tmpPos = position;
//
//	while ((tmpPos--) > 0) {
//		valArg = &(*iarg);
//		iarg++;
//
//	} //now the valArg points to interesting argument value.

	Value* valArg = getFunctionArg(function, position);

	if(isDep) {
		ValueCache vCache;
		trackDataFlowDep(valArg, faCache, &vCache, &newCallChain);
		return NULL;

	} else { //is for single directive

		/*
		 * === [NOTE] sfp is not empty ===
		 * In this case, we do not interested in the _ufcache,
		 * because we are not sure whether the tracing is interesting or not.
		 *
		 * So we have to go through the trackDataFlow process.
		 * here, nobody cares the return value;
		 */
		if(sfp && !(sfp->isEmpty())) {
			ValueCache vCache;
			trackDataFlow(valArg, sfp, NULL, faCache, &vCache, &newCallChain);

			return NULL;

		} else {

			// -- [Previous Assumption]: This function definitely not exist in cache, because it should be preceded by checkSysCall()
			//    [NOTE] **NO** such assumption any more!!!!!!
			//
			// In the new implementation, the constraints associated with a function+argument pair (i.e., in _ufcache) can be changed or added
			// This is because we need give more flexibility to develpers mainly for annotation.
			// For example, if the developer wants to annotate opt_set_bool, he can put <opt_set_bool, 1, BOOL> in the semantic DB,
			// However, he still need PTest to go inside the opt_set_bool function to extract the specifications

			Constraints* newCons = new Constraints();

			pair<FuncArgPair*, Constraints* >* fcpair = _ufcache.getFapNConstraints(funcName, position);

			if(!fcpair) { //does not exist, that means it's a brand new function with no annotation

				//insert it into function cache, so we won't be trapped into recursive call
				FuncArgPair* fap = new FuncArgPair(funcName, position, trackReturn, 0);
				_ufcache.insertFuncConstraints(fap, newCons);

//				/*Use funcDataFlowAnalysis() instead of trackDataFlow*/
//				bool ret = funcDataFlowAnalysis(func, position, newCons, isFuncMapping, sfp);

				ValueCache vCache;
				trackDataFlow(valArg, sfp, newCons, faCache, &vCache, &newCallChain);

				return newCons;

			} else { //the function already exists in the _ufcache, so whether go further depends on _goInside

				FuncArgPair* fap = fcpair->first;
				fap->_checkReturn = trackReturn;

				if(fap->_goInside == true) {
					fap->_goInside = false; //we check at most one time

//					funcDataFlowAnalysis(func, position, newCons, isFuncMapping, sfp);
					ValueCache vCache;
					trackDataFlow(valArg, sfp, newCons, faCache, &vCache, &newCallChain);

					//update the current constraints in _ufcache
					Constraints* oldCons = fcpair->second;
					oldCons->insert(newCons);

					delete newCons;

					return oldCons;

				} else {
					//impossible to be here!!! because if exist, check system call should be handled earlier.
					//Here means the function exist and we need to check.

					//[NOTE] the only explanation is we have the implementation of system call inside
					//       the bitcode, e.g., memcpy
					//in this case, although the previous system call check get it, you go will fall in here
					errs() << "[panic] it's impossible to be here!!!\n";
					assert(false);

					return NULL;
				}
			}
		}
	}
}

void Misconfig::updateSFPMap(map<Value*, unsigned>* sfpMap, Value* src, Value* dst) {
	if(sfpMap->count(dst) == 0) {
		assert(sfpMap->count(src) != 0);

		sfpMap->operator [](dst) = sfpMap->operator [](src);

	} else {
		errs() << "[warn] In updateSFPMap, sfpMap->count(dst) != 0\n";
	}
}

void Misconfig::updateSFPMap(map<Value*, unsigned>* sfpMap, Value* src1, Value* src2, Value* dst) {
	if(sfpMap->count(dst) == 0) {
		assert(sfpMap->count(src1) != 0 || sfpMap->count(src2) != 0);

		unsigned idx1 = sfpMap->count(src1) == 0 ? -1 : sfpMap->operator [](src1);
		unsigned idx2 = sfpMap->count(src2) == 0 ? -1 : sfpMap->operator [](src2);

		sfpMap->operator [](dst) = max(idx1, idx2);

	} else {
		errs() << "[warn] In updateSFPMap, sfpMap->count(dst) != 0\n";
	}
}

Constraints* Misconfig::analyzeStructConstraint(StructFieldPairs* isfp) {
	if(!isfp || isfp->isEmpty()) return NULL;

	list<string>* stnames = isfp->getStructs();
	list<unsigned>* fields = isfp->getFields();

	list<string>::reverse_iterator stIter = stnames->rbegin(), stEnd = stnames->rend();
	list<unsigned>::reverse_iterator idxIter = fields->rbegin(), idxEnd = fields->rend();

	//we want to go back to the last sf pair with out a wildcard
	for( ;
		stIter != stEnd, idxIter != idxEnd;
		stIter++, idxIter++) {

		if((*stIter).compare("*") != 0 && (*idxIter) != UINT_MAX) {
			break;
		}
	}

	return analyzeStructConstraint(*stIter, *idxIter);
}

Constraints* Misconfig::analyzeStructConstraint(string structname, unsigned field) {
	Constraints* res = new Constraints();

	StructCache::sc_iterator iter = _scons.findStruct(structname, field);

	if (iter != _scons.end()) {
		errs() << "Found struct related constraint\n";
		Constraints* cons = (*iter)->second;

		res->insert(cons);

//		unsigned linenum = 0;
//		StringRef fileName;

		errs() << DUMP_CONSTRAINTS("CHECK_STRUCT_CONSTRAINTS");
		dumpConstraints(cons);
		errs() << FINISH_DUMP_CONS;

		return res;

	} else {
		delete res;
		return NULL;
	}
}

Constraints* Misconfig::analyzeStructConstraint(Instruction* inst) {
//	Constraints* res = new Constraints();

	if(isa<GetElementPtrInst>(inst)) {
		GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(inst);

		if(gep->getNumOperands() == 3) {

			int field = -1;
			Value* v2 = gep->getOperand(2);
			if(isa<ConstantInt>(v2)) {
				field = dyn_cast<ConstantInt>(v2)->getSExtValue();

				Type* t = gep->getOperand(0)->getType();

				//haha, stupid qiqi
				Type* elemt = isa<PointerType>(t) ? dyn_cast<PointerType>(t)->getElementType() : t;

				if (isa<StructType>(elemt)) {
					StructType * selemt = dyn_cast<StructType>(elemt);

					if(!selemt->isLiteral()) { //literal has no name

						StringRef tName = stripStructName(selemt->getName());
						return analyzeStructConstraint(tName, field);
//						StructCache::sc_iterator iter = _scons.findStruct(tName, field);
//
//						if (iter != _scons.end()) {
//							errs() << "Found struct related constraint\n";
//							Constraints* cons = (*iter)->second;
//
//							res->insert(cons);
//
//							unsigned linenum = 0;
//							StringRef fileName;
//
//							getInstSrcLocation(inst, linenum, fileName);
//							errs() << "Found Constraint at " << fileName.str() << ":" << linenum << "\n";
//
//							errs() << DUMP_CONSTRAINTS("CHECK_STRUCT_CONSTRAINTS");
//							dumpConstraints(cons);
////							this->_mapDirectiveConstraints.mergeConstraints(this->_curDirectiveStr, cons);
//							errs() << FINISH_DUMP_CONS;
//
//							return res;
//						}
					}
				}
			}
		}

	} else if(isa<ConstantExpr>(inst)) {
		ConstantExpr* cexpr = dyn_cast<ConstantExpr>(inst);
		if(cexpr->isDereferenceablePointer()) {

		}
	}

//	delete res;
	return NULL;
}

/* Get the actually called function, when there is pointer cast that surrounds
 * the call.
 * This function may be not needed any more because llvm now has a %call
 * that stores the direct return value of a function call. And later this
 * %call is casted. Not like before, where call is directly wrapped with
 * a cast.
 * But we still put it here just in case
 * Input:
 * 		CallInst* callinst: the calling instruction to be inspected
 * Return:
 * 		The called function
 */
Function* Misconfig::getRealCalledFunction(CallInst* callinst) {
	Function* callee = callinst->getCalledFunction();
	if (!callee) {
		int numOperands = callinst->getNumOperands();
		Value *v = callinst->getOperand(numOperands - 1);
		Value *stripped = v->stripPointerCasts();
		callee = dyn_cast<Function>(stripped);
	}
	return callee;
}

/* Iterate all the global variables in this module and
 * find the one with the given name
 * Input:
 * 		StringRef& gvarName: the name of the global variable
 * 		mode: whether to try the c++filt
 * 	Return:
 * 		the GlobalVariable structure
 * 		NULL if nothing found
 */
GlobalVariable* Misconfig::getGlobalVariable(StringRef& gvarName) {

//	StringRef realname = gvarName;
//	if(demangledGlobalMap->find(gvarName.str()) != demangledGlobalMap->end()) {
//		realname = StringRef(demangledGlobalMap->operator [](gvarName.str()));
//	}

	Module::global_iterator iter = _module->global_begin(), end = _module->global_end();

	for (; iter != end; iter++) {
		if ((iter->getName()).equals(gvarName)) {
			errs() << " WE GOT THIS VARIABLE " << iter->getName() << "\n";
			return &(*iter);

		} else if (DEFINE_CPP && iter->getName().find(gvarName, 0) != StringRef::npos) {
			string symbol2name = getDemangledName(iter->getName().str());
			errs() << "[info] cplus_demangle " << symbol2name << " ? " << gvarName.str() << "\n";

			if (symbol2name.compare(gvarName.str()) == 0) {
				return &(*iter);
			}
		}
	}

	return NULL;
}

/* Get the source code location of a specific instruction.
 * This can be put into utility class later, if we will have that class:)
 * Input:
 * 		Instruction* inst: the instruction we target on
 * Output:
 * 		unsigned& linenum: the linenumber of the instruction
 * 		StringRef& filename: the file name of the source code
 */
void Misconfig::getInstSrcLocation(Instruction* inst, unsigned& linenum,
		StringRef& filename) {
	MDNode *dbg = inst->getMetadata(LLVMContext::MD_dbg);
	DILocation loc(dbg);
	linenum = loc.getLineNumber();
	filename = loc.getFilename();
}

//defines how one variable would affect another variable
void Misconfig::_initializeFuncAL() {

////	list<int> *al = new list<int>();
////	al->push_back(1);
//	pair<string, int> tmp("memcpy", 2); 
//	_funcAMap[tmp] = 1;

	ifstream infile(AFFECT_FUNCTION);

	string line;
	string token;

	string funcName;
	int src, dst;

	while (getline(infile, line)) {
		line = trim(line);
		if (line.size() == 0 || line.find_first_of('#', 0) == 0
				|| line.find_first_of('?', 0) == 0
				|| line.find_first_of('!', 0) == 0) {
			continue;
		}

		stringstream ss(line);

		//we know it should be 3 values: funcName,src,dst
		//function name
		getline(ss, token, ',');
		token = trim(token);
		funcName = token;

		//src
		getline(ss, token, ',');
		token = trim(token);
		src = atoi(token.c_str());

		//structure field
		getline(ss, token, ',');
		token = trim(token);
		dst = atoi(token.c_str());

		//put it into the _funcAMap
		_funcAMap.insert(funcName, src, dst);

//		if (_funcAMap.count(pair<string, int>(funcName, src))) {
//			pair<string, int> tmp(funcName, src);
//			list<int> *al = _funcAMap[tmp];
//			al->push_back(dst);
//
//		} else {
//			list<int> *al = new list<int>();
//			al->push_back(dst);
//			pair<string, int> tmp(funcName, src);
//			_funcAMap[tmp] = al;
//
//			//errs() << "what the hell you put?! " << tmp.first << " " << tmp.second << "\n";
//		}
	}
}

/*
 * We first put it here to make sure we still have the old
 * version. This is for tracking function parameter affects
 * InterestNodes is list<DFNode*>
 * We changed back to make it a standalone function, in order
 * to make the logic simple and clear
 */
set<unsigned>* Misconfig::getFuncAffectedArgumentList(Function *func, unsigned position, set<Function*>* entrances) {
	//the return results
	set<unsigned>* res = new set<unsigned>();

	/*
	 * Build an array of interesting nodes. each interest node list corresponding to the taint set of each argument
	 * Basically, we have the following structure
	 *
	 * arg0 -> list<Value*>()
	 * arg1 -> list<Value*>()
	 * arg2 -> list<Value*>()
	 * arg3 -> list<Value*>()
	 * ...  -> list<Value*>()
	 *
	 * In this way, we can know how the arguments interact with each other.
	**/
	int numArgs = func->getFunctionType()->getNumParams();
	ArgMap* argMap = new ArgMap(numArgs);

	//initialize the argMap by adding the corresponding arguments
	Function::arg_iterator iiter = func->arg_begin(), iend = func->arg_end();
	for(int i=0; iiter != iend; iiter++, i++) {
		argMap->push2TaintedSet(i, &(*iiter));
	}

	/*
	 * The analysis goes like this: we have an interesting value set. For each
	 * instruction we meet, depending on its semantic, we will find whether
	 * it could be affected by any value in the interesting value set. For example,
	 * if it is a STORE instruction, if the source is in the set, we also put
	 * the target into the set. It's like static tainting analysis. So after
	 * the traverse, we can find every use  of our input argument
	 */

	inst_iterator iter = inst_begin(func), end = inst_end(func);
	for (; iter != end; iter++) {
		Instruction* inst = &(*iter);
//		errs() << *inst << "\n";

		if (isa<StoreInst>(inst)) {
			Value* src = inst->getOperand(0);
			Value* tgt = inst->getOperand(1);

			set<unsigned>* srcTSets = argMap->findCorrespondingTaintedSet(src);
			set<unsigned>* tgtTSets = argMap->findCorrespondingTaintedSet(tgt);

			//here we check the case that tgt is already known which is associated with some argument
			//if src is from the major one, we can know that the arguments associated with tgt is affected
			if(tgtTSets && srcTSets) {
				if(srcTSets->count(position-1)) {
					set<unsigned>::iterator siter = tgtTSets->begin(), send = tgtTSets->end();
					for(; siter != send; siter++) {
						res->insert( (*siter)+1 );
					}
				}
			}

			if (srcTSets) {
//				if(findDFNodeList(interestNodes, srcNode)!=interestNodes->end()){
				/* If the target is from getelementptr instruction, then we
				 * go and check the source of the pointer, as well as the
				 * possible index used
				 */
				if (isa<GetElementPtrInst>(tgt)) {
					GetElementPtrInst * gepi = dyn_cast<GetElementPtrInst>(tgt);

					if (!isNotInterestStructField(gepi, &_listPredefinedStructField)) {

						if (gepi->getNumOperands() == 3) {

							Value* epsrc = gepi->getOperand(0);
							if (isa<LoadInst>(epsrc)) {
								LoadInst * lins = dyn_cast<LoadInst>(epsrc);

								Value* ldsrc = lins->getOperand(0);
								argMap->push2TaintedSets(srcTSets, ldsrc);
							}
						}
					}
				}

				argMap->push2TaintedSets(srcTSets, tgt);

				delete srcTSets;
			}

			if(tgtTSets) {
				delete tgtTSets;
			}

		} else if (isa<LoadInst>(inst) || isa<CastInst>(inst)) {
			// If it is load instruction, then check whether the source is of interest.
			// If yes, also put the instruction into the list
			Value* src = inst->getOperand(0);

			set<unsigned>* tsets = argMap->findCorrespondingTaintedSet(src);

			if (tsets) {
				//if(findDFNodeList(interestNodes, &tmpNode)!=interestNodes->end()){
				argMap->push2TaintedSets(tsets, inst);
				delete tsets;
			}

		} else if (isa<GetElementPtrInst>(inst)) {

			GetElementPtrInst* geptr = dyn_cast<GetElementPtrInst>(inst);
			if (isNotInterestStructField(geptr, &_listPredefinedStructField)) {
				continue;
			}

			if (inst->getNumOperands() == 3) {
				/* We check the source and index
				 * If any of them comes from interestNodes
				 * we add this instruction to interestNodes
				 */
				/*A field-sensitive approach*/
				Value* src1 = inst->getOperand(0);

				//check sysutil.o.ll:vsf_sysutil_sockaddr_set_port()
				//This part is to determine the structure constraint
				/*I comment it because seems we should not check it
				 * here. Because we don't know thether it is of interest
				 * or not yet. We do it in store
				 */
				/*						Type* t = src1->getType();
				 if(index>=0){
				 checkStructConstraint(inst, t, index);
				 }*/

				if (isa<LoadInst>(src1)) {
					LoadInst * lins = dyn_cast<LoadInst>(src1);
					Value* lsrc = lins->getOperand(0);

					set<unsigned>* setset = argMap->findCorrespondingTaintedSet(lsrc);

					if (setset) {
						argMap->push2TaintedSets(setset, inst);
						delete setset;
					}
				}
			}

		} else if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
			Function* callee;

			if (isa<CallInst>(inst)) {
				CallInst *callinst = dyn_cast<CallInst>(inst);
				callee = getRealCalledFunction(callinst);

			} else if (isa<InvokeInst>(inst)) {
				InvokeInst *invokeinst = dyn_cast<InvokeInst>(inst);
				callee = invokeinst->getCalledFunction();
			}

			StringRef realName;
			if (!callee || !stripIntrinsic(callee, realName)) {
				continue;
			}

			errs() << "[info] in analyzeFuncAffects: " << func->getNameStr()
				   << " >> " + realName.str() << "\n";

			//to avoid deadlock, because the callee is where here comes from (A->B->C->A).
			if (entrances->find(callee) != entrances->end()) {
				continue;
			} else {
				entrances->insert(callee);
			}

			/*
			 *
			 * Storing the mapping info between the argpos of current function and the callee function.
			 * **[NOTE]** In this pair, we start the argument from 0. Sorry about the confusion...
			 * @stupid qiqi!!!
			**/
			list<pair<unsigned, unsigned>*> largPair;

			//stores the parameters in the callee function that comes from the main argument of the caller
			list<unsigned> subMajors;

			/*Let's see if any of the argument is affected by things of interests*/
			Function::arg_iterator argIter = callee->arg_begin(), argEnd = callee->arg_end();
			for (unsigned argPos=0;
					argIter != argEnd;
					argIter++, argPos++) {

				Value* argValue = inst->getOperand(argPos);

				set<unsigned>* tsets = argMap->findCorrespondingTaintedSet(argValue);

				if (!tsets) {
					continue;
				}

				if (tsets->count(position-1)) {
					subMajors.push_back(argPos + 1);

				} else {
					//We are trying to get which original argument (in the current function)
					//corresponds to which argument in the callee.
					//Well, now we just pick one if there exists several.
					//TODO: shall we add all?

					unsigned pickoneIN = *(tsets->begin());
//					int arIndex = argMap->getArgIndex(pickoneIN);
					largPair.push_back(new pair<unsigned, unsigned>(pickoneIN + 1, argPos + 1));
				}

				delete tsets;

			} //end for(argIter)

			//If an argument is from the main argument, get the affecting factors
			//@Terminology: main argument means the argument we are tracing.
			if (subMajors.size() != 0) {
				list<unsigned>::iterator smIter = subMajors.begin(), smEnd = subMajors.end();

				set<unsigned> al;
				for (; smIter != smEnd; smIter++) {
					set<unsigned>* tmpal = getAffectedArgumentList(callee, *smIter, entrances);

//					mergeList(&al, tmpal);
					copySet(&al, tmpal);

					if (tmpal != NULL && tmpal->size() > 0) {

						Value* sm = inst->getOperand(*smIter - 1);
						set<unsigned>* tsetset = argMap->findCorrespondingTaintedSet(sm);

						//add the related ones to the taintlist
						set<unsigned>::iterator alIter = al.begin(), alEnd = al.end();
						for (; alIter != alEnd; alIter++) {
							if (*alIter <= inst->getNumOperands()) {
								if (*alIter == 0) {
									argMap->push2TaintedSets(tsetset, inst);
								} else {
									argMap->push2TaintedSets(tsetset, inst->getOperand(*alIter - 1));
								}
							}
						}
					}
				}

				list<pair<unsigned, unsigned>*>::iterator argIter = largPair.begin(),	argEnd = largPair.end();

				for (; argIter != argEnd; argIter++) {
					if(al.count((*argIter)->second)) {
						errs() << " ummm " << (*argIter)->first;
						res->insert((*argIter)->first);
					}
				}
			}

			//clean largPair
			list<pair<unsigned, unsigned>*>::iterator pIter = largPair.begin(), pEnd = largPair.end();
			for(; pIter != pEnd; pIter++) {
				delete (*pIter);
			}

		} else if (isa<ReturnInst>(inst)) {
			//We need to track this, for example, forthe str_getbuf used
			//when doing str_chdir(), on vsftpd: tunable_secure_chroot_dir
			ReturnInst* rInst = dyn_cast<ReturnInst>(inst);
			Value* retVal = rInst->getReturnValue();

			set<unsigned>* tsets = argMap->findCorrespondingTaintedSet(retVal);

			if (!tsets) {
				continue;
			}

			/*
			 * Here are 2 cases, corresonding to the "if" and "else" branch
			 *
			 * 1. If the returned value is within the main set, of course we need to further track
			 *    the return value of the function.
			 *
			 * 2. If the returned value is within the set of other argument, and that argument is
			 *    affected by the main argument, and we also need to further tracing the return value
			 *
			 *    e.g., for example, the affected argument is (2, 4), the main arugment is 1,
			 *          and the returned value is from 2, so we do care the return value.
			**/
			if (tsets->count(position-1)) {
				res->insert(0);

			} else {
//				errs() << "res -- \n";
//				printSet(res);
//				errs() << "tsets -- \n";
//				printSet(tsets);

				//**NOTE** that the indices in tsets and those in res is different!!!
				// - in tsets, the indices start with 0 (it records which taintedset in argMap)
				// - in res,   the indices start with 1 (0 is reserved for the return value)
				//I agree this is stupid and confusing but it's too hard to change because of legacy code
				set<unsigned>::iterator iter = res->begin(), end = res->end();

				for(; iter != end; iter++) {
					if(tsets->count((*iter)-1)) {
						res->insert(0);
						break;
					}
				}
			}

			delete tsets;

		} else if (isa<BinaryOperator>(inst)) {
			BinaryOperator * binOp = dyn_cast<BinaryOperator>(inst);
			/* Instructions such as add
			 * We add them to interesting list if any of the
			 * operands is in the list
			 */
			Value* op1 = binOp->getOperand(0);
			Value* op2 = binOp->getOperand(1);

			set<unsigned>* setset1 = argMap->findCorrespondingTaintedSet(op1);
			set<unsigned>* setset2 = argMap->findCorrespondingTaintedSet(op2);

			if (setset1) {
				argMap->push2TaintedSets(setset1, inst);
				delete setset1;
			}

			if (setset2) {
				argMap->push2TaintedSets(setset2, inst);
				delete setset2;
			}
		}

	} //end all instruction iteration

	if(res->count(position-1)) {
		errs() << "[panic] the result set has the input argument itself!!\n";
	}

	return res;
}

int Misconfig::getArgIndex(InterestNodes** arrINodes, InterestNodes* n, int size) {

	for (int i = 0; i < size; i++) {
		if (arrINodes[i] == n) {
			return i + 1;
		}
	}

	return -1;
}

set<unsigned>* Misconfig::getSysAffectedArgumentList(Function* func, unsigned argPos) {
	StringRef tmpStr;
	stripIntrinsic(func, tmpStr);

	string funcName = stripFunctionNameSuffix(tmpStr.str());

	return getSysAffectedArgumentList(funcName, argPos);
}

//Return it if exists (already computed or pre-know)
set<unsigned>* Misconfig::getSysAffectedArgumentList(string funcname, unsigned argPos) {

	//the _funcAMap (map of function & argument) acts as a cache
	//each time we meet a function and argument, we do an analyzeFuncAffects,
	//and know the other arguments that is infected by this argument and put the knowledge into the _funcAMap
	//Thus, the next time we do not need to do it but just get it from _funcAMap
	//[comments] qiqi is smart here!
	set<unsigned>* affectedSet = _funcAMap.getAfftectedSet(funcname, argPos);
	if (affectedSet) {
		errs() << "[info] *** can we really reach this point?!!!\n";
		errs() << "       *** " << funcname << ", " << argPos << " ***\n";
//		errs() << "       *** " << tmp.first << ", " << tmp.second << " ***\n";

		set<unsigned>::iterator iter = affectedSet->begin(), end = affectedSet->end();
		for (; iter != end; iter++) {
			errs() << *iter << " ";
		}
		errs() << "\n";

		return affectedSet;
	}

	return NULL;
}

set<unsigned>* Misconfig::getAffectedArgumentList(Function* func, unsigned argPos, set<Function*>* entrances) {
	StringRef tmpStr;
	stripIntrinsic(func, tmpStr);

	string funcName = stripFunctionNameSuffix(tmpStr.str());

	set<unsigned>* res = getSysAffectedArgumentList(funcName, argPos);

	if (res == NULL) { //If doesn't already exist, perform the analysis, put into the map, and return it.

//		//Return it if exists (already computed or pre-know)
//		pair<string, int> tmp(tmpStr.str(), argPos);

		entrances->insert(func);

		res = getFuncAffectedArgumentList(func, argPos, entrances);

//		_funcAMap[tmp] = res;
		_funcAMap.insert(funcName, argPos, res);
	}

	return res;
}

void Misconfig::controlDepRecording(BasicBlock* tbb, BasicBlock* fbb,
		string reference,
		DominatorTree* dt, PostDominatorTree* pdt,
		Context* callchain) {

	PerBlockControlDepInfo* cdepInfo_t = NULL;
	PerBlockControlDepInfo* cdepInfo_f = NULL;

	if(tbb) {
		cdepInfo_t = new PerBlockControlDepInfo();
		controlDepRecording(tbb, reference, cdepInfo_t, dt, pdt, callchain);
	}

	if (fbb) {
		string peerRef = "!" + reference;
		cdepInfo_f = new PerBlockControlDepInfo();
		controlDepRecording(fbb, peerRef, cdepInfo_f, dt, pdt, callchain);
	}

	//if both truebb and falsebb has the same configuration directives, just delete it
	//because it is not a if-dependency
	_controlDepAggr->checkConflicts(cdepInfo_t, cdepInfo_f);
	_controlDepAggr->add2List(cdepInfo_t);
	_controlDepAggr->add2List(cdepInfo_f);
}

void Misconfig::controlDepRecording(BasicBlock* bb,
		string reference,
		PerBlockControlDepInfo* output,
		DominatorTree* dt, PostDominatorTree* pdt,
		Context* callchain) {

	/////////////////////////////////////////////////////////////////////////////////////////
	//first, we want to get all the basic blocks that is dominiated by the input bb
	//we put these basic blocks into realbblist for later usage
	list<BasicBlock*> bblist;
	bblist.push_back(bb);

	list<BasicBlock*> realbblist;

	//avoid infinite loop and repeated calculation
	set<BasicBlock*> bbCache;

	while (bblist.size() > 0) {
		//remove the 1st element
		BasicBlock* bbp = *bblist.begin();
		bblist.pop_front();

		if (bbCache.find(bbp) != bbCache.end()) {
			continue;
		} else {
			bbCache.insert(bbp);
		}

		if (dt->dominates(bb, bbp)) {
			realbblist.push_front(bbp);

			list<BasicBlock*>* tmpll = getSuccessors(bbp);
			mergeList(&bblist, tmpll);
			delete tmpll;
		}
	}
	//By now, we have all the basic blocks in the reversed order
	//Note that the reason we did not do checking when get all the bbq is
	//because we want to do backward tracing from the end to the beginning.
	/////////////////////////////////////////////////////////////////////////////////////////

	//@let's add them to the PerBlockControlDepInfo
	copyList2Set(&(output->domingBB), &realbblist);

	//for backward tracing: also avoid repeated calculation as well as infinite loop
	//missCache tells you that this instruction is already visited,
	//as long as you see sth that can be found in the miss cache, you ignore it.
	set<Value*> missCache;

	list<BasicBlock*>::iterator ir = realbblist.begin(), er = realbblist.end();
	for (; ir != er; ir++) {

		//get a BasicBlock list with all the instructions in a reversed order
		list<Instruction*> reversebb;
		BasicBlock::iterator iterb = (*ir)->begin(), itere = (*ir)->end();
		for (; iterb != itere; ++iterb) {
			reversebb.push_front(iterb);
		}

		list<Instruction*>::iterator ritb = reversebb.begin(), rite = reversebb.end();
		for (; ritb != rite; ritb++) {
			Instruction* tmpInst = *ritb;

			/*Check if it is already cached to avoid infinite loop*/
			if (missCache.find(tmpInst) != missCache.end()) {
				continue;
			}

			missCache.insert(tmpInst);

			/****************current what we are doing**********************************/
			if (isa<BranchInst>(tmpInst)) {
				BranchInst* binst = dyn_cast<BranchInst>(tmpInst);
				if (!binst->isConditional()) {
					continue;
				}

				Value* cond = binst->getCondition();
				//First we are going to check possible
				if (isa<CmpInst>(cond)) {
					missCache.insert(cond);

					CmpInst* cmpInst = dyn_cast<CmpInst>(cond);

					Value* cmpsrc1 = cmpInst->getOperand(0);
					Value* cmpsrc2 = cmpInst->getOperand(1);

					set<string> tpara1;
					if(isFromConfParameter(cmpsrc1, &tpara1, callchain, false)) {
//						params->insert(tpara1.begin(), tpara1.end());
						output->domedParameters.insert(tpara1.begin(), tpara1.end());
					}

					set<string> tpara2;
					if(isFromConfParameter(cmpsrc2, &tpara2, callchain, false)) {
//						params->insert(tpara2.begin(), tpara2.end());
						output->domedParameters.insert(tpara2.begin(), tpara2.end());
					}
				}

			} else if (isa<StoreInst>(tmpInst)) {
				//do nothing

			} else if (isa<CallInst>(tmpInst)) { //function call
				CallInst* callInst = dyn_cast<CallInst>(tmpInst);
//				Function* f = callInst->getCalledFunction();
				Function* f = getCalledFunction(callInst);

				if (f) {
					if (_func2ConfUsageMap.count(f) != 0) {
						errs() << "--> inserting " << f->getNameStr() << "\n";
						output->domedCallees.insert(f);

						set<string>* tpara = _func2ConfUsageMap[f];
//						params->insert(tpara->begin(), tpara->end());
						output->domedParameters.insert(tpara->begin(), tpara->end());
					}

					for (unsigned i = 0; i < callInst->getNumArgOperands();	i++) {
						Value* v = callInst->getArgOperand(i);
						set<string> tpara;

						if (isFromConfParameter(v, &tpara, callchain, false)) {
//							params->insert(tpara.begin(), tpara.end());
							output->domedParameters.insert(tpara.begin(), tpara.end());
						}
					}

				} else { //should be a function pointer
					; //do nothing
				}

			} else { //e.g., load or cast

				for (unsigned i = 0; i < tmpInst->getNumOperands(); i++) {
					Value* v = tmpInst->getOperand(i);
					set<string> tpara;
					if (isFromConfParameter(v, &tpara, callchain, false)) {
//						params->insert(tpara.begin(), tpara.end());
						output->domedParameters.insert(tpara.begin(), tpara.end());
					}
				}
			}
		}
	}
}

void Misconfig::mergeMinMaxDependencies(string d1, string d2,
		CmpInst::Predicate pred, string currDirective) {

	if (d1.compare(d2) != 0) {
		Constraint* newCon = new MinMaxDependencyConstraint(d1, d2, pred);
		_mapDirectiveConstraints.mergeConstraint(currDirective, newCon);

		newCon->dump();
	}
}

void Misconfig::mergeMinMaxDependencies(set<string>* d1, string d2,
		CmpInst::Predicate pred, string currDirective) {

	set<string>::iterator iter = d1->begin(), end = d1->end();
	for (; iter != end; iter++) {
		mergeMinMaxDependencies(*iter, d2, pred, currDirective);
	}
}

void Misconfig::mergeMinMaxDependencies(string d1, set<string>* d2,
		CmpInst::Predicate pred, string currDirective) {
	set<string>::iterator iter = d2->begin(), end = d2->end();
	for (; iter != end; iter++) {
		mergeMinMaxDependencies(d1, *iter, pred, currDirective);
	}
}

/**
 * o
 */
void Misconfig::mergeIfDependency(string dominatingParam, string dominatingRef, string dominatedParam) {

	if (dominatingParam.compare(dominatedParam) != 0) {
		errs() << "[" << dominatedParam << "] depends on ["
			   << dominatingParam << ", " << dominatingRef << "]\n";

		Constraint* newCon = new IfDependencyConstraint(dominatedParam, dominatingParam, dominatingRef);
		_mapDirectiveConstraints.mergeConstraint(dominatingParam, newCon);

		newCon->dump();
	}
}

void Misconfig::mergeIfDependencies(string dominatingParam, string dominatingRef, set<string>* dominatedParams) {

	set<string>::iterator iter = dominatedParams->begin();
	set<string>::iterator end  = dominatedParams->end();

	for (; iter != end; iter++) {
		if(iter->length() != 0) {
			mergeIfDependency(dominatingParam,  dominatingRef, *iter);
		}
	}
}

/**
 * the idea of decide whether a variable is from config parameter is complicated.
 * We mainly try two things here:
 * 1. through backward tracking, the variable can reach a global variable associated with some config parameter
 * 2. if not, we try to see whether the variable comes from a argument of the current function (by tainting).
 *    if it is, we use of the call chain to go back to the upper level function to check recursively until
 *    the call chain is empty.
 *
 * =====input=====
 * checkUsage: decide whether we check the usage of the value v,
 * the typical setting is false, but for min-max comparison, one common pattern is like
 *
 * min < a < max, i.e., there's an intermediate value (a) compared with min and max,
 *
 * so, in this case, we set checkUsage to be true, so it will go one step further.
 * ===============
 */
bool Misconfig::isFromConfParameter(Value* v, set<string>* params, Context* callchain, bool checkUsage) {

	set<Value*> misscache;
	bool ccpbbt = checkConfParameterByBackTrack(v, params, &misscache, checkUsage);

	if(ccpbbt) {
		return true;

	} else {
		//			errs() << "argsrc = {";
		//			printSet(&argsrc);
		//			errs() << "call chain ->\n";
		//			callchain->print();

		Value* last = callchain->getLast();
		if(isa<CallInst>(last) || isa<InvokeInst>(last)) {

			set<unsigned> argsrc;
			bool iffa = isFromFunctionArgument(v, &argsrc);

			if(iffa) {
				set<unsigned>::iterator iter = argsrc.begin(), end = argsrc.end();
				for(; iter != end; iter++) {
					Value* arg = NULL;

					if(isa<CallInst>(last)) {
						CallInst* callinst = dyn_cast<CallInst>(last);

						unsigned ai = *iter - 1;
						if(ai >= callinst->getNumArgOperands()) {
//							errs() << "[fatal] ===> " << *callinst << " | " << int2string(ai) << "\n";
//							assert(false);

							/*
							 * This is possible because we might have sth like
							 * %call = call %struct._vfiler.312364* @sk_get_vfiler89954()
							**/
							return false;
						}

						arg = callinst->getOperand(ai);

					} else if(isa<InvokeInst>(last)) {
						InvokeInst* invokeInst = dyn_cast<InvokeInst>(last);
						arg = invokeInst->getOperand(*iter-1);
					}

					assert(arg != NULL);

					Context newcallchain(callchain);
					newcallchain.popLast();
					return isFromConfParameter(arg, params, &newcallchain, false);
				}
			}

		} else {
			return false;
		}

		return false;
	}
}

/**
 * we want to know whether the Value* v comes from an function parameter
 * Here we do not do inter-prodecure analysis because we do not care
 */
bool Misconfig::isFromFunctionArgument(Value* v, set<unsigned>* argsrc) {
	if(!isa<Instruction>(v)) {
		return false;
	}

	Instruction* i = dyn_cast<Instruction>(v);
	Function* function = i->getParent()->getParent();

	/*
	 * Build an array of interesting nodes. each interest node list corresponding to the taint set of each argument
	 * Basically, we have the following structure
	 *
	 * arg0 -> list<Value*>()
	 * arg1 -> list<Value*>()
	 * arg2 -> list<Value*>()
	 * arg3 -> list<Value*>()
	 * ...  -> list<Value*>()
	 *
	 * In this way, we can know how the arguments interact with each other.
	**/
	int numArgs = function->getFunctionType()->getNumParams();
	ArgMap* argMap = new ArgMap(numArgs);

	//initialize the argMap by adding the corresponding arguments
	Function::arg_iterator iiter = function->arg_begin(), iend = function->arg_end();
	for(int i=0; iiter != iend; iiter++, i++) {
		argMap->push2TaintedSet(i, &(*iiter));
	}

	/*
	 * The analysis goes like this: we have an interesting value set. For each
	 * instruction we meet, depending on its semantic, we will find whether
	 * it could be affected by any value in the interesting value set. For example,
	 * if it is a STORE instruction, if the source is in the set, we also put
	 * the target into the set. It's like static tainting analysis. So after
	 * the traverse, we can find every use  of our input argument
	 */

	inst_iterator iter = inst_begin(function), end = inst_end(function);
	for (; iter != end; iter++) {
		Instruction* inst = &(*iter);

		if (isa<StoreInst>(inst)) {
			Value* src = inst->getOperand(0);
			Value* tgt = inst->getOperand(1);

			set<unsigned>* srcTSets = argMap->findCorrespondingTaintedSet(src);

			if (srcTSets) {
//				if(findDFNodeList(interestNodes, srcNode)!=interestNodes->end()){
				/* If the target is from getelementptr instruction, then we
				 * go and check the source of the pointer, as well as the
				 * possible index used
				 */
				if (isa<GetElementPtrInst>(tgt)) {
					GetElementPtrInst * gepi = dyn_cast<GetElementPtrInst>(tgt);

					if (gepi->getNumOperands() == 3) {

						Value* epsrc = gepi->getOperand(0);
						if (isa<LoadInst>(epsrc)) {
							LoadInst * lins = dyn_cast<LoadInst>(epsrc);

							Value* ldsrc = lins->getOperand(0);
							argMap->push2TaintedSets(srcTSets, ldsrc);
						}
					}
				}

				argMap->push2TaintedSets(srcTSets, tgt);
				delete srcTSets;
			}

		} else if (isa<LoadInst>(inst) || isa<CastInst>(inst)) {
			// If it is load instruction, then check whether the source is of interest.
			// If yes, also put the instruction into the list
			Value* src = inst->getOperand(0);

			set<unsigned>* tsets = argMap->findCorrespondingTaintedSet(src);

			if (tsets) {
				//if(findDFNodeList(interestNodes, &tmpNode)!=interestNodes->end()){
				argMap->push2TaintedSets(tsets, inst);
				delete tsets;
			}

		} else if (isa<GetElementPtrInst>(inst)) {
			//the assumption makes it not important.

		} else if (isa<CallInst>(inst) || isa<InvokeInst>(inst)) {
			//we do not consider it as a first try
			//TODO

		} else if (isa<ReturnInst>(inst)) {
			//we do not care return

		} else if (isa<BinaryOperator>(inst)) {
			BinaryOperator * binOp = dyn_cast<BinaryOperator>(inst);
			/* Instructions such as add
			 * We add them to interesting list if any of the
			 * operands is in the list
			 */
			Value* op1 = binOp->getOperand(0);
			Value* op2 = binOp->getOperand(1);

			set<unsigned>* setset1 = argMap->findCorrespondingTaintedSet(op1);
			set<unsigned>* setset2 = argMap->findCorrespondingTaintedSet(op2);

			if (setset1) {
				argMap->push2TaintedSets(setset1, inst);
				delete setset1;
			}

			if (setset2) {
				argMap->push2TaintedSets(setset2, inst);
				delete setset2;
			}
		}
	}

	set<unsigned>* res = argMap->findCorrespondingTaintedSet(v);

	if(res) {
		set<unsigned>::iterator iter = res->begin(), end = res->end();
		for(; iter != end; iter++) {
			argsrc->insert( (*iter)+1 );
		}
		return true;

	} else {
		return false;
	}
}

/*
 * recursively back-tracking whether the value is derived from a configuration directive
 *
 * the parameter "cmp" means whether count the comparison with a directive related variable
 *     e.g., (length >= ft_min_word_len && length < ft_max_word_len)
 *
 * the corresponding bitcode is:
 *    ; <label>:119                                     ; preds = %104
 * 	  %120 = load i32* %length, align 4, !dbg !2362550
 *    %121 = load i32* @ft_min_word_len, align 4, !dbg !2362550
 *    %122 = icmp uge i32 %120, %121, !dbg !2362550
 *    br i1 %122, label %123, label %141, !dbg !2362550
 *
 *    ; <label>:123                                     ; preds = %119
 *    %124 = load i32* %length, align 4, !dbg !2362550
 *    %125 = load i32* @ft_max_word_len, align 4, !dbg !2362550
 *    %126 = icmp ult i32 %124, %125, !dbg !2362550
 *
 * so basically, we want to trace @ft_min_word_len --> %121 --> %120 --> %length --> %124% --> 125% -->  ft_max_word_len
 * such case is only used for MIN-MAX specifications
 */
bool Misconfig::checkConfParameterByBackTrack(Value* v, set<string>* params, set<Value*>* missCache, bool checkUsage) {

	if (missCache->find(v) != missCache->end()) {
		return false;
	} else {
		missCache->insert(v);
	}

//	if(!isa<Instruction>(v)) {
//		errs() << "v -- " << *v << "\n";
//		assert(false);
//	}

	if (isa<Instruction>(v)) {
		Instruction* inst = dyn_cast<Instruction>(v);

		if (isa<GetElementPtrInst>(v)) {
			GetElementPtrInst* gepi = dyn_cast<GetElementPtrInst>(v);

			if(_confUsageInfo.getConfParamSet(gepi, params)) {
				return true;
			} else {
				return false;
			}

		} else if (isa<AllocaInst>(v)) {
			return false;

		} else if (isa<LoadInst>(v)) {
			Value* src = inst->getOperand(0);

			int res = false;

			if (checkUsage == true) { //check whether it's used in comparison
				//we have to do a forward trace again
				set<Value*> valueCache;
				bool opx = isComparedWithConfParameters(src, params, missCache, &valueCache);
				if (opx == true) {
					missCache->erase(src);
				}
				res |= opx;
			}

			bool opy = checkConfParameterByBackTrack(src, params, missCache, checkUsage);
			if (opy == true) {
				missCache->erase(src);
			}

			res |= opy;
			return res;

		} else {
			bool res = false;

			for (unsigned i = 0; i < inst->getNumOperands(); i++) {
				Value* o = inst->getOperand(i);

				bool opr = checkConfParameterByBackTrack(o, params, missCache, checkUsage);
				if (opr == true) {
					missCache->erase(o);
				}
				res |= opr;
			}
			return res;
		}

	} else if (isa<Constant>(v)) {

		if (isa<GlobalVariable>(v) || isa<ConstantExpr>(v)) {
			if( _confUsageInfo.getConfParamSet(v, params) ) {
				return true;
			} else {
				return false;
			}

		} else if (isa<ConstantInt>(v)) {
			return false;
		}

	} else if (isa<Operator>(v)) {
		Operator* op = dyn_cast<Operator>(v);

		bool res = false;
		for (unsigned i = 0; i < op->getNumOperands(); i++) {
			Value* o = op->getOperand(i);
			bool opr = checkConfParameterByBackTrack(o, params, missCache, checkUsage);

			if (opr == true) {
				missCache->erase(o);
			}
			res |= opr;
		}
		return res;

	}

	return false;
}

bool Misconfig::isComparedWithConfParameters(Value* v, set<string>* params,
		set<Value*>* missCache, set<Value*>* valueCache) {

	if (missCache->find(v) != missCache->end()) {
		return false;
	}

	if (!isa<Instruction>(v)) {
		return false;
	}

	valueCache->insert(v);
	bool res = false;

	Value::use_iterator u = v->use_begin(), end = v->use_end();
	for ( ; u != end;	u++) {
		/*Check if it is already cached to avoid infinite loop*/
		if (valueCache->find(*u) != valueCache->end()) {
			continue;
		} else {
			valueCache->insert(*u);
		}

		if (isa<LoadInst>(*u)) {
			LoadInst *inst = dyn_cast<LoadInst>(*u);
			res |= isComparedWithConfParameters(inst, params, missCache, valueCache);

		} else if (isa<CastInst>(*u)) {
			CastInst *inst = dyn_cast<CastInst>(*u);
			res |= isComparedWithConfParameters(inst, params, missCache, valueCache);

		} else if (isa<CmpInst>(*u)) {
			CmpInst* inst = dyn_cast<CmpInst>(*u);

			Value* target;
			if ((target = inst->getOperand(0)) == v) {
				target = inst->getOperand(1);
			}

			//On compare, we are going to check its usage
			res |= checkConfParameterByBackTrack(target, params, missCache, false);
		}
	}

	return res;
}

/**
 * check the range constraint based on the compare instruction
 * Value *v: the value needs to be checked. used to determine which one in the 
 * 			 compare operand is the target value. If called in trackDataFlow,
 * 			 it is the "v" that is currently under analysis
 * CmpInst* inst: the comparison instruction
 * Return the constraints inferred
 *
 * [NOTE] for reference (range), we don't care whether it's <, =, >,
 * it does not make any difference to us.
 **/
Constraints* Misconfig::analyzeRangeConstraints(Value* v, CmpInst* inst) {
	Constraints* res = new Constraints();

	 //3288666   %conv17 = zext i8 %conv16 to i32, !dbg !5361351
	 //3288667   %cmp18 = icmp sge i32 %conv17, 65, !dbg !5361351
	bool isChar  = isChar2IntCast(v) ? true : false;

	//Get the comparison target
	Value* target;
	if ((target = inst->getOperand(0)) == v) {
		target = inst->getOperand(1);
	}

	Constant* constant = dyn_cast<ConstantInt>(target);
	if (!constant) {
		//Currently we only deal with comparison with constants
		errs() << "Not comparing to constant int, give up\n";

		delete res;
		return NULL;
	}

	CmpInst::Predicate pred = inst->getPredicate();
	switch (pred) {
	case CmpInst::ICMP_UGT:
	case CmpInst::ICMP_UGE:
	case CmpInst::ICMP_ULT:
	case CmpInst::ICMP_ULE: {

		uint64_t cmpInt = dyn_cast<ConstantInt>(constant)->getZExtValue();
		ReferenceConstraint* rCon;
		if(isChar) {
			rCon = new ReferenceConstraint(char2string((char) cmpInt));
		} else {
			rCon = new ReferenceConstraint(num2string(cmpInt));
		}
		res->insert(rCon);
		break;
	}
	case CmpInst::ICMP_SGT:
	case CmpInst::ICMP_SGE:
	case CmpInst::ICMP_SLE:
	case CmpInst::ICMP_SLT: {

		int cmpInt = dyn_cast<ConstantInt>(constant)->getSExtValue();
		ReferenceConstraint* rCon;
		if(isChar) {
			rCon = new ReferenceConstraint(char2string((char) cmpInt));
		} else {
			rCon = new ReferenceConstraint(num2string(cmpInt));
		}
		res->insert(rCon);
		break;
	}
	/*
	 * Here we consider three cases (the same thing applies to ICMP_EQ but not less or greater cases)
	 *
	 * 1. It compare with some integers, that's the most common cases.
	 *	      %ce_type = getelementptr inbounds %struct.CfEntryInfo* %0, i32 0, i32 4, !dbg !87181
	 *	      %1 = load i32* %ce_type, align 4, !dbg !87181
	 *	      %cmp = icmp ne i32 %1, 5, !dbg !87181
	 *	  So, here the target is 5, and the comparison is for integer type
	 *
	 * 2. It compare with a char, not an integer. This is pretty tricky, the code pattern is like:
	 *        %6 = load i8** %w, align 4, !dbg !88124
	 *        %7 = load i8* %6, !dbg !88124
	 *        %conv = sext i8 %7 to i32, !dbg !88124
	 *        %cmp = icmp eq i32 %conv, 43, !dbg !88124
	 *    Actually, the corresponding C code is:
	 *        if (*w == '+' || *w == '-') { //'+' is 43
	 *    In this case, we have to check whether the operand, i.e., %conv is from a cast conversion instead of blindly thinking it's a integer 43
	 *
	 * 3. It compare with a null, not an integer, e.g.,
	 *    %tobool = icmp ne %struct.Operation* %0, null, !dbg !87243
	 *
	 **/
	case CmpInst::ICMP_NE:
	case CmpInst::ICMP_EQ: {

		if (isa<ConstantInt>(constant)) {
			ConstantInt* ci = dyn_cast<ConstantInt>(constant);
			int cmpInt = ci->getSExtValue();

			if (isString2Char2Int(v)) { //case 2
				Constraint* tCon = new TypeConstraint("STRING");
				res->insert(tCon);

				char c = cmpInt;
				string sc = char2string(c);
				errs() << "char2string -- " << sc << "\n";
				ReferenceConstraint* rCon = new ReferenceConstraint(sc);
				res->insert(rCon);

			} else { //case 1
//				Constraint* tCon = new Constraint(TYPE, INTEGER);
//				res->insert(tCon);

				ReferenceConstraint* rCon = new ReferenceConstraint(num2string(cmpInt));
				res->insert(rCon);
			}

		} else if (isa<ConstantPointerNull>(constant)) { //case 3
//			string *cmpString = new string("null");
			ReferenceConstraint* rCon = new ReferenceConstraint("null");
			res->insert(rCon);

		} else {
//			errs() << "[info] another kind of constant:" << *constant << "/n";
		}

		break;
	}

//TODO:float number

	default:
		errs() << "Not determined comparison type\n";
		break;
	}

	if (res->size() > 0) {
		return res;
	} else {
		delete res;
		return NULL;
	}
}

//TODO we only care the last one!!!
void Misconfig::analyzeMetadataTypeConstraints(int mode) {

	if(mode == GLOBAL) {
		errs() << "==============================addMetadataTypeConstraints (Global Struct) ==============================\n";

		map<string, GlobalConfValues*>::iterator iter = _mapGV.begin(), end = _mapGV.end();
		for(; iter != end; iter++) {
			_curDirectiveStr = (*iter).first;

			set<GlobalConfValue*>::iterator gcvIter = (*iter).second->begin(), gcvEnd = (*iter).second->end();
			for(; gcvIter != gcvEnd; gcvIter++) {
				StructFieldPairs* sfp = (*gcvIter)->sfp;

				if(sfp && !sfp->isEmpty()) {
					errs() << "------------------AMTC GLOBAL " << _curDirectiveStr << "\n";
					sfp->print();

					addMetadataTypeConstraint(sfp);
				}
			}
		}

	} else if(mode == LOCAL) {
		errs() << "==============================addMetadataTypeConstraints (Local Struct) ==============================\n";

		map<string, LocalConfValues*>::iterator iter = _mapLVStructField.begin(), end = _mapLVStructField.end();
		for (; iter != end; iter++) {

			_curDirectiveStr = (*iter).first;

			set<StructFieldPairs*>* sfpSet = (*iter).second->getStructFieldPairs();
			set<StructFieldPairs*>::iterator setIter = sfpSet->begin(), setEnd = sfpSet->end();

			for (; setIter != setEnd; setIter++) {
				errs() << "------------------AMTC LOCAL " << _curDirectiveStr << "\n";
				(*setIter)->print();

				//check the metadata nodes to find some basic types of this structure-field
				addMetadataTypeConstraint(*setIter);
			}
		}
	}
}

/**
 * This function is only for global structure and actually for VICTIM
 */
void Misconfig::trackSpecFromGV(map<string, GlobalConfValues*>* gmap) {

	ValueUsageMap* gvumap = collectGlobalValues(gmap);

	map<string, list<ValueUsage*>*> gvum = gvumap->_lvUsageMap;
	map<string, list<ValueUsage*>*>::iterator iter = gvum.begin(), end = gvum.end();

	for (; iter != end; iter++) {
		this->_curDirectiveStr = (*iter).first;
		list<ValueUsage*>* gvulist = (*iter).second;
		errs() << "gvulist size: " << gvulist->size() << "\n";

		errs() << "+++++++++++++++Processing Directive (GV) " << _curDirectiveStr << "++++++++++++++++++++++\n";

		list<ValueUsage*>::iterator gvuIter = gvulist->begin(), gvuEnd = gvulist->end();
		for(; gvuIter != gvuEnd; gvuIter++) {
			ValueUsage* tmpgvu = *gvuIter;

			StructFieldPairs newsfp(tmpgvu->_sfp);

			GlobalValue* gv2track = cast<GlobalValue>(tmpgvu->_lUsage);
			errs() << "***gv2track name*** " << gv2track->getNameStr() << "\n";

			if(newsfp.isEmpty()) {
				_confUsageInfo.addConfUsageInfo(_curDirectiveStr, gv2track, NULL);
			}

			if (isa<GlobalVariable>(gv2track)) {
				//type stuff
				string typeName = _metaDataInfo->getGlobalVariableTypeName(dyn_cast<GlobalVariable>(gv2track));
				if(typeName.size() != 0) {
					putTypeConstraint(typeName);
				}
			} //else means it's a function or structure and we don't care the type

			isFromMiddle = true;
			ValueCache vCache;
			FACache faCache;
			Context callchain; //for global one, even struct-based, we don't know the usage

			trackDataFlow(gv2track, &newsfp, NULL, &faCache, &vCache, &callchain);
		}
	}

	delete gvumap;
}

void Misconfig::trackSpecFromLVStruct(map<string, LocalConfValues*>* lvmap) {
	errs() << "====================================trackSpecFromLVStruct\n";

	ValueUsageMap* lvumap = collectGetElementInsts(lvmap);

//	map<string, LocalConfValues* >::iterator iter = lvmap->begin(), end = lvmap->end();

	map<string, list<ValueUsage*>*> lvum = lvumap->_lvUsageMap;
	map<string, list<ValueUsage*>*>::iterator iter = lvum.begin(), end = lvum.end();

	for (; iter != end; iter++) {
		this->_curDirectiveStr = (*iter).first;
		list<ValueUsage*>* lvulist = (*iter).second;

		errs() << "+++++++++++++++Processing Directive (LV) " << _curDirectiveStr << "++++++++++++++++++++++\n";

		//one directive uses one ValueCache and FACache
		ValueCache vCache;

		list<ValueUsage*>::iterator lvuIter = lvulist->begin(), lvuEnd = lvulist->end();
		for(; lvuIter != lvuEnd; lvuIter++) {
			ValueUsage* tmplvu = *lvuIter;

			errs() << "+++++++++++++++LVUsage.SFP " << "\n";
			tmplvu->_sfp->print();

			StructFieldPairs newsfp(tmplvu->_sfp);
			//here we already checked the gep matches the <struct+field>
			//later, in trackDataflow(), it tracks the usage of this gep so we need to pop the sfp
			newsfp.popFirstStructName();
			newsfp.popFirstField();

//			unsigned linenum = 0;
//			StringRef filename;
//			getInstSrcLocation(tmplvu->_lUsage, linenum, filename);
//			errs() << " tracking from position " << filename.str() << ":" << linenum << "\n";

			FACache faCache;

			Context callchain;
			if(isa<Instruction>(tmplvu->_lUsage)) {
				Instruction* i = dyn_cast<Instruction>(tmplvu->_lUsage);
				callchain.pushBack(i);
			}

			trackDataFlow(tmplvu->_lUsage, &newsfp, NULL, &faCache, &vCache, &callchain);
		}
	}

	delete lvumap;
}

void Misconfig::trackDepSpec() {
//	errs() << "====================================trackDepSpec\n";

	map<string, UsageInfo* >::iterator confIter = _confUsageInfo._confUsageInfo.begin();
	map<string, UsageInfo* >::iterator confEnd  = _confUsageInfo._confUsageInfo.end();
	for(; confIter != confEnd; confIter++) {
		this->_curDirectiveStr = confIter->first;
		UsageInfo* uinfo = confIter->second;

		errs() << "+++++++++++++++Processing Directive (DepSpec) "
			   << _curDirectiveStr << "++++++++++++++++++++++\n";

		this->_controlDepAggr = new ControlDepAggregator();

		UsageInfo::iterator uIter = uinfo->begin(), uEnd = uinfo->end();
		for(; uIter != uEnd; uIter++) {
			Value* trackValue = uIter->first;

//			errs() << "trackValue = " << *trackValue << "\n";

			list<Context* >* lcc = uIter->second;

			list<Context*>::iterator lccIter = lcc->begin(), lccEnd = lcc->end();
			for(; lccIter != lccEnd; lccIter++) {
				Context* cc = *lccIter;

//				cc->hardPrint();

				ValueCache vCache;
				FACache faCache;

				trackDataFlowDep(trackValue, &faCache, &vCache, cc);
			}
		}

		//TODO: analyze the if-dep results are filter the fake deps according to
		//1. cdep (record the control dependency infomation of the current parameter)
		//2. _func2ConfUsageMap
//		_controlDepAggr->print();

		set<string> depParams;
		_controlDepAggr->getRealDepParams(&_confUsageInfo, &_func2ConfUsageMap, _ccg, &depParams);

		this->mergeIfDependencies(_curDirectiveStr, "*", &depParams);

		delete _controlDepAggr;
	}
}

bool Misconfig::isNotInterestStructField(Value* v, unsigned field,
		list<StructFieldPairs*>* spList) {
	string structName = getStructName(v);
	return isNotInterestStructField(structName, field, spList);
}

bool Misconfig::isNotInterestStructField(Type* t, unsigned field,
		list<StructFieldPairs*>* spList) {
	string structName = getStructName(t);
	return isNotInterestStructField(structName, field, spList);
}

bool Misconfig::isNotInterestStructField(string structName, unsigned field,
		list<StructFieldPairs*>* spList) {
	if (!structName.empty() && field != UINT_MAX) {
		if (checkStructField(structName, field, spList) == 2) {
			return true;
		} else {
			return false;
		}
	}
	return false;
}

bool Misconfig::isNotInterestStructField(GetElementPtrInst* gepi,
		list<StructFieldPairs*>* spList) {
	PointerType* pType = gepi->getPointerOperandType();
	Type* t = pType->getElementType();

//	string structName;
	unsigned field = UINT_MAX;

	if (gepi->getNumIndices() >= 2 && gepi->hasAllConstantIndices()) {
		ConstantInt* cInt = dyn_cast<ConstantInt>(gepi->getOperand(2));
		field = cInt->getSExtValue();
	}

	return isNotInterestStructField(t, field, spList);
}

bool Misconfig::isInterestStructField(string structName, unsigned field,
		list<StructFieldPairs*>* spList) {
	if (!structName.empty() && field != UINT_MAX) {
		if (checkStructField(structName, field, spList) == 1) {
			return true;
		} else {
			return false;
		}
	}
	return false;
}

bool Misconfig::isInterestStructField(Type* t, unsigned field,
		list<StructFieldPairs*>* spList) {
	string structName = getStructName(t);
	return isInterestStructField(structName, field, spList);
}

bool Misconfig::isInterestStructField(Value* v, unsigned field,
		list<StructFieldPairs*>* spList) {
	string structName = getStructName(v);
	return isInterestStructField(structName, field, spList);
}

bool Misconfig::isInterestStructField(GetElementPtrInst* gepi,
		map<string, LocalConfValues*>* spMap, string& structName,
		unsigned& field, string& directive) {

	PointerType* pType = gepi->getPointerOperandType();
	Type* t = pType->getElementType();

	if (gepi->getNumIndices() >= 2 && gepi->hasAllConstantIndices()) {
		for (unsigned i = 2; i < gepi->getNumOperands(); i++) {
			ConstantInt* cInt = dyn_cast<ConstantInt>(gepi->getOperand(i));
			field = cInt->getSExtValue();
		}
	}

	structName = getStructName(t);

	if (!structName.empty() && field != UINT_MAX) {
		map<string, LocalConfValues*>::iterator iter = findStructField(structName, field, spMap);
		if (iter != spMap->end()) {
			directive = (*iter).first;
			return true;
		} else {
			return false;
		}
	}

	return false;
}

bool Misconfig::isInterestStructField(ConstantExpr* cexpr,
		map<string, LocalConfValues*>* spMap, string& structName,
		unsigned& field, string& directive) {

	if (cexpr->getNumOperands() == 0) {
//		errs() << "[info] the ConstantExpr " << *cexpr << " has no operand.\n";
		return false;
	}

	Value* st = cexpr->getOperand(0);
	if (isa<GlobalVariable>(st)) {
		Type* ty = st->getType();
		structName = getStructName(ty);

		if (isa<StructType>(ty)) {
			StructType* st = dyn_cast<StructType>(ty);
			unsigned ne = st->getNumElements();
			for (unsigned i = 0; i < ne; i++) {
//				Type* t = st->getElementType(i);
//				errs() << "uuuuuuuu " << *t;
			}
		}

		if (cexpr->getNumOperands() >= 2) {
			for (unsigned i = 2; i < cexpr->getNumOperands(); i++) {
				ConstantInt* cInt = dyn_cast<ConstantInt>(cexpr->getOperand(i));
				field = cInt->getSExtValue();
			}
		}

		if (!structName.empty() && field != UINT_MAX) {
			map<string, LocalConfValues*>::iterator iter = findStructField(structName, field, spMap);
			if (iter != spMap->end()) {
				directive = (*iter).first;
				return true;
			} else {
				return false;
			}
		}
	}

	return false;
}

ValueUsageMap* Misconfig::collectGlobalValues(map<string, GlobalConfValues*>* gcv) {
	ValueUsageMap* gvmap = new ValueUsageMap();

	//1. first check functions
	Module::iterator modIter = _module->begin(), modEnd = _module->end();
	for (; modIter != modEnd; modIter++) {
		Function* f = &(*modIter);

		list<ValueUsage*> gvus;
		findGVUsage(f, gcv, &gvus, FUNCTION);

		gvmap->insert(&gvus);
	}

	//2. then check global variables
	Module::global_iterator iter = _module->global_begin(), end = _module->global_end();
	for (; iter != end; iter++) {
		GlobalVariable* gv = &(*iter);

		list<ValueUsage*> gvus;
		findGVUsage(gv, gcv, &gvus, GLOBAL_VAR);

		gvmap->insert(&gvus);
	}

	return gvmap;
}

//Check each function to find the place that makes use of
//any of the predefined <structure, field>
ValueUsageMap* Misconfig::collectGetElementInsts(map<string, LocalConfValues*>* lcv) {
	ValueUsageMap* lvumap = new ValueUsageMap();

	Module::iterator mi = _module->begin(), me = _module->end();
	for (; mi != me; mi++) {
		Function* F = &*mi;
		inst_iterator i = inst_begin(F), e = inst_end(F);

		for (; i != e; i++) {
			Instruction* inst = &*i;

			if (isa<GetElementPtrInst>(inst) && inst->getNumOperands() == 3) {
				GetElementPtrInst* gepi = dyn_cast<GetElementPtrInst>(inst);

				list<ValueUsage*> lvus;
				findLVUsage(gepi, lcv, &lvus);

//				unsigned linenum = 0;
//				StringRef filename;
//				getInstSrcLocation(inst, linenum, filename);
//				errs() << "Found out usage at "	<< filename.str() << ":" << linenum << "\n";

				lvumap->insert(&lvus);
			}
		}
	}

	return lvumap;
}

/**
 * mode tells whether ingv is a function or a global variable
 */
void Misconfig::findGVUsage(GlobalValue* ingv, map<string, GlobalConfValues*>* gcv, list<ValueUsage*>* gvus, int mode) {
	string in_gvname;
	if(mode == FUNCTION) {
		in_gvname = stripFunctionNameSuffix(ingv->getNameStr());
	} else if(mode == GLOBAL_VAR) {
		;
	}

	map<string, GlobalConfValues*>::iterator iter = gcv->begin(), end = gcv->end();

	for(; iter != end; iter++) {
		string directive = (*iter).first;
		GlobalConfValues* gv = (*iter).second;

		set<GlobalConfValue*>::iterator gvIter = gv->begin(), gvEnd = gv->end();
		for(; gvIter != gvEnd; gvIter++) {
			GlobalValue* tmpgv = (*gvIter)->gvar;

			if(mode == FUNCTION) {
				if(isa<Function>(tmpgv)) {
					string tmp_gvname = tmpgv->getNameStr();
//					if(tmp_gvname.find(".str") != string::npos) {
//						continue;
//					}
					string tp_gvname = stripFunctionNameSuffix(tmp_gvname);

					if(in_gvname.compare(tp_gvname) == 0) {
						ValueUsage* tgvu = new ValueUsage(directive, (*gvIter)->sfp, ingv);
						gvus->push_back(tgvu);
					}
				}

			} else if(mode == GLOBAL_VAR) {
				if(isa<GlobalVariable>(tmpgv)) {
					//TODO
					if(tmpgv == ingv) {
						ValueUsage* tgvu = new ValueUsage(directive, (*gvIter)->sfp, ingv);
						gvus->push_back(tgvu);
					}
				}
			}
		}
	}
}

void Misconfig::findLVUsage(GetElementPtrInst* gepi, map<string, LocalConfValues*>* lcv, list<ValueUsage*>* lvus) {
	map<string, LocalConfValues*>::iterator iter = lcv->begin(), end = lcv->end();

	for(; iter != end; iter++) {
		string directive = (*iter).first;
		LocalConfValues* lv = (*iter).second;

		set<StructFieldPairs*>::iterator lvIter = lv->begin(), lvEnd = lv->end();
		for(; lvIter != lvEnd; lvIter++) {
			string sname = (*lvIter)->getFirstStructName();
			unsigned idx = (*lvIter)->getFirstField();

//			//it is possible that the first SF pair is <*, 1> or <struct.ABC, *>
//			if(sname.compare("*") != 0) {

			//TODO struct with the same name
			if(gepMatch(gepi, sname, idx)) {
				ValueUsage* tlvu = new ValueUsage(directive, *lvIter, gepi);
				lvus->push_back(tlvu);
			}
//			}
		}
	}
}

void Misconfig::dumpLVs() {
	map<string, LocalConfValues*>::iterator iter =
			_mapLVStructField.begin(), end = _mapLVStructField.end();

	errs() << "Read Local Configuration structures:\n";

	for (; iter != end; iter++) {
		errs() << (*iter).first << ":" << "\n";

		set<StructFieldPairs*>* sfp = (*iter).second->getStructFieldPairs();
		set<StructFieldPairs*>::iterator setIter = sfp->begin(), setEnd = sfp->end();

		for (; setIter != setEnd; setIter++) {

			const list<string>* st = (*setIter)->getStructs();
			list<string>::const_iterator stIter = st->begin(), stEnd =
					st->end();

			const list<unsigned>* fl = (*setIter)->getFields();
			list<unsigned>::const_iterator flIter = fl->begin(), flEnd =
					fl->end();

//		errs() << "st.size / fl.size: " << st->size() << " / " << fl->size() << "\n";
			assert(fl->size() == st->size());
			assert(fl->size() > 0);

			for (; flIter != flEnd && stIter != stEnd; stIter++, flIter++) {
				errs() << "< " << *stIter << ", " << *flIter << " > ";
			}
			errs() << "\n";
		}
	}

	errs() << "local configuration structures dump end\n";
}

void Misconfig::addMetadataTypeConstraint(StructFieldPairs* sfp) {
	string structName = sfp->getLastStructName();
	unsigned field = sfp->getLastField();

	//the assumption here is that this is definitely a named MDNode,
	//so we can search it by name;
	MDNode* structMD = _metaDataInfo->getStructureMDNodeFromName(structName);

	if (!structMD) {
		errs() << "[error] the structName " << structName << " cannot be found!\n";
		return;
	}

	MDNode* memberMDs = (MDNode*) (structMD->getOperand(10));

	//It is possible that the 10th operand is a null,
	//e.g., in Apache httpd, we get the following one:
	//      !{i32 720915, null, metadata !"ssi_internal_ctx", metadata <badref>, i32 101, i64 0, i64 0, i32 0, i32 4, i32 0,
	//        null, i32 0, i32 0} ; [ DW_TAG_structure_type ]
	if (!memberMDs) {
		return;
	}

	if(field >= memberMDs->getNumOperands()) {return; }

	MDNode* fieldMD = (MDNode*) (memberMDs->getOperand(field));

	/**
	 * TODO: currently we have a bug,
	 * the two struct with different name might refer to different structures,
	 * though in most case they refer to the same structure
	 *
	 * Currently we try to ignore the type name.
	 * But this's definitely an issue
	 */
	if (!fieldMD) { return; }

	string typeName = _metaDataInfo->getTypeFromMD(fieldMD);
	errs() << "------------typeName: " << typeName << "\n";

	if (typeName.compare("Not Determined DW_TAG type") != 0) {
		putTypeConstraint(typeName);
	}
}

string Misconfig::dumpMark(string type) {
	string dstr = START_DUMP_CONS + type + "] \n";
	return dstr;
}

//&_mapLCVStructField
void Misconfig::readLocalVariables(string fileName) {
//	cout << "reading file " << fileName.str() << endl;

	ifstream infile(fileName.c_str());
	string line;
	string entryName, confStruct;
	unsigned field;

	while (getline(infile, line)) {
		istringstream iss(line);
		iss >> entryName >> confStruct >> field;

		list<string> stlist;
		list<unsigned> flist;
		stlist.push_back(confStruct);
		flist.push_back(field);

		string exst = " ";
		unsigned exfl = UINT_MAX;
		while (1) {
			iss >> exst >> exfl;
			if (exst.compare(" ") == 0 || exfl == UINT_MAX) {
				break;
			}
			//errs() << ex << "\n";
			stlist.push_back(exst);
			flist.push_back(exfl);
			exst = " ";
			exfl = UINT_MAX;
		}

//		confStruct = confStruct.substr(confStruct.find(".")+1);
		StructFieldPairs* tmp = new StructFieldPairs(&stlist, &flist);

		errs() << "entryName: " << entryName << "\n";

		if(!testadd2MapVictimLV(entryName, tmp)) {
			delete tmp;
		}
	}
}

//TODO: vCache should be other cache (a set is enough);
bool Misconfig::gotoBranch(Value* v, set<int>* rlist, set<Value*>* dCache) {
	unsigned linenum;
	StringRef fileName;

	bool res = false;

	for (Value::use_iterator u = v->use_begin(), end = v->use_end(); u != end;
			u++) {

		/*Check if it is already cached to avoid infinite loop*/
		if (dCache->find(*u) != dCache->end()) {
			continue;
		}

		dCache->insert(*u);

		if (isa<LoadInst>(*u)) {
			LoadInst *inst = dyn_cast<LoadInst>(*u);
			getInstSrcLocation(inst, linenum, fileName);
//			errs() << "Got it in file " << fileName.str() << ":" << linenum << " of load instruction\n";

			res |= gotoBranch(inst, rlist, dCache); //the load inst itself represents the target

		} else if (isa<StoreInst>(*u)) {
			StoreInst *inst = dyn_cast<StoreInst>(*u);
			getInstSrcLocation(inst, linenum, fileName);
//			errs() << "Got it in file "<< fileName.str() <<":"<<linenum<<" of store instruction\n";

			//The second operand is the store target. operand(0) is the value operand -- see Instructions.h getValueOperand()
			Value *target = inst->getOperand(1);

			if (target == v) {
				continue;
			}

			res |= gotoBranch(inst, rlist, dCache);

		} else if (isa<GetElementPtrInst>(*u)) {
			GetElementPtrInst *inst = dyn_cast<GetElementPtrInst>(*u);
			getInstSrcLocation(inst, linenum, fileName);
//			errs() << "Got it in file "<<fileName.str()<<":"<<linenum<<" of getElementPtr instruction\n";

			if (!isNotInterestStructField(inst, &_listPredefinedStructField)) {
				res |= gotoBranch(inst, rlist, dCache);
			}

		} else if (isa<CastInst>(*u)) {
			CastInst *inst = dyn_cast<CastInst>(*u);

			res |= gotoBranch(inst, rlist, dCache);

		} else if (isa<CmpInst>(*u)) {
			CmpInst* inst = dyn_cast<CmpInst>(*u);

			_valueBefore2Steps = v;

			//On compare, we are going to check its usage
			res |= gotoBranch(inst, rlist, dCache);

		} else if (isa<BranchInst>(*u)) {
			BranchInst* inst = dyn_cast<BranchInst>(*u);

			getInstSrcLocation(inst, linenum, fileName);
//			errs() << "Got it in file "<<fileName.str()<<":"<<linenum<<" of store instruction\n";

			if (!inst->isConditional()) {
				continue;
			}

			Value* cond = inst->getCondition();

			//First we are going to check possible
			if (isa<CmpInst>(cond)) {
				int toCompare = INT_MAX;
				CmpInst* cmpInst = dyn_cast<CmpInst>(cond);

				Value* cmpsrc1 = cmpInst->getOperand(0);
				Value* cmpsrc2 = cmpInst->getOperand(1);

//				errs() << "before the assert\n";
				assert(
						cmpsrc1 == _valueBefore2Steps || cmpsrc2 == _valueBefore2Steps);

				Value* theOther =
						(cmpsrc1 == _valueBefore2Steps) ? cmpsrc2 : cmpsrc1;

				if (isa<Constant>(theOther)) {
					if (isa<ConstantInt>(theOther)) {
						ConstantInt* cInt = dyn_cast<ConstantInt>(theOther);

						toCompare = cInt->getSExtValue();
						rlist->insert(toCompare);

						res |= true;

					} else {
						errs() << "[warn] it might not be a constant value!\n";
					}

				} else {
					continue;
				}
			}

		} else if (isa<Instruction>(*u)) {
			Instruction *inst = dyn_cast<Instruction>(*u);

			getInstSrcLocation(inst, linenum, fileName);

			if (isa<BinaryOperator>(inst)) {
				BinaryOperator *binOper = dyn_cast<BinaryOperator>(inst);

				res |= gotoBranch(binOper, rlist, dCache);
			}

		}
	}
	return res;
}

void Misconfig::readPredefinedStruct(string fileName) {
//	cout << "reading file " << fileName.str() << endl;

	ifstream infile(fileName.c_str());
	string line;
	string confStruct;
	int field;

	while (getline(infile, line)) {
		istringstream iss(line);
		iss >> confStruct >> field;
//		confStruct = confStruct.substr(confStruct.find(".")+1);
		StructFieldPairs* tmp = new StructFieldPairs(confStruct, field);

		this->_listPredefinedStructField.push_back(tmp);
	}
}

void Misconfig::dumpAllConstraints() {
	_mapDirectiveConstraints.dumpAllConstraints();
	_mapDirectiveConstraints.dumpAllConstraintsToFile(_conf.outputFile);
}

void Misconfig::dumpAllConstraints2File(string s) {
	_mapDirectiveConstraints.dumpAllConstraintsToFile(_conf.outputFile+s);
}

void Misconfig::getIncubationBB() {
	set<Function*> doneCache;

	Module::iterator modIter = _module->begin(), modEnd = _module->end();
	for (; modIter != modEnd; modIter++) {
		Function* f = &(*modIter);

		pair<FuncArgPair*, Constraints* >* fcpair = _cfcache.getFapNConstraints(f->getNameStr(), 0);
		if (fcpair) {
			Constraints* cons = fcpair->second;

			errs() << "%%%%%%%%%%%%%%%%%%%%%%%getIncubationBB%%%%%%%%%%%%%%%%%%\n";
			Value::use_iterator u = f->use_begin(), end = f->use_end();
			for ( ; u != end; u++) {

				if (isa<CallInst>(*u)) {
					CallInst* callinst = dyn_cast<CallInst>(*u);
					BasicBlock* bb = callinst->getParent();
					_incubationBB[bb] = cons;
//					incubationFunc.insert(bb->getParent());
					errs() << "*In Func:*" << bb->getParent()->getNameStr()	<< "\n";
				}
			}
		}
	}
}

void Misconfig::getLoopInfo() {
//	LoopInfo li;
	map<BasicBlock*, Constraints*>::iterator icbIter = _incubationBB.begin(),
			icbEnd = _incubationBB.end();

	for (; icbIter != icbEnd; icbIter++) {
		BasicBlock* bb = (*icbIter).first;
		Function* ff = bb->getParent();
//	li.runOnFunction(*ff);

		errs() << "----------Loop Tracking For [" << ff->getNameStr() << "]\n";

		set<Value*> doneCache;
		funcBackTrack(ff, bb, &doneCache, (*icbIter).second);

	}
}

void Misconfig::constructFunc2ConfUsageMapping(Func2ConfUsageMap* f2pmap, ConfUsageInfo* confUsage) {
	//1. put the known functions (in the confusagemap)
	confUsage->getExistingFunction2ParamMapping(f2pmap);

//	errs() << "AFTER getExistingFunction2ParamMapping\n";
//	this->_confUsageInfo.print();
//	this->printFunc2ConfUsageMap(&_func2ConfUsageMap);
//
//	errs() << "CONF PARAM 2 FUNC MAPPING\n";
//	Conf2FuncUsageMap _conf2FuncUsageMap;
//	keyValueReverse(&_func2ConfUsageMap, &_conf2FuncUsageMap);
//	printUsageMap(&_conf2FuncUsageMap);

	//2. do all the functions one by one
	Module::iterator modIter = _module->begin(), modEnd = _module->end();
	for (; modIter != modEnd; modIter++) {
		Function* f = &(*modIter);

		if(f->getNameStr().compare("main") == 0) {
			errs() << "we do have main!!!\n";
		}

		set<Function*> checked;
		construct1Func2ConfUsageMapping(f, f2pmap, &checked);
	}

	//3. clean the empty ones
	list<Function*> deleteList;
	Func2ConfUsageMap::iterator fiter = f2pmap->begin(), fend = f2pmap->end();
	for(; fiter != fend; fiter++) {
		if(!fiter->second || fiter->second->empty()) {
			deleteList.push_back(fiter->first);
		}
	}

	list<Function*>::iterator diter = deleteList.begin(), dend = deleteList.end();
	for(; diter != dend; diter++) {
		f2pmap->erase(*diter);
	}
}

void Misconfig::construct1Func2ConfUsageMapping(Function* f, Func2ConfUsageMap* umap, set<Function*>* checkedSet) {
	if(!f || f->isIntrinsic()) {
		return;
	}

	if(checkedSet->count(f) != 0) {
		return;
	} else {
		checkedSet->insert(f);
	}

//	/**
//	 * [HACKS]
//	 * if it's a system call, ignore it otherwise we will get too much false positive
//	 * because system calls are usually called everywhere...
//	 */
//	if(syscallcache.count(funcname) != 0) {
//		return;
//	}

	//[BUG NOTE]
	//umap[f] could be not null because of initial function set
	if(umap->count(f) == 0) {
		umap->operator [](f) = new set<string>();
	}

	inst_iterator funcIter = inst_begin(f), funcEnd = inst_end(f);
	if (funcIter == funcEnd) { // this means it's either a system call or library call!
		errs() << "-----Empty Function----- " << f->getNameStr() << "\n";
		return;
	}

	for (; funcIter != funcEnd; funcIter++) {
		Instruction* inst = &(*funcIter);

		if (isa<CallInst>(inst)) {
			CallInst* cInst = dyn_cast<CallInst>(inst);

			if (isa<IntrinsicInst>(cInst)) {
				continue;
			}

			Function* callee = getCalledFunction(cInst);
			if(!callee) {
				continue;
			}

//			if(!has1RealCallInstUsage(callee)) {
//				continue;
//			}

			string calleeName = stripFunctionNameSuffix(callee->getNameStr());
			if(callee &&
					calleeName.length() > 0 &&
					(calleeName.compare("fork") == 0 ||
					 calleeName.find("thread") != string::npos ||
					 calleeName.find("process") != string::npos) ) {
				errs() << "MMMMMMMMUAAA%%%%%%%%%%%%%%%%%%%%%%% hahaha, fork!!!\n";
			}

			construct1Func2ConfUsageMapping(callee, umap, checkedSet);

			set<string>* tmpset = umap->operator [](callee);

			if(tmpset) {
//				errs() << "===" << f->getNameStr() << "===\n";
//				errs() << calleeName << "\n";
//				printSet(tmpset, " ");

				umap->operator [](f)->insert(tmpset->begin(), tmpset->end());
			}
		}
	}
}

//heuristic: llvm's global variable might not has the exact name matching with the real global variable
bool Misconfig::isSameGVar(string llvmGvar, string realGvar) {
	if (llvmGvar.size() < realGvar.size()) {
		return false;
	}

	if (llvmGvar.substr(0, realGvar.size()).compare(realGvar) == 0) {
		return true;
	}

	return false;
}

//directly add, not safe if you do not check first
void Misconfig::addGVmap(string directive, GlobalConfValue* gvs, int orgOrVic) {
	map<string, GlobalConfValues*>* map = &_mapGV;
	if (orgOrVic == VICTIM) {
		map = &_mapVictimGV;
	}

	if (map->count(directive)) {
		GlobalConfValues* tset = map->operator [](directive);
		tset->insert(gvs);

	} else {
		GlobalConfValues* tset = new GlobalConfValues();
		tset->insert(gvs);
		map->operator[](directive) = tset;
	}
}

//directly add, not safe if you do not check first
void Misconfig::addLVmap(string directive, StructFieldPairs* sfpair, int orgOrVic) {
	map<string, LocalConfValues*>* map = &_mapLVStructField;
	if (orgOrVic == VICTIM) {
		map = &_mapVictimLVStructField;
	}

	if (map->count(directive)) {
		LocalConfValues* tset = map->operator [](directive);
		tset->insert(sfpair);

	} else {
		LocalConfValues* tset = new LocalConfValues();
		tset->insert(sfpair);
		map->operator[](directive) = tset;
	}
}

void Misconfig::filterLVRedundency() {
	map<string, LocalConfValues* >* lmap = &_mapLVStructField;

	/**
	 * actually there's no need to check the GV map,
	 * because if there's a match in gvmap, it's already ignored
	 * and the infomation in LV map can only go more detailed
	 * instead of go less detailed.
	 *
	 * The "detailness" in terms of sfp is the chain will be longer and
	 * can never be shorter
	 *
	 * so, if a shorter chain cannot match the gv map, its updated version
	 * cannot match either.
	 */

	//2. detect the redundency in lv
	map<string, LocalConfValues* >::iterator iter = lmap->begin(), end = lmap->end();
	for(; iter != end; iter++) {
		string directive = (*iter).first;
		LocalConfValues* lcv = (*iter).second;
		set<StructFieldPairs*>* lsfp = lcv->getStructFieldPairs();

		//use a tmp list to store the pointers
		list<StructFieldPairs*> tmplist;
		set2list(&tmplist, lcv->getStructFieldPairs());

		while(tmplist.size() > 0) {
			StructFieldPairs* sfp = tmplist.front();
			tmplist.pop_front();

			//delete from the original set
			lsfp->erase(sfp);

			if(existInLocalMap(directive, sfp)) {
				//4. delete both from origin and local

				if(_mapVictimLVStructField.count(directive)) {
					LocalConfValues* viclcv = _mapVictimLVStructField[directive];
					viclcv->deleteSFP(sfp);
				}

			} else {
				//oh, it's unique, add it back!!
				lsfp->insert(sfp);
			}
		}
	}
}

bool Misconfig::existInGlobalMap(string directive, StructFieldPairs* sfpair) {
	map<string, GlobalConfValues*>* map = &_mapGV;

	if(map->count(directive)) {
		GlobalConfValues* gcv = map->operator [](directive);
		return gcv->isIncluded(sfpair);

	} else {
		return false;
	}
}

bool Misconfig::existInLocalMap(string directive, StructFieldPairs* sfpair) {
	map<string, LocalConfValues* >* map = &_mapLVStructField;

	if(map->count(directive)) {
		LocalConfValues* lcv = map->operator [](directive);
		return lcv->isIncluded_update(sfpair);

	} else {
		return false;
	}
}

bool Misconfig::testadd2MapVictimLV(string directive, StructFieldPairs* sfpair) {

	//it's ok only both of the map does not exist the infomation
	//**NOTE**
	//in the exsitence check, the update is done
	bool gExist = existInGlobalMap(directive, sfpair);
	bool lExist = existInLocalMap(directive, sfpair);

	if(!gExist && !lExist) {
		//add to localmap.
		addLVmap(directive, sfpair, ORIGIN);
		addLVmap(directive, sfpair, VICTIM);
		return true;

	} else {
		return false;
	}
}

//sorry for use the two logic for the same thing in
//testadd_LV2MapVictimLV && testadd_GV2MapVictimGV
bool Misconfig::testadd2MapVictimGV(string directive, GlobalConfValue* gvs) {
	map<string, GlobalConfValues*>* map = &_mapGV;

	if (map->find(directive) != map->end()) {
		GlobalConfValues* tset = map->operator [](directive);
		bool isNew = tset->insert(gvs);

		if (isNew) {
			addGVmap(directive, gvs, VICTIM);
			return true;
		}

	} else { //even
		addGVmap(directive, gvs, ORIGIN);
		addGVmap(directive, gvs, VICTIM);
		return true;
	}

	return false;
}

void Misconfig::_initializeStringCmp() {
	_strcopycache.insert("strcmp");
	_strcopycache.insert("strncmp");
	_strcopycache.insert("strcasecmp");
	_strcopycache.insert("strncasecmp");
}

/**
 * ===== Clean memory after Stage 1 =======
 *
 * this function clean all the data structures used in stage 1,
 * i.e., the single directive specification inference, including
 *
 * 1. _mapDirectiveConstraints (we already dump them to file!)
 * 2. _metaDataInfo (we don't need type specs!)
 * 3. _lcnv (we already know everything from gvmap and lvmap)
 * 4. _mapGV (_mapVictimGV is already cleared)
 * 5. _mapLVStructField (_mapVictimLVStructField is already cleared)
 */
void Misconfig::cleanMemory_s1() {

	//1. clean the constraints
	_mapDirectiveConstraints.cleanAll();

	//2. metaDataInfo
	_metaDataInfo->clean();
	delete _metaDataInfo;

	//3. _lcnv
	list<ConfnameGvarPair*>::iterator lcngvIter = _lcngv.begin();
	for (; lcngvIter != _lcngv.end(); lcngvIter++) {
		delete *lcngvIter;
	}

	//4. clean _mapGV
	errs() << "_mapGV.size() = " << _mapGV.size() << "\n";
	map<string, GlobalConfValues* >::iterator mgvIter = _mapGV.begin(), mgvEnd = _mapGV.end();
	for(; mgvIter != mgvEnd; mgvIter++) {
		delete (*mgvIter).second;
	}
	_mapGV.clear();

	//5. clean _mapLVStructField
	errs() << "_mapLVStructField.size() = " << _mapLVStructField.size() << "\n";
	map<string, LocalConfValues* >::iterator mlvIter = _mapLVStructField.begin(), mlvEnd = _mapLVStructField.end();
	for(; mlvIter != mlvEnd; mlvIter++) {
		delete (*mlvIter).second;
	}
	_mapLVStructField.clear();
}

/**
 * ===== Clean memory after Stage 2 =======
 *
 * this function clean all the data structures used in stage 2,
 * i.e., the dep directive specification inference, including
 *
 * 1. _func2ConfUsageMap
 */
void Misconfig::cleanMemory_s2() {

	//1. _func2ConfUsageMap
	Func2ConfUsageMap::iterator f2pIter = _func2ConfUsageMap.begin(), f2pEnd = _func2ConfUsageMap.end();
	for(; f2pIter != f2pEnd; f2pIter++) {
		f2pIter->second->clear();
		delete f2pIter->second;
	}
	_func2ConfUsageMap.clear();

	//2.
	delete _ccg;
}

//I'm so happy you come here from the bottom block
void Misconfig::funcMappingAnalysis(string fname) {
//	string fname = _conf.funcMappingFile;

	//Read the file that stores directive-function
	struct stat buffer;
	if (stat(fname.c_str(), &buffer)) {
		errs() << "file " << fname << " not found\n";
		return;
	}
	errs() << "reading file " << fname;

	ifstream infile(fname.c_str());
	string line;
	string directiveName, funcName;

	list<pair<string, string> > _lpEntryFunc; //Stores the pairs of entry name and the parsing function name

	while (getline(infile, line)) {
		istringstream iss(line);
		iss >> directiveName >> funcName;
		_lpEntryFunc.push_back(pair<string, string>(directiveName, funcName));
	}

	//For each function in the entryName-FuncName list, we process the parsing function
	list<pair<string, string> >::iterator iter = _lpEntryFunc.begin(), end = _lpEntryFunc.end();

	for (; iter != end; iter++) {
		directiveName = (*iter).first;
		funcName = (*iter).second;

		Function* f = _module->getFunction(StringRef(funcName));
		if (!f) {
			errs() << "cannot find the function -- " << funcName << "\n";
			continue;
		}

		this->_curDirectiveStr = directiveName;
		errs() << "\n+++++++++++++++++ Function Mapping ParsingPhaseInference Processing CONFNAME "
			   << directiveName << "+++++++++++++++\n";

		//add to confUsageInfo
		Value* farg = getFunctionArg(f, _conf.funcArgIdx);
		this->_confUsageInfo.addConfUsageInfo(_curDirectiveStr, farg, NULL);

		FunctionTrack needsFurtherTrack = analyzeAPIConstraints(NULL, f, _conf.funcArgIdx, NULL, false);

		bool goInside = false;

		switch(needsFurtherTrack) {
		case NO_GO_INSIDE_NO_TRACK_RETURN:
		case NO_GO_INSIDE_BUT_TRACK_RETURN:
			break;

		case KNOW_NOTHING:
		case GO_INSIDE_NO_TRACK_RETURN: //do inter procedure data flow checking
		case GO_INSIDE_AND_TRACK_RETURN:
			goInside = true;
			break;

		default:
			break;
		}

		if(goInside) {
			FACache faCache;

			/*
			 * this means the provided info from PMapping has some problems. But we have to tolerate this error
			 * e.g., in httpd, we get
			 *   AuthnCacheEnable  authn_cache_enable
			 * But, actually authn_cache_enable() only takes two arguments while _conf.funcArgIdx == 3
			 */
			if(f->arg_size() < _conf.funcArgIdx) {
				errs() << "[FATAL] the mapping \"" << directiveName << " " << funcName << "\" is problematic";
				continue;
			}

			Constraints* cons = trackInterProcedureFlow(f, _conf.funcArgIdx, NULL, NULL, &faCache, NULL, false, false);

			_mapDirectiveConstraints.mergeConstraints(this->_curDirectiveStr, cons);

			errs() << DUMP_CONSTRAINTS("");
			dumpConstraints(cons);
			errs() << FINISH_DUMP_CONS;
		}
	}
}

/**
 * for constant gep (ConstantExpr)
 *
 * return value:
 * how many elements is matched in sfp
 * (if 0, it means there's no match)
 */
bool Misconfig::constantGepMatch(ConstantExpr* cgep, StructFieldPairs* sfp, unsigned& skip) {
	assert(cgep->isDereferenceablePointer());

	skip = 0;
//	unsigned tracedepth = cgep->getNumOperands() - 2; //the 1st operand is the global value and the 2nd is the offset

	string structName  = "*";

	list<string>* sf_stlist    = sfp->getStructs();
	list<unsigned>* sf_idxlist = sfp->getFields();

	list<string>::iterator sf_st_iter = sf_stlist->begin(), sf_st_end = sf_stlist->end();
	list<unsigned>::iterator sf_idx_iter = sf_idxlist->begin(), sf_idx_end = sf_idxlist->end();

//	errs() << "[debug] in constantGepMatch\n";

	for(unsigned i = 2;
			i<cgep->getNumOperands() && sf_st_iter != sf_st_end && sf_idx_iter != sf_idx_end;
			i++, sf_st_iter++, sf_idx_iter++) {

		Value* idx = cgep->getOperand(i);
		assert(isa<ConstantInt>(idx));
		unsigned index = dyn_cast<ConstantInt>(idx)->getZExtValue();

//		errs() << "from cgrep: " << structName << " " << index << "\n";
//		errs() << "from gv/lv: " << *sf_st_iter << " " << *sf_idx_iter << "\n";

		if(!SFMatch(structName, index, *sf_st_iter, *sf_idx_iter)) {
			return false;
		}

		skip++;
	}

	return true;
}

bool Misconfig::gepMatch(GetElementPtrInst* gep, string structname, unsigned idx) {
	assert(gep->getNumOperands() == 3);

	string sn = getStructName(gep);

	unsigned ix = UINT_MAX - 1;
	Value* v = gep->getOperand(2);
	if(isa<ConstantInt>(v)) {
		ix = dyn_cast<ConstantInt>(v)->getZExtValue();
	} //else we still need to check because idx might be *, i.e., "UINT_MAX"

//	bool ret = SFMatch(sn, ix, structname, idx);
//
//	errs() << "from gep: " << sn << ", " << ix << "\n";
//	errs() << "from sfp: " << structname << ", " << idx << "\n";
//	errs() << "ret -- " << ret << "\n";
//
//	return ret;
	return SFMatch(sn, ix, structname, idx);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// Printers /////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Misconfig::printLVMap(int orgOrVic) {
	map<string, LocalConfValues*>* map = &_mapLVStructField;

	if (orgOrVic == VICTIM) {
		map = &_mapVictimLVStructField;
		errs() << "----------------------printLVMap (VICTIM)-------------------------\n";

	} else {
		errs() << "----------------------printLVMap (ORIGIN)-------------------------\n";
	}

	std::map<string, LocalConfValues*>::iterator mapIter = map->begin(), mapEnd = map->end();
	for (; mapIter != mapEnd; mapIter++) {
		errs() << "[info] local value -- " << mapIter->first << "\n";

		LocalConfValues* gcv = mapIter->second;
		if(gcv) {
			gcv->print();
		}

		errs() << "-----------------------------------------------\n";
	}
}

void Misconfig::printGVMap(int orgOrVic) {
	map<string, GlobalConfValues*>* map = &_mapGV;

	if (orgOrVic == VICTIM) {
		map = &_mapVictimGV;
		errs() << "----------------------printGVMap (VICTIM)-------------------------\n";

	} else {
		errs() << "----------------------printGVMap (ORIGIN)-------------------------\n";
	}

	std::map<string, GlobalConfValues*>::iterator mapIter = map->begin(), mapEnd = map->end();
	for (; mapIter != mapEnd; mapIter++) {
		errs() << "[info] global value -- " << mapIter->first << "\n";

		GlobalConfValues* gcv = mapIter->second;
		gcv->print();

		errs() << "-----------------------------------------------\n";
	}
}

bool Misconfig::SFPKeywordFilter(StructFieldPairs* sfp) {
	list<string>* nlist = sfp->getStructs();

	list<string>::iterator iter = nlist->begin(), end = nlist->end();
	for(; iter != end; iter++) {
		if(keywordMatch(*iter, &nonInterestedStructs)) {
			return true;
		}
	}

	return false;
}

void Misconfig::printFunc2ConfUsageMap(map<Function*, set<string>* >* usageMap) {

	errs() << "----------------------printUsageMap: # = "
		   << usageMap->size() << "-------------------------\n";

	map<Function*, set<string>* >::iterator iter = usageMap->begin(), end = usageMap->end();
	for(; iter != end; iter++) {
		Function* func = iter->first;
		set<string>* paras = iter->second;

		errs() << func->getNameStr() << "(" << paras->size() << ") | ";

		set<string>::iterator si = paras->begin(), se = paras->end();
		for(; si != se; si++) {
			errs() << *si << " ";
		}
		errs() << "\n";
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// Testers //////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Misconfig::TestSFP() {
	StructFieldPairs sfp;
	sfp.test();
}

void Misconfig::TestGVLVMaps() {
	errs() << "===========================start GV&LV Testing==========================\n";

	//for local variables
	map<string, LocalConfValues* > mapLVStructField;
	map<string, LocalConfValues* > mapVictimLVStructField; //to store new discovered mappings

	StructFieldPairs sfp1; // [<A, 1>, <B, 2>, <C, 3>, <D, 4>]
	list<string>* st1 = sfp1.getStructs();
	list<unsigned>* sf1 = sfp1.getFields();
	st1->push_back("A"); st1->push_back("B"); st1->push_back("C"); st1->push_back("D");
	sf1->push_back(1); 	 sf1->push_back(2);   sf1->push_back(3);   sf1->push_back(4);

	StructFieldPairs sfp2; // [<a, 1>, <b, 2>, <c, 3>, <d, 4>]
	list<string>* st2 = sfp2.getStructs();
	list<unsigned>* sf2 = sfp2.getFields();
	st2->push_back("a"); st2->push_back("b"); st2->push_back("c"); st2->push_back("d");
	sf2->push_back(1); 	 sf2->push_back(2);   sf2->push_back(3);   sf2->push_back(4);

	StructFieldPairs sfp3; // [<X, 5>, <Y, 6>, <Z, 7>]
	list<string>* st3 = sfp3.getStructs();
	list<unsigned>* sf3 = sfp3.getFields();
	st3->push_back("X"); st3->push_back("Y"); st3->push_back("Z");
	sf3->push_back(5);   sf3->push_back(6);   sf3->push_back(7);

	GlobalConfValue gcv1(NULL, &sfp1);
	GlobalConfValue gcv2(NULL, &sfp2);

	testadd2MapVictimGV("xiong", &gcv1);
	testadd2MapVictimGV("xiong", &gcv2);
	testadd2MapVictimLV("xiong", &sfp3);

	/* Now,
	 * ===== GV Map have
	 * <A, 1>, <B, 2>, <C, 3>, <D, 4>
	 * <a, 1>, <b, 2>, <c, 3>, <d, 4>
     * ===== LV
     * <X, 5>, <Y, 6>, <Z, 7>
    **/

	errs() << "----test-1\n";

	StructFieldPairs testsfp1; // [<Z, 7>, <ZZ, 8>]
	list<string>* tst1 = testsfp1.getStructs();
	list<unsigned>* tsf1 = testsfp1.getFields();
	tst1->push_back("Z"); tst1->push_back("ZZ");
	tsf1->push_back(7);   tsf1->push_back(8);

	assert(testadd2MapVictimLV("xiong", &testsfp1));
	printGVMap(ORIGIN);
	printLVMap(ORIGIN);

	errs() << "----test-2\n";

	StructFieldPairs testsfp2; // [<B, 2>, <C, 3>]
	list<string>* tst2 = testsfp2.getStructs();
	list<unsigned>* tsf2 = testsfp2.getFields();
	tst2->push_back("B"); tst2->push_back("C");
	tsf2->push_back(2);   tsf2->push_back(3);

	assert(!testadd2MapVictimLV("xiong", &testsfp2));
	printGVMap(ORIGIN);
	printLVMap(ORIGIN);

	errs() << "----test-3\n";

	StructFieldPairs testsfp3; // [<Y, 6>, <Z, 7>]
	list<string>* tst3 = testsfp3.getStructs();
	list<unsigned>* tsf3 = testsfp3.getFields();
	tst3->push_back("Y"); tst3->push_back("Z");
	tsf3->push_back(6);   tsf3->push_back(7);

	assert(!testadd2MapVictimLV("xiong", &testsfp3));
	printGVMap(ORIGIN);
	printLVMap(ORIGIN);

	errs() << "----test-4\n";

	StructFieldPairs testsfp4; // [<X, 5>, <Y, 6>, <Z, 7>, <ZZ, 8>]
	list<string>* tst4 = testsfp4.getStructs();
	list<unsigned>* tsf4 = testsfp4.getFields();
	tst4->push_back("X"); tst4->push_back("Y"); tst4->push_back("Z"); tst4->push_back("ZZ");
	tsf4->push_back(5);   tsf4->push_back(6);   tsf4->push_back(7);   tsf4->push_back(8);

	assert(!testadd2MapVictimLV("xiong", &testsfp4));
	printGVMap(ORIGIN);
	printGVMap(VICTIM);
	printLVMap(ORIGIN);
	printLVMap(VICTIM);

	filterLVRedundency();

	errs() << "----test-5 (after filtering)\n";

	printGVMap(ORIGIN);
	printGVMap(VICTIM);
	printLVMap(ORIGIN);
	printLVMap(VICTIM);

	this->_mapGV.clear();
	this->_mapVictimGV.clear();
	this->_mapLVStructField.clear();
	this->_mapVictimLVStructField.clear();

	errs() << "[info] Congratulations!!! You passed the GV&LV checking! Go ahread!!\n";
}

void Misconfig::TestStrips() {
	string st1 = "union.anon.464945";
	string st2 = "struct.err_state_st";
	string st3 = "struct.comp_method_st.464875";

	string sst1 = stripStructName(st1);
	string sst2 = stripStructName(st2);
	string sst3 = stripStructName(st3);

	errs() << st1 << " --> " << sst1 << "\n";
	errs() << st2 << " --> " << sst2 << "\n";
	errs() << st3 << " --> " << sst3 << "\n";
}

void Misconfig::PrintFunctionList2File() {
	string outputf = "_functionlist";
	std::ofstream out(outputf.c_str());
	if (!out) {
		errs() << "[error] Cannot open output file" << outputf << "\n";
		return;
	}

	int count = 0;
	Module::iterator modIter = _module->begin(), modEnd = _module->end();
	for (; modIter != modEnd; modIter++) {
		Function* f = &(*modIter);

		if(f && f->hasName()) {
			out << f->getNameStr() << "\n";
			count++;
		}
	}

	out.close();

	errs() << "[info] there're " << int2string(count) << "functions in this module in total\n";
}

void Misconfig::PrintStructGVList2File() {
	string outputf = "_struct_gv_list";

	std::ofstream out(outputf.c_str());
	if (!out) {
		errs() << "[error] Cannot open output file" << outputf << "\n";
		return;
	}

	Module::global_iterator iter = _module->global_begin(), end = _module->global_end();
	for (; iter != end; iter++) {
		Type* t = (*iter).getType();
		if(isStructType(t)) {
			string stname = getStructName(t);
			if(stname.size() > 0) {
				out << stname << "\n";
			}
		}
	}

	out.close();
}

void Misconfig::PrintGVName2File() {
	string outputf = "_gvname_list";

	std::ofstream out(outputf.c_str());
	if (!out) {
		errs() << "[error] Cannot open output file" << outputf << "\n";
		return;
	}

	Module::global_iterator iter = _module->global_begin(), end = _module->global_end();
	for (; iter != end; iter++) {
		out << (*iter).getNameStr() << "\n";
	}

	out.close();
}

void Misconfig::keyValueReverse(map<string, set<string>* >* input, map<string, set<string>* >* output) {
	map<string, set<string>* >::iterator iiter = input->begin(), iend = input->end();
	for(; iiter != iend; iiter++) {
		string key = iiter->first;
		set<string>* value = iiter->second;

		set<string>::iterator viter = value->begin(), vend = value->end();
		for(; viter != vend; viter++) {
			if(output->count(*viter) == 0) {
				output->operator [](*viter) = new set<string>();
			}
			output->operator [](*viter)->insert(key);
		}
	}
}
