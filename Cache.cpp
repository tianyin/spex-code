/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#include "Cache.h"
#include <stdio.h>
#include <stdlib.h>

/* Insert a function constraint to the function constraints list
 * check whether the function/argPos pair already exists. If it does, only update
 * its constraint list
 */
void FunctionCache::insertFuncConstraint(FuncArgPair* newPair, Constraint* newConstraint) {
	Constraints* newCons = new Constraints();
	newCons->insert(newConstraint);

	insertFuncConstraints(newPair, newCons);
}

void FunctionCache::insertFuncConstraints(FuncArgPair* newPair, Constraints* newConstraints) {
	string fname = newPair->getFuncName();
	unsigned pos = newPair->getArgPosition();

	pair<FuncArgPair*, Constraints* >* fcpair = getFapNConstraints(fname, pos);
	if(fcpair) {
		Constraints* cs = fcpair->second;
		cs->insert(newConstraints);

	} else {
		if(_fcache.count(fname) == 0) {
			_fcache[fname] = new FuncCacheType();
			_fcache[fname]->push_back(new pair<FuncArgPair*, Constraints* >(newPair, newConstraints));

		} else {
			_fcache[fname]->push_back(new pair<FuncArgPair*, Constraints* >(newPair, newConstraints));
		}
	}
}

//void FunctionCache::insertFuncConstraint(string funcname, unsigned pos, bool checkReturn, Constraint* newConstraint) {
//	pair<FuncArgPair*, Constraints* >* fcpair = getFapNConstraints(funcname, pos);
//
//	if(fcpair) {
//		Constraints* cs = fcpair->second;
//		cs->insert(newConstraint);
//
//	} else {
//		FuncArgPair* fap = new FuncArgPair(funcname, pos, checkReturn, 0);
//
//		Constraints* newCons = new Constraints();
//		newCons->insert(newConstraint);
//
//		_cachedFunction.push_back(new pair<FuncArgPair*, Constraints* >(fap, newCons));
//	}
//}

void FunctionCache::insertFuncConstraints(string funcname, unsigned pos, bool checkReturn, Constraints* newConstraints) {
	FuncArgPair* fap = new FuncArgPair(funcname, pos, checkReturn, 0);

	insertFuncConstraints(fap, newConstraints);
}

FuncArgPair* FunctionCache::getFuncArgPair(string func, unsigned pos) {
	if(_fcache.count(func)) {
		FuncCacheType* fc = _fcache[func];

		fc_iterator iter = fc->begin(), end = fc->end();

		for (; iter != end; iter++) {
			pair<FuncArgPair*, Constraints* >* fcpair = *iter;
			FuncArgPair* fap = fcpair->first;

			if (fap->FACompare(func, pos) == true) {
				return fap;
			}
		}
	}

	return NULL;
}

void FunctionCache::printFAP() {
	map<string, FuncCacheType*>::iterator fIter = _fcache.begin(), fEnd = _fcache.end();

	for(; fIter != fEnd; fIter++) {
		FuncCacheType* fc = (*fIter).second;
		fc_iterator iter = fc->begin(), end = fc->end();

		for (; iter != end; iter++) {
			pair<FuncArgPair*, Constraints* >* fcpair = *iter;
			FuncArgPair* fap = fcpair->first;

			fap->printAll();
		}
	}
}

void FunctionCache::printAll() {
	errs() << "-------------FunctionCache::printAll()---------------------\n";
	map<string, FuncCacheType*>::iterator fIter = _fcache.begin(), fEnd = _fcache.end();

	for(; fIter != fEnd; fIter++) {
		FuncCacheType* fc = (*fIter).second;
		fc_iterator iter = fc->begin(), end = fc->end();

		for (; iter != end; iter++) {
			pair<FuncArgPair*, Constraints* >* fcpair = *iter;
			FuncArgPair* fap = fcpair->first;
			fap->printAll();

			errs() << "-------------------------------------0-\n";

			Constraints* cons = fcpair->second;
			cons->printAll();
		}
	}
}

pair<FuncArgPair*, Constraints* >* FunctionCache::getFapNConstraints(string func, unsigned pos) {
	if(_fcache.count(func)) {
		FuncCacheType* fc = _fcache[func];

		fc_iterator iter = fc->begin(), end = fc->end();

		for (; iter != end; iter++) {
			pair<FuncArgPair*, Constraints* >* fcpair = *iter;
			FuncArgPair* fap = fcpair->first;

			if (fap->FACompare(func, pos) == true) {
				return fcpair;
			}
		}
	}

	return NULL;
}

//bool FunctionCache::needCheckReturn(FuncArgPair& fa) {
//	fc_iterator iter = _cachedFunction.begin(), end = _cachedFunction.end();
//
//	for (; iter != end; iter++) {
//		pair<FuncArgPair*, Constraints* >* fcpair = *iter;
//		FuncArgPair* fap = fcpair->first;
//
//		if (fap->FACompare(fa) == true) {
//			return fap->_checkReturn;
//		}
//	}
//
//	return false;
//}
//
//bool FunctionCache::needGoInside(FuncArgPair& fa) {
//	fc_iterator iter = _cachedFunction.begin(), end = _cachedFunction.end();
//
//	for (; iter != end; iter++) {
//		pair<FuncArgPair*, Constraints* >* fcpair = *iter;
//		FuncArgPair* fap = fcpair->first;
//
//		if (fap->FACompare(fa) == true) {
//			return fap->_goInside;
//		}
//	}
//
//	return false;
//}

/* Initialize the structure constraint list. 
 * Let's count the the field from 0. Sorry not consistent with
 * function arg pair... 555 because i'm crazy~~
 * @qiqi, you are crazy!!! kick your ass!!!
 */
void StructCache::_initialize() {
	string structName;

	////////////////////////////////////////////////////////
	//All About Sockets
	////////////////////////////////////////////////////////

	structName = "sockaddr_in";
	insertStructCon(structName, 1,
		new StructConstraint(structName, 1, "PORT"));

	structName = "sockaddr_in";
	insertStructCon(structName, 2,
		new StructConstraint(structName, 2, "IP_ADDRESS"));

	structName = "sockaddr_in6";
	insertStructCon(structName, 1,
		new StructConstraint(structName, 1, "PORT"));

	structName = "sockaddr_in6";
	insertStructCon(structName, 2,
		new StructConstraint(structName, 2, "IP_ADDRESS"));

	structName = "in_addr";
	insertStructCon(structName, 0,
		new StructConstraint(structName, 0, "IP_ADDRESS"));

	////////////////////////////////////////////////////////
	//All About Time
	////////////////////////////////////////////////////////
	structName = "timeval";
	insertStructCon(structName, 0,
		new StructConstraint(structName, 0, "SECOND"));

	structName = "timeval";
	insertStructCon(structName, 1,
		new StructConstraint(structName, 1, "MICROSECOND"));

	structName = "timespec";
	insertStructCon(structName, 0,
			new StructConstraint(structName, 0, "SECOND"));

	structName = "timespec";
	insertStructCon(structName, 1,
			new StructConstraint(structName, 1, "NANOSECOND"));

	structName = "timezone";
	insertStructCon(structName, 0,
			new StructConstraint(structName, 0, "tz_minuteswest")); /* minutes west of Greenwich */

	structName = "timezone";
	insertStructCon(structName, 1,
			new StructConstraint(structName, 1, "tz_dsttime")); /* type of dst correction */
}

/**
 * we need to check the conficts!
 * e.g., in data ontap, the sockaddr_in structure is different from the normal Linux interface
 */
void StructCache::_loadStructSemanticDB(string path) {
	ifstream infile(path.c_str());

	string line;
	string token;

    while(getline(infile, line)) {
    	line = trim(line);

    	if(line.size() == 0 || line.find_first_of('#', 0) == 0 || line.find_first_of('?', 0) == 0
    			|| line.find_first_of('!', 0) == 0) {
    		continue;
    	}

    	stringstream ss(line);

    	//struct name
    	getline(ss, token, ',');
    	token = trim(token);
    	string structName = token;

    	//field
    	getline(ss, token, ',');
    	token = trim(token);
    	unsigned idx = atoi(token.c_str());

    	//semantic
    	getline(ss, token, ',');
    	token = trim(token);
    	string semantic = token;

    	//check the conflict ones
    	StructCache::sc_iterator iter = findStruct(structName, idx);
    	if (iter != end()) {
    		_scache.remove(*iter);
    	}

    	insertStructCon(structName, idx,
    			new StructConstraint(structName, idx, semantic));
    }
}

StructCache::sc_iterator StructCache::findStruct(string structName, unsigned field) {
	StructFieldPairs tmp(structName, field);

	sc_iterator iter = _scache.begin(), end = _scache.end();
	for(; iter!=end; iter++){
		StructFieldPairs* p = (*iter)->first;
		if(*p == tmp){
			return iter;
		}
	}

	return end;
}

void StructCache::deleteStructInfo(string structName) {
	StructCacheType removelist;

	sc_iterator iter = _scache.begin(), end = _scache.end();
	for(; iter!=end; iter++) {
		StructFieldPairs* p = (*iter)->first;

		if(p->getFirstStructName().compare(structName) == 0) {
			removelist.push_back(*iter);
		}
	}

	//delete
	sc_iterator rmIter = removelist.begin(), rmEnd = removelist.end();
	for(; rmIter != rmEnd; rmIter++) {
		_scache.remove(*rmIter);
	}
}

void StructCache::printStructCache() {
	sc_iterator iter = _scache.begin(), end = _scache.end();
	for(; iter!=end; iter++) {
		StructFieldPairs* p = (*iter)->first;
		p->print();
	}
}

void StructCache::insertStructCon(string structName, unsigned field, Constraint* newConstraint) {

	StructFieldPairs* newPair = new StructFieldPairs(structName, field);
	Constraints* newCon = new Constraints();
	newCon->insert(newConstraint);

	pair<StructFieldPairs*, Constraints* >* newStructConPair =
		new pair<StructFieldPairs*, Constraints* >(newPair, newCon);

	_scache.push_back(newStructConPair);
}

void MapDirectiveConstraints::mergeConstraints(string directive, Constraints* newConstraints) {

	if(newConstraints && newConstraints->size() > 0) {

		if(_mapDirectiveConstraints.find(directive) == _mapDirectiveConstraints.end()){
			_mapDirectiveConstraints[directive] = new Constraints();
		}

		mergeConstraints(_mapDirectiveConstraints[directive], newConstraints);
	}
}

void MapDirectiveConstraints::mergeConstraint(string directive, Constraint* newConstraint) {

	if(_mapDirectiveConstraints.find(directive) == _mapDirectiveConstraints.end()) {
		_mapDirectiveConstraints[directive] = new Constraints();
	}

	Constraints newCons;
	newCons.insert(newConstraint);
	mergeConstraints(_mapDirectiveConstraints[directive], &newCons);
}

void MapDirectiveConstraints::mergeConstraints(Constraints* oldConstraints, Constraints* newConstraints) {
	list<Constraint*>::iterator nb = newConstraints->begin(), ne = newConstraints->end();
	for(; nb != ne; nb++) {
		Constraint* c = *nb;

		bool exist = false;
		list<Constraint*>::iterator ob = oldConstraints->begin(), oe = oldConstraints->end();
		for(; ob != oe; ob++) {
			if((*ob)->compareTo(c)) {
				exist = true;
				break;
			}
		}
		if(!exist) {
			oldConstraints->insert(c);
		}
	}
}

void MapDirectiveConstraints::dumpAllConstraints() {
	map<string, Constraints*>::iterator ib = _mapDirectiveConstraints.begin(), ie = _mapDirectiveConstraints.end();

	errs() << "=============================START DUMP ALL=======================================\n";

	for(; ib != ie; ib++) {
		errs() << "++++++++++++++++++++++++++++++" << (*ib).first << "+++++++++++++++++++++++++++++++++++++\n";
		list<Constraint*>::iterator lb = (*ib).second->begin(), le = (*ib).second->end();
		for(; lb != le; lb++) {
			(*lb)->dump();
		}
	}
	errs() << "==============================END DUMP ALL========================================\n";
}

void MapDirectiveConstraints::dumpAllConstraintsToFile(string fname) {
//	std::ofstream out(fname.c_str(), ios::out | ios::app);
	std::ofstream out(fname.c_str());
	if (!out) {
		errs() << "[error] Cannot open output file" << fname << "\n";
		return;
	}

	map<string, Constraints*>::iterator ib = _mapDirectiveConstraints.begin(), ie = _mapDirectiveConstraints.end();
	out << "=============================START DUMP ALL=======================================\n";

	for(; ib != ie; ib++) {
		out << "++++++++++++++++++++++++++++++" << (*ib).first << "+++++++++++++++++++++++++++++++++++++\n";
		list<Constraint*>::iterator lb = (*ib).second->begin(), le = (*ib).second->end();
		for(; lb != le; lb++) {
			(*lb)->dumpToFile(out);
		}
		out << "++++++++++++++++++++++++++++++ End Directive +++++++++++++++++++++++++++++++++++++\n";
	}

	out << "==============================END DUMP ALL========================================\n";

	out.close();
}

void MapDirectiveConstraints::cleanAll() {
	//trace all the constraints we newed so we can destroy them in the end
	set<Constraint*> traceConstraints;

	map<string, Constraints*>::iterator ib = _mapDirectiveConstraints.begin(), ie = _mapDirectiveConstraints.end();
	for(; ib != ie; ib++) {
		list<Constraint*>::iterator lb = (*ib).second->begin(), le = (*ib).second->end();
		for(; lb != le; lb++) {
			traceConstraints.insert(*lb);
		}

		delete (*ib).second;
	}
	_mapDirectiveConstraints.clear();

	set<Constraint*>::iterator ic = traceConstraints.begin();
	for(; ic != traceConstraints.end(); ic++) {
		delete (*ic);
	}
}
