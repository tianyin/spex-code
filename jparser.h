#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_SIZE 1024

FILE* jfile;

struct funcSemantic {
    char* func_nm;
    int arg_idx;
    char* struct_nm;
    char* struct_fld;
    char* type;
    int return_val;
};

int open_jparser(char* filepath) {
    int errno;
    jfile = fopen(filepath, "r");

    if(!jfile) {
       printf("[error] Fail to open file %s: %s\n", filepath, strerror(errno));
       return -1;
    }
    return 0;
}

void jscopy(char** p, char* token) {
    *p = malloc(strlen(token));
    memset(*p, 0, strlen(*p));
    strcpy(*p, token);
}

int jgetnext(struct jcall* jjc) {
    char line[MAX_LINE_SIZE];
    memset(line, 0, MAX_LINE_SIZE);
    
    if(fgets(line, sizeof(line), jfile)) {
        char* token;
       
        //func_nm 
        token = strtok(line, ",");
        jscopy(&(jjc->func_nm), token);
        //jjc->func_nm = malloc(strlen(token));
        //memset(jjc->func_nm, 0, strlen(jjc->func_nm));
        //strcpy(jjc->func_nm, token);        

        //arg_idx
        token = strtok(NULL, ",");
        jjc->arg_idx = atoi(token);

        //struct_nm
        token = strtok(NULL, ",");
        if(atoi(token) == 0) {
           jjc->struct_nm = NULL;
        } else {
           jscopy(&(jjc->struct_nm), token);
        }

        //struct_fld
        token = strtok(NULL, ",");
        if(atoi(token) == 0) {
           jjc->struct_fld = NULL;
        } else {
           jscopy(&(jjc->struct_fld), token);
        }

        //type
        token = strtok(NULL, ",");
        jscopy(&(jjc->type), token);        

        //return_val
        token = strtok(NULL, ",");
        jjc->return_val = atoi(token);

        return 0;
 
    } else {
        return -1;
    }
}

free_jcall(struct jcall* xjj) {
    free(xjj->func_nm);
    free(xjj->type);
    if(xjj->struct_nm) {
        free(xjj->struct_nm);
        free(xjj->struct_fld);
    }
}

void close_jparser() {
    fclose(jfile);
}

void print_jcall(struct jcall* jjcall) {
    printf("{\n");
    printf("    func_nm: %s\n", jjcall->func_nm);
    printf("    arg_idx: %d\n", jjcall->arg_idx);
    printf("    struct_nm: %s\n", jjcall->struct_nm);
    printf("    struct_fld: %s\n", jjcall->struct_fld);
    printf("    type: %s\n", jjcall->type);
    printf("    return_val: %d\n", jjcall->return_val);
    printf("}\n");
}

int main () {
    open_jparser("/home/tianyin/Testing_Framework/sys_lib_call/syscall_jva");
    //jparser_getnext(NULL);
    struct jcall xjjcall; 
    while(!jgetnext(&xjjcall)) {
    	print_jcall(&xjjcall);
        free_jcall(&xjjcall);
    }
    //jgetnext(NULL);
    //jgetnext(NULL);
    close_jparser();
}
