/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#ifndef _CACHE_H_
#define _CACHE_H_

#include <map>
#include <list>
#include <iostream>
#include <fstream>
#include "llvm/Support/raw_ostream.h"
#include "Constraints.h"

#include "Utils.h"
#include "Container.h"

#define USAGE_FUNCTION "FuncUsage.db"
#define CONTROL_FUNCTION "FuncControl.db"
#define AFFECT_FUNCTION "FuncAffect.db"

using namespace std;
using namespace llvm;

struct FuncSemantic {
    string func_name;
    unsigned arg_idx;
    string struct_name;
    int struct_fld;
    string semantics;
    int returnCheck;
    int goInside;

    void print() {
        errs() << "{" << "\n"
               << "   func_name:   " << func_name << "\n"
               << "   arg_idx:     " << arg_idx << "\n"
               << "   struct_name: " << struct_name << "\n"
               << "   struct_fld:  " << struct_fld << "\n"
               << "   semantics:   " << semantics << "\n"
               << "   returnCheck: " << returnCheck << "\n"
               << "   goInside:    " << goInside << "\n"
               << "}" << "\n"; 
    }
};

class FACache {
public:
	bool exist(string funcname, unsigned argument) {
		if(_faMap.count(funcname) == 0) {
			return false;

		} else {
			set<unsigned>* argset = _faMap[funcname];

			if(argset->find(argument) == argset->end()) {
				return false;

			} else {
				return true;
			}
		}
	}

	void insert(string funcname, unsigned argument) {
		if(_faMap.count(funcname) == 0) {
			set<unsigned>* argset = new set<unsigned>();
			argset->insert(argument);

			_faMap[funcname] = argset;

		} else {
			set<unsigned>* argset = _faMap[funcname];

			if(argset->find(argument) == argset->end()) {
				argset->insert(argument);

			} //else means exists
		}
	}

	int size() {
		return _faMap.size();
	}

private:
	map<string, set<unsigned>* > _faMap;
};

class FunctionCache {

#define ARGUMENT_FUNC 1
#define RESOURCE_FUNC 2

public:
	typedef list< pair<FuncArgPair*, Constraints* >* > FuncCacheType;
	typedef FuncCacheType::iterator fc_iterator;

	void insertFuncConstraint(FuncArgPair* fap, Constraint* newConstraint);

	void insertFuncConstraints(FuncArgPair* fap, Constraints* newConstraints);

	void insertFuncConstraints(string funcname, unsigned pos, bool checkReturn, Constraints* newConstraint);

	FuncArgPair* getFuncArgPair(string func, unsigned pos);

	pair<FuncArgPair*, Constraints* >* getFapNConstraints(string func, unsigned pos);

	void _Initialize(int mode);

//	void _loadSemanticDB(string path);

	void printFAP();

	void printAll();

private:
	map<string, FuncCacheType*> _fcache;
};


/*
 * ugly implementation, should improve!!!
 */
class StructCache {
public:
	typedef list< pair<StructFieldPairs*, Constraints* >* > StructCacheType;
	typedef StructCacheType::iterator sc_iterator;

	StructCache() {
		_initialize();
	}

	void _loadStructSemanticDB(string path);

	void insertStructCon(string structName, unsigned field, Constraint* newConstraint);

	void insertStructCon(FuncSemantic* fs, Constraint* newConstraint);

	bool hasStruct(string structName, unsigned field);

	sc_iterator findStruct(string structName, unsigned field);

	void deleteStructInfo(string structName);

	sc_iterator begin(){
		return _scache.begin();
	}

	sc_iterator end(){
		return _scache.end();
	}

	void printStructCache();

	void _initialize();

private:
	StructCacheType _scache;
};

class MapDirectiveConstraints {
public:
	void mergeConstraints(string directive, Constraints* newConstraints);

	void mergeConstraint(string directive, Constraint* newConstraint);

	void dumpAllConstraints();

	void dumpAllConstraintsToFile(string fname);

	void cleanAll();

private:
	void mergeConstraints(Constraints* oldConstraints, Constraints* newConstraints);

	map<string, Constraints*> _mapDirectiveConstraints; //mapping the directive and all its constraints;
};

#endif
