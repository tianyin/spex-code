/*
 * Common.cpp
 *
 *  Created on: Aug 30, 2012
 *      Author: tianyin
 */

#include "Common.h"

/* LLVM has intrinsic functions. Generally we want to ignore
 * them, but sometimes we have to deal with them. For example,
 * LLVM uses its llvm.memset.p0i8.i32() for memset(). And we
 * should care for them so that we don't miss memset()...
 * Return Value:
 * 		true if the stripped function name is interesting for us
 * 		false if we really want to ignore it
 * Parameters:
 * 		Function* func:
 */
bool stripIntrinsic(Function* func, StringRef& strippedName) {
	/*If it is not intrinsic at all, return true*/
	if (!func->isIntrinsic()) {
		strippedName = func->getName();
		return true;
	}
	/* else check the list of interesting function names
	 * that may be prefixed by "llvm"...
	 */
	StringRef funcName = func->getName();
	if (funcName.find(".memset.") != StringRef::npos) {
		strippedName = "memset";
		return true;

	} else if (funcName.find(".memcpy.") != StringRef::npos) {
		strippedName = "memcpy";
		return true;
	}
	return false;
}

///* The struct name becomes struct.real_name.xyz
// * in llvm in use. So we extract the real_name
// * from this mess
// */
//StringRef stripStructType(StringRef irName) {
//
//	pair<StringRef, StringRef> str_pair = irName.split('.');
//	if(!str_pair.first.equals("struct")) {
//		str_pair = str_pair.first.split('.');
//		//return irName;
//	} else {
//		str_pair = str_pair.second.split('.');
//	}
//
//	return str_pair.first;
//}

void getTrueFalseBlocks(BranchInst* bi, list<BasicBlock*>* trueBlockList,
		list<BasicBlock*>* falseBlockList) {
	BasicBlock* firstTrueBlock = bi->getSuccessor(0);
	BasicBlock* firstFalseBlock = bi->getSuccessor(1);

	list<BasicBlock*>* blocks = new list<BasicBlock*>();
	blocks->push_back(firstTrueBlock);

	while (1) {
		BasicBlock* bb = *(blocks->begin());
		blocks->pop_front();
		list<BasicBlock*>* successors = getSuccessors(bb);
		//		list<BasicBlock*>* predOfSucc = GetPredOfSuccessors(bb);

		BasicBlock* hitBlock;
		if (contain(successors, firstFalseBlock) == true) {
			//that mains it's if(A) {}
			//get all the Basic Blocks from bb to firstFalseBlock, i.e., trueList
			return;
		} else if (predContain(successors, firstFalseBlock, &hitBlock)) {
			//that mains it's if(A) {} else {}
			//get all the Basic Blocks from firstTrueBlock to hitBlock, i.e., trueList
			//get all the Basic Blocks from firstFalseBlock to hitBlock, i.e., falseList
			return;
		} else {
			//			trueBlockList->push_back(bb);
			blocks->merge(*successors);
			delete successors;
		}
	}
}

list<BasicBlock*>* getSuccessors(BasicBlock* bb) {
	list<BasicBlock*> *successors = new list<BasicBlock*>();

	for (succ_iterator SI = succ_begin(bb), E = succ_end(bb); SI != E; ++SI) {
		successors->push_back(*SI);
	}

	return successors;
}

bool contain(list<BasicBlock*>* lb, BasicBlock* tb) {
	for (list<BasicBlock*>::iterator I = lb->begin(), E = lb->end(); I != E;
			++I) {
		if (*I == tb) {
			return true;
		}
	}
	return false;
}

bool predContain(list<BasicBlock*>* lb, BasicBlock* tb, BasicBlock** hit) {
	for (list<BasicBlock*>::iterator I = lb->begin(), E = lb->end(); I != E;
			++I) {
		for (pred_iterator pi = pred_begin(*I), pe = pred_end(*I); pi != pe;
				++pi) {
			BasicBlock* pred = *pi;
			if (pred == tb) {
				*hit = *I;
				return true;
			}
		}
	}
	return false;
}

/*
 * check whether bb2 is a successor of bb1
 * return: true & false
 */
bool isSuccessor(BasicBlock* bb1, BasicBlock* bb2) {
	for (pred_iterator PI = pred_begin(bb2), E = pred_end(bb2); PI != E; ++PI) {
		BasicBlock* pred = *PI;
		if (pred == bb1) {
			return true;
		}
	}
	return false;
}

/*
 * Here "empty function" means the function does not have a body
 * it should be either a system call or a lib call, or sth else?
 */
bool isEmptyFunction(Function* f) {
	if (!f) {
		return false;
	}

	if (f->isIntrinsic()) {
		return false;
	}

	inst_iterator funcIter = inst_begin(f), funcEnd = inst_end(f);
	if (funcIter == funcEnd) { // this means it's either a system call or library call!
		return true;
	}

	return false;
}

Function* getCalledFunction(CallInst* callInst) {
	Function* callee = callInst->getCalledFunction();

	if (callee) {
		return callee;

	} else { //damn it, it's either a combined CallInst, or a function pointer

		for (unsigned i = 0; i < callInst->getNumOperands(); i++) {
			Value* v = callInst->getOperand(i);

			if (!v) {
				return NULL;
			}

			if (isa<ConstantExpr>(v)) {
				ConstantExpr* cExpr = dyn_cast<ConstantExpr>(v);

				if (cExpr->isCast()) {
					Value* mf = cExpr->getOperand(0); //maybe func?

					if (isa<Function>(mf)) {
						Function* f = dyn_cast<Function>(mf);
						return f;

					} else {
						errs() << "[error] undefined cases: the first operand is zero! omg\n";
//						errs() << "        " << *callInst << "\n";
						return NULL;
					}

				} else {
//					errs() << "[error] undefined cases: not a cast!" << *callInst << "\n";
					errs() << "[error] undefined cases: not a cast!\n";
//					errs() << "        " << *callInst << "\n";
					return NULL;
				}
			}
		}
		//probably function pointers, or like this:
		//%0 = call { i32, i32 } asm sideeffect "rdtsc", "={ax},={dx},~{dirflag},~{fpsr},~{flags}"() nounwind, !dbg !3667763, !srcloc !3667764
		//		errs() << "[error] undefined cases: not a ConstantExpr!" << *callInst << "\n";
		errs() << "[error] undefined cases: not a ConstantExpr!\n";
//		errs() << *callInst << "\n";

		return NULL;
	}

	return NULL;
}

bool isStructType(Type* t) {
	Type* ty = getPointedType(t);

	if (ty->getTypeID() == Type::StructTyID) {
		return true;
	} else {
		return false;
	}
}

bool isStructType(Value* v) {
	return isStructType(v->getType());
}

bool isBasicType(Type* t) {
	Type* ty = getPointedType(t);

	Type::TypeID tyid = ty->getTypeID();
	if (tyid != Type::StructTyID && tyid != Type::VectorTyID
			&& tyid != Type::ArrayTyID && tyid != Type::LabelTyID
			&& tyid != Type::MetadataTyID && tyid != Type::X86_MMXTyID) {

		return true;
	} else {
		return false;
	}
}

string getStructName(GetElementPtrInst* gepi) {
	PointerType* pType = gepi->getPointerOperandType();
	Type* t = pType->getElementType();
	return getStructName(t);
}

string getStructName(Value* v) {
	Type* t = v->getType();
	return getStructName(t);
}

string getStructName(Type* tx) {
	Type* ty = getPointedType(tx);

	string structName = "";

	if (ty->getTypeID() == Type::StructTyID) {
		StructType *STy = cast<StructType>(ty);

		if (!STy->isLiteral()) {
			//Literal structs do not have name
			structName = STy->getName().str();
		}
	}

	return stripStructName(structName);
}

//TODO
bool isFromString(Value* v) {
	return false;
}

/*
 * here we would like to know whether the Value* v is in the following form like:
 *  %conv = sext i8 %1 to i32, !dbg !3755921
 */
bool isChar2IntCast(Value* v) {
	if (isa<CastInst>(v)) {
		CastInst* cast = dyn_cast<CastInst>(v);

		Type* sty = getPointedType(cast->getSrcTy());
		Type* dty = getPointedType(cast->getDestTy());

		if (sty->getTypeID() == Type::IntegerTyID
				&& dty->getTypeID() == Type::IntegerTyID) {

			if (sty->getPrimitiveSizeInBits() == 8
					|| sty->getPrimitiveSizeInBits() == 32) {

				return true;
			}
		}
	}

	return false;
}

/*
 *  This function gives you the answer of whether the Value* v is from a String to Char to Int
 *  The following code snippet is the example:
 *      %6 = load i8** %w, align 4, !dbg !88124
 *      %7 = load i8* %6, !dbg !88124
 *      %conv = sext i8 %7 to i32, !dbg !88124
 *      %cmp = icmp eq i32 %conv, 43, !dbg !88124
 *
 *  which corresponds to the C code
 *  	char *w = ap_getword_conf(cmd->temp_pool, &l);
 * 		if (*w == '+' || *w == '-') { //'+' is 43
 *
 *  Originally, this is used for Misconfig::analyzeRangeConstraints,
 *  please refer the comments in that function
 *
 *  Accordingly, the input v corresponds to %conv
 */
bool isString2Char2Int(Value* v) {
	if (isa<SExtInst>(v)) {
		SExtInst* sei = dyn_cast<SExtInst>(v);

		Type* sty = sei->getSrcTy();
		Type* dty = sei->getDestTy();

		if (sty->getTypeID() == Type::IntegerTyID
				&& dty->getTypeID() == Type::IntegerTyID) {

//			errs() << "getPrimitiveSizeInBits " << sty->getPrimitiveSizeInBits() << "\n";
//			errs() << "getPrimitiveSizeInBits " << dty->getPrimitiveSizeInBits() << "\n";

			if (sty->getPrimitiveSizeInBits() == 8
					|| sty->getPrimitiveSizeInBits() == 32) {
				Value* tv = sei->getOperand(0);

				//if tv is from an i8 pointer p and p is from a i8**, then return true
				if (isa<LoadInst>(tv)) {
					LoadInst* li = dyn_cast<LoadInst>(tv);
					Value* tvv = li->getOperand(0);
					Type* tyy = tvv->getType();

					//if the type of tvv is i8**
					if (isPointer2I8(tyy) && isa<LoadInst>(tvv)) {
						LoadInst* lii = dyn_cast<LoadInst>(tvv);
						Value* tvvv = lii->getOperand(0);
						Type* tyyy = tvvv->getType();

//						errs() << *lii << " | " << *tvvv << "\n";
						if (isPointerPointer2I8(tyyy)) {
//							errs() << "isString2Char2Int is true\n";
//							sei->getOperand(1);
							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

bool isPointer2I8(Type* ty) {
	if (ty->getTypeID() == Type::PointerTyID) {
		PointerType* pType = dyn_cast<PointerType>(ty);
		Type* t = pType->getElementType();

		if (t->getTypeID() == Type::IntegerTyID
				&& t->getPrimitiveSizeInBits() == 8) {
			return true;
		}
	}
	return false;
}

bool isPointerPointer2I8(Type* ty) {
	if (ty->getTypeID() == Type::PointerTyID) {
		PointerType* pType = dyn_cast<PointerType>(ty);
		Type* t = pType->getElementType();

//		errs() << "In PPI8 Ty: " << *ty << " | " << *t << "\n";

		return isPointer2I8(t);
	}
	return false;
}

string getAccurateType(Type* Ty) {
	string ret;
	switch (Ty->getTypeID()) {
	case Type::VoidTyID:
		ret = "void";
		break;
	case Type::FloatTyID:
		ret = "float";
		break;
	case Type::DoubleTyID:
		ret = "double";
		break;
	case Type::X86_FP80TyID:
		ret = "x86_fp80";
		break;
	case Type::FP128TyID:
		ret = "fp128";
		break;
	case Type::PPC_FP128TyID:
		ret = "ppc_fp128";
		break;
	case Type::LabelTyID:
		ret = "label";
		break;
	case Type::MetadataTyID:
		ret = "metadata";
		break;
	case Type::X86_MMXTyID:
		ret = "x86_mmx";
		break;
	case Type::IntegerTyID:
		ret = "i";
		ret += int2string(cast<IntegerType>(Ty)->getBitWidth());
		break;
	default:
		break;
	}

	return ret;
}

bool isNullPointer(Value* v) {
	if (isa<ConstantPointerNull>(v)) {
		return true;
	}

	return false;
}

string getConstantArray(Value* ca) {
	assert(isa<ConstantArray>(ca));

	ConstantArray* st = dyn_cast<ConstantArray>(ca);
	string constantArgument = st->getAsString();
	replace(constantArgument.begin(), constantArgument.end(), '\00', ' ');
	return constantArgument;
}

Type* getPointedType(Type* t) {
	Type* ret = t;

	while (isa<PointerType>(ret)) {
		ret = dyn_cast<PointerType>(ret)->getElementType();
	}

	return ret;
}

bool isSameType(Type* t1, Type* t2) {
	Type* st1 = getPointedType(t1);
	Type* st2 = getPointedType(t2);

	if (st1->getTypeID() == st2->getTypeID()) {
		return true;
	} else {
		return false;
	}
}

string stripTailedDigits(string s) {
	int i = 0;

	string::reverse_iterator iter = s.rbegin(), end = s.rend();
	for (; iter != end; iter++) {
		if (*iter >= '0' && *iter <= '9') {
			i++;
		} else {
			break;
		}
	}

	return s.substr(0, s.size() - i);
}

//the first two parameters are from instuctions like ConstantGetElementPtrInst or GetElementPtrInst
//the last two parameters are from StructFieldPairs, and may have <*, *>
bool SFMatch(string inst_stname, unsigned inst_idx, string sf_stname,
		unsigned sf_idx) {
	//1. compare the struct name
	if (sf_stname.compare("*") != 0 && inst_stname.compare("*") != 0) {
		if (inst_stname.compare(sf_stname) != 0) {
			return false;
		}
	}

	//2. compare the index
	if (sf_idx != UINT_MAX && inst_idx != UINT_MAX) {
		if (sf_idx != inst_idx) {
			return false;
		}
	}

	return true;
}

bool isNumeric(const std::string& s) {
	std::string::const_iterator it = s.begin();
	while (it != s.end() && std::isdigit(*it))
		++it;
	return !s.empty() && it == s.end();
}

/**
 * In some bitcode files, the same functions are placed multiple times
 * with different names like htons58324, htons59108, htons76724
 *
 * actually they point to the same function.
 *
 * [NOTE] Here're two things. we only strip function names with >3 numbers as tails,
 * otherwise we do not strip and think they are different.
 *
 * Remember the magic number *3*, I checked with netapp data ontap who has 59644 functions
 * and this rule stands. One or two numbers could be like ipv4, nfs_v2, etc
 */
string stripFunctionNameSuffix(string funcname) {

	//I don't know whether it is appropriate, anyway, everything is hacks.
	//could be more aggressive? @@
	string stripedname = stripTailedDigits(funcname);
	if (funcname.size() - stripedname.size() > 3) {
		//if the number of tail digits are larger than 3, we think the digits are greated by llvm
		//e.g., in ontap, we have sth like
		//htons58324, htons59108, htons76724, etc
//		errs() << "[info] function name strip: from " << funcname << " to " << stripedname << "\n";
		return stripedname;

	} else {
		return funcname;
	}
}

/**
 * struct name is more difficult to strip than function names.
 * First, they have a prefix "struct." (it's simple), but it has suffix like
 *
 * struct._disk_unique_identifier_t.8834
 * struct.anon.149.14720
 *
 * and also, it could be:
 *
 * union.anon.464945
 *
 * So, here is the algorithm. we go from the back to the front, and
 * filter all the .12345 like tails.
 */
string stripStructName(string stname) {
	string stripedname = stname;
	if (stripedname.substr(0, 7).compare("struct.") == 0) {
		stripedname = stripedname.substr(7);
	}

	while (true) {
		if (stripedname.rfind('.') != string::npos) {
			unsigned dp = stripedname.rfind('.');
			string sub = stripedname.substr(dp + 1);
			if (isNumeric(sub)) {
				stripedname = stripedname.substr(0, dp);
			} else {
				break;
			}
		} else {
			break;
		}
	}

	return stripedname;
}

//Strip the prefix of "struct.", if it exists
string stripStructNamePrefix(string name) {
	if (name.find("struct.") == 0) {
		return name.substr(7);
	}
	return name;
}

Value* getFunctionArg(Function* function, unsigned position) {
	Function::arg_iterator iarg = function->arg_begin(); //, iend = func->arg_end();
	Value *valArg;
	unsigned tmpPos = position;

	while ((tmpPos--) > 0) {
		valArg = &(*iarg);
		iarg++;

	} //now the valArg points to interesting argument value.

	return valArg;
}

bool has1RealCallInstUsage(Function* F) {
	return numOfRealCallInstUsage(F) == 1;
}

int numOfRealCallInstUsage(Function* F) {
//	int c = 0;
//
//	Value::use_iterator u = f->use_begin(), end = f->use_end();
//	for (; u != end; u++) {
//
//		if(isa<CallInst>(*u)) {
//			c ++;
//
//		} else if(isa<ConstantExpr>(*u)) {
//			ConstantExpr* cexpr = dyn_cast<ConstantExpr>(*u);
//			if(cexpr->isCast()) {
//				Value* v = cexpr->getOperand(0);
//				if(isa<CallInst>(v)) {
//					c++;
//				}
//			}
//		}
//	}
//	return c;

	set<CallInst*> calInstSet;
	getRealCallInstUsage(F, &calInstSet);
	return calInstSet.size();
}

void printInstructionSet(set<Instruction*>* iset) {
	errs() << "-------print instruction set ------------\n";
	set<Instruction*>::iterator iter = iset->begin(), end = iset->end();
	for(; iter != end; iter++) {
		errs() << *(*iter) << "\n";
	}
}

void printFunctionSet(set<Function*>* functionSet) {
	errs() << "-------print function set ------------\n";
	set<Function*>::iterator iter = functionSet->begin(), end = functionSet->end();
	for(; iter != end; iter++) {
		errs() << (*iter)->getNameStr() << "\n";
	}
	errs() << "-------end print function ------------\n";
}

void get1stStopInstUsageInFunction(Value* v, set<Function*>* fset) {
	set<Instruction*> iset;
	get1stStopInstUsage(v, &iset);

	set<Instruction*>::iterator iter = iset.begin(), end = iset.end();
	for(; iter != end; iter++) {
		Function* f = (*iter)->getParent()->getParent();
		if(f->getNameStr().compare("tunables_load_defaults") != 0 &&
				f->getNameStr().compare("install_str_setting") != 0) {
			fset->insert(f);
		}
	}
}

void get1stStopInstUsage(Value* v, set<Instruction*>* iset) {
	Value::use_iterator u = v->use_begin(), end = v->use_end();

	for(; u != end; u++) {
		if(isa<Instruction>(*u)) {
			Instruction* ins = dyn_cast<Instruction>(*u);

			if(isa<CallInst>(ins)) {
				Function* f = getCalledFunction(dyn_cast<CallInst>(ins));
				if(f && (f->getNameStr().compare("install_str_setting") == 0 ||
						f->getNameStr().compare("tunables_load_defaults") == 0)) {
					continue;
				}
			}

			iset->insert(ins);
		} else if(isa<ConstantExpr>(*u)) {
			get1stStopInstUsage(*u, iset);
		}
	}
}

void getRealCallInstUsage(Function* F, set<CallInst*>* callinstSet) {
	Value::use_iterator u = F->use_begin(), end = F->use_end();

	for (; u != end; u++) {
		if(isa<CallInst>(*u)) {
			callinstSet->insert(dyn_cast<CallInst>(*u));

		} else if(isa<ConstantExpr>(*u)) {
			ConstantExpr* cexpr = dyn_cast<ConstantExpr>(*u);

			/**
			 * [NOTE] bug
			 * Here constantExpr will have multiple usage! e.g., we see two different callinst:
			 *
			 * (in function @vsf_one_process_login)
			 * 18223   call void bitcast (void (%struct.vsf_session.4*)* @process_post_login to void (%struct.vsf_session*)*)(%struct.vsf_session* %      37), !dbg !10236
			 *
			 * (in function @common_do_login)
			 * 17253   call void bitcast (void (%struct.vsf_session.4*)* @process_post_login to void (%struct.vsf_session*)*)(%struct.vsf_session* %      4), !dbg !9773
			 *
			 * actually they refer to the same cexpr:
			 * void bitcast (void (%struct.vsf_session.4*)* @process_post_login to void (%struct.vsf_session*)*)
			 */
			Value::use_iterator cu = cexpr->use_begin(), ce = cexpr->use_end();
			for(; cu != ce; cu++) {
				if(isa<CallInst>(*cu)) {
					callinstSet->insert(dyn_cast<CallInst>(*cu));
				} else {
					errs() << **cu << "\n";
					errs() << "[in getRealCallInstUsage] NOT A CALLINST!!\n";
					assert(false);
				}
			}
		}
	}
}
