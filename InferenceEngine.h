/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Tianyin Xu, Jiaqi Zhang                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#ifndef _MISCONFIG_H_
#define _MISCONFIG_H_

#include <sys/stat.h>
#include <climits>
#include <stdlib.h>
#include <time.h>

#include "llvm/Pass.h"
#include "llvm/Value.h"
#include "llvm/Function.h"
#include "llvm/Operator.h"
#include "llvm/Constants.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Module.h"
#include "llvm/Metadata.h"
#include "llvm/LLVMContext.h"
#include "llvm/Instructions.h"
#include "llvm/InstrTypes.h"
#include "llvm/IntrinsicInst.h" //for DbgDeclareInst
#include "llvm/ValueSymbolTable.h"

#include "llvm/Analysis/Dominators.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/DebugInfo.h"

#include "llvm/Support/CFG.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/Dwarf.h"

#include "llvm/Transforms/Scalar.h"
#include "llvm/ADT/DenseMap.h"

#include "Utils.h"
#include "Common.h"
#include "Constraints.h"
#include "Cache.h"
#include "Container.h"
#include "Tainting.h"
#include "ControlDepAggregator.h"

#include "MyLoopInfo.h"
#include "MyMetadataInfo.h"
#include "MyCallGraph.h"

/* Some versions of glibc and the binutils libiberty library give
   conflicting prototypes for basename(). We don't use that function
   anyway, but to work around the problem, make libliberty.h think
   that it has already been declared. */
#define HAVE_DECL_BASENAME 1
//#include </usr/include/demangle.h>

#define CPP 1

#define CONFIG_FILE "PInference.conf"

#define DUMP_CONSTRAINTS(c) dumpMark(c)
#define START_DUMP_CONS "************DUMPING CONSTRAINTS ["
#define FINISH_DUMP_CONS "------------FINISH DUMP*************************\n"

#define SINGLE_DIRECTIVE_SPEC 1
#define DEPENDENCY_SPEC 2

#define GLOBAL 1
#define LOCAL  2

#define ORIGIN 1
#define VICTIM 2

#define CALLINST 1
#define INVOKEINST 2

#define FUNCTION 0
#define GLOBAL_VAR 1

namespace llvm {

struct Misconfig: public ModulePass {
	static char ID;

	enum FunctionTrack {
		KNOW_NOTHING = 0,
		GO_INSIDE_NO_TRACK_RETURN,
		NO_GO_INSIDE_BUT_TRACK_RETURN,
		NO_GO_INSIDE_NO_TRACK_RETURN,
		GO_INSIDE_AND_TRACK_RETURN
	};

	virtual bool runOnModule(Module &M);

	explicit Misconfig() :
		ModulePass(ID) {
		DEFINE_CPP = false;

		isFromReturn = false;
		isFromMiddle = false;
//		isFromComparison = false;
	}

	virtual void getAnalysisUsage(AnalysisUsage &AU) const {
//		AU.addRequiredTransitive<LoopInfo>();

//		AU.setPreservesAll();
//		AU.addRequiredTransitive<LoopInfo>();
//		AU.addRequiredTransitive<DominatorTree>();
//		AU.addPreserved("domtree");
//		AU.addRequiredTransitive<AliasAnalysis>();
//		AU.setPreservesCFG();
	}

	//=============The Core Processors ===================

	//both trackDataFlow and trackInterProcedureFlow could be the initiator
	//and they call each other
	//who initiate the process need to initialize the FACache and ValueCache

	void trackDataFlow(Value *v,
			StructFieldPairs* sfp,
			Constraints* upperSpec,
			FACache* faCache, ValueCache* vCache,
			Context* callchain);

	void trackDataFlowCallCheck(Instruction* inst, Function* callee, Value* v,
			int callorinvoke,
			StructFieldPairs* sfp,
			Constraints* specifications,
			FACache* faCache, ValueCache* vcache,
			Context* callchain);

	Constraints* trackInterProcedureFlow(Function *function, unsigned position,
			Instruction* callInst,
			StructFieldPairs* sfp,
			FACache* faCache,
			Context* callchain,
			bool trackReturn,
			bool isDep);

	void trackDataFlowDep(Value* v,
			FACache* faCache, ValueCache* vCache,
			Context* ocallchain);

	void trackDataFlowCallCheckDep(Instruction* inst, Function* callee, Value* v,
			int callorinvoke,
			FACache* faCache, ValueCache* vcache,
			Context* callchain);

//	bool funcDataFlowAnalysis(Function* func, unsigned position, Constraints* constraints,
//			bool isFuncMapping, StructFieldPairs* sfp);

	set<unsigned>* getAffectedArgumentList(Function* func, unsigned argPos, set<Function*>* entrances);

	set<unsigned>* getSysAffectedArgumentList(string funcname, unsigned argPos);

	set<unsigned>* getSysAffectedArgumentList(Function* func, unsigned argPos);

	set<unsigned>* getFuncAffectedArgumentList(Function *func, unsigned position, set<Function*>* entrances);

	//============= Starters ===================

	//Gobal Variables
	void trackSpecFromGV(map<string, GlobalConfValues*>* gmap);

	//Local Variables
	void trackSpecFromLVStruct(map<string, LocalConfValues*>* lvmap);

	//Functions
	void funcMappingAnalysis(string fname);

	//dependency specifications
	void trackDepSpec();

	//============= Specification Analyzers ===================

	FunctionTrack analyzeAPIConstraints(CallInst* callinst, Function* func,
			unsigned argPos, Constraints* specifications, bool isDep);

	Constraints* analyzeStructConstraint(Instruction* inst);

	Constraints* analyzeStructConstraint(StructFieldPairs* isfp);

	Constraints* analyzeStructConstraint(string structname, unsigned field);

	Constraints* analyzeRangeConstraints(Value*, CmpInst*);

	void putTypeConstraint(StringRef);

	void controlDepRecording(BasicBlock* tbb, BasicBlock* fbb,
			string reference,
			DominatorTree* dt, PostDominatorTree* pdt,
			Context* callchain);

	void controlDepRecording(BasicBlock* bb,
			string reference,
			PerBlockControlDepInfo* output,
			DominatorTree* dt, PostDominatorTree* pdt,
			Context* callchain);

	void addMetadataTypeConstraint(StructFieldPairs*);

	void analyzeMetadataTypeConstraints(int mode);

	//============= Initialization ===================

	void _initializeParamUsage();

	void _initializeParamAffect();

	void _initializeFuncAL();

	void _initializeStringCmp();

	void _initializeMetaDataInfo();

	bool _initializeGV();

	bool _initializeLV();

	void readLocalVariables(string fileName);

	void readPredefinedStruct(string fileName);

	void readConfigFile(Config& confs);

	//============= LVUsage stuff ===================

	ValueUsageMap* collectGetElementInsts(map<string, LocalConfValues*>* lcv);

	ValueUsageMap* collectGlobalValues(map<string, GlobalConfValues*>* gcv);

	void findLVUsage(GetElementPtrInst* gepi, map<string, LocalConfValues*>* lcv, list<ValueUsage*>* lvus);

	void findGVUsage(GlobalValue* ingv, map<string, GlobalConfValues*>* gcv, list<ValueUsage*>* gvus, int mode);

	//============= Utility Functions and Auxiliary Functions ===================

	//Find the global variable
	GlobalVariable* getGlobalVariable(StringRef& gvar_name);

	void getInstSrcLocation(Instruction* inst, unsigned& linenum, StringRef& fileName);

	Function* getRealCalledFunction(CallInst* callinst);

	bool isSameGVar(string llvmGvar, string realGvar);

	bool gepMatch(GetElementPtrInst* gep, string structname, unsigned idx);
	bool constantGepMatch(ConstantExpr* cgep, StructFieldPairs* sfp, unsigned& skip);

	void updateSFPMap(map<Value*, unsigned>* sfpMap, Value* src, Value* dst);
	void updateSFPMap(map<Value*, unsigned>* sfpMap, Value* src1, Value* src2, Value* dst);

//	void _demangleMapInitialze();

	bool SFPKeywordFilter(StructFieldPairs* sfp);

	void addConstraintToCurDirective(Constraint* newCon, Instruction* inst);

	void addConstraintsToCurDirective(Constraints*, Instruction*);

	//============= Backward Tracking for Dependency ============================

	bool isFromConfParameter(Value* v, set<string>* params, Context* callchain, bool checkUsage);

	//--- auxiliary functions to implement isFromConfParameter
	bool checkConfParameterByBackTrack(Value* v, set<string>* params, set<Value*>* misscache, bool checkUsage);

	bool isFromFunctionArgument(Value* v, set<unsigned>* argsrc);

	bool isComparedWithConfParameters(Value* v, set<string>* params, set<Value*>* missCache, set<Value*>* valueCache);

	//============= Control-based Semantics ============================

	void getIncubationBB();

	void getLoopInfo();

	void constructFunc2ConfUsageMapping(Func2ConfUsageMap* f2pmap, ConfUsageInfo* confUsage);
	//auxiliary function of constructFunc2ConfUsageMap();
	void construct1Func2ConfUsageMapping(Function* f, Func2ConfUsageMap* umap, set<Function*>* checkedSet);

	void constructConfCallGraph(ConfUsageInfo* confUsage, MiniCallGraph* ccg);

//	void getRealDepParams(ControlDepInfo* depinfo, Func2ConfUsageMap* f2cMap, MiniCallGraph* mcg,
//			set<string>* results);

	//============================================================================

	bool isInterestStructField(GetElementPtrInst* gepi, map<string, LocalConfValues* >* spMap,
			string& structName, unsigned& field, string& directive);

	bool isInterestStructField(ConstantExpr* cexpr, map<string, LocalConfValues* >* spMap,
			string& structName, unsigned& field, string& directive);

	bool isInterestStructField(Type* t, unsigned field, list<StructFieldPairs*>* spList);
	bool isInterestStructField(Value* v, unsigned field, list<StructFieldPairs*>* spList);
	bool isInterestStructField(string structName, unsigned field, list<StructFieldPairs*>* spList);

	bool isNotInterestStructField(Type* t, unsigned field, list<StructFieldPairs*>* spList);
	bool isNotInterestStructField(GetElementPtrInst* gepi, list<StructFieldPairs*>* spMap);
	bool isNotInterestStructField(Value* v, unsigned field, list<StructFieldPairs*>* spList);
	bool isNotInterestStructField(string structName, unsigned field, list<StructFieldPairs*>* spList);


	bool gotoBranch(Value* v, set<int>* rlist, set<Value*>* dCache);

	void funcBackTrack(Function* ff, BasicBlock* bb, set<Value*>* doneCache, Constraints* scc);

	//================Utility function for dep constraints===============

	void mergeIfDependency(string dominatingParam, string dominatingRef, string dominatedParam);
	void mergeIfDependencies(string dominatingParam, string dominatingRef, set<string>* dominatedParams);

	void mergeMinMaxDependencies(string d1, string d2, CmpInst::Predicate pred, string currDirective);
	void mergeMinMaxDependencies(set<string>* d1, string d2, CmpInst::Predicate pred, string currDirective);
	void mergeMinMaxDependencies(string d1, set<string>* d2, CmpInst::Predicate pred, string currDirective);

	//=============== cleaner ====================

	void cleanMemory_s1();

	void cleanMemory_s2();

	//============= GV Maps & LV Maps ===================

	void addGVmap(string directive, GlobalConfValue* gvs, int orgOrVic);

	void addLVmap(string directive, StructFieldPairs* sfpair, int orgOrVic);

	bool testadd2MapVictimGV(string directive, GlobalConfValue* gvs);

	bool testadd2MapVictimLV(string directive, StructFieldPairs* sfpair);

	bool existInGlobalMap(string directive, StructFieldPairs* sfpair);

	bool existInLocalMap(string directive, StructFieldPairs* sfpair);

	void filterLVRedundency();

	//============= Tainting ===================

	int getArgIndex(InterestNodes**, InterestNodes*, int);

	//============= Printers ===================

	void dumpLVs();

	string dumpMark(string s);

	void dumpAllConstraints();

	void dumpAllConstraints2File(string s);

	void printGVMap(int orgOrVic);

	void printLVMap(int orgOrVic);

	void printAffectingMap();

	void printFunc2ConfUsageMap(map<Function*, set<string>* >* usageMap);

	void print(std::ostream &O, const Module *M) const {
		O << "in Inference pass with Module \n";
	}

	//============= Testers ===================
	void TestSFP();

	void TestGVLVMaps();

	void TestStrips();

	void PrintSysPars();

	void PrintFunctionList2File();
	void PrintStructGVList2File();
	void PrintGVName2File();

	void keyValueReverse(map<string, set<string>* >* input, map<string, set<string>* >* output);

private:
	bool DEFINE_CPP;

	Config _conf;

	Module *_module;

	FunctionCache _ufcache; //usage based
	FunctionCache _cfcache; //control based

	set<string> syscallcache;

	set<string> _strcopycache;

	StructCache _scons;

	Value* _valueBefore2Steps; //used in traceDataFlow(), to know what is the Value* before 2 steps

	//To record the current directive we are tracking
	string _curDirectiveStr;

	ConfnameGvarPairs _lcngv;

	ParamAffectingMap _funcAMap;

	MyMetaDataInfo* _metaDataInfo;

	//for global ones
	map<string, GlobalConfValues* > _mapGV;
	map<string, GlobalConfValues* > _mapVictimGV; //to store new discovered mappings

	//for local variables
	map<string, LocalConfValues* > _mapLVStructField;
	map<string, LocalConfValues* > _mapVictimLVStructField; //to store new discovered mappings

	list<StructFieldPairs*> _listPredefinedStructField;

	list<string> nonInterestedStructs;
	list<string> nonInterestedGvars;

	MapDirectiveConstraints _mapDirectiveConstraints; //mapping the directive and all its constraints;

	// this map tells that for each function, which global variable are used inside,
	// of course, here "global variable" refers to those related to configuration directives
//	map<Function*, set<GlobalVariable*>* > _mapFunc2GV;

//	MapFunc2CVD _mapFunc2CVD;
	Func2ConfUsageMap _func2ConfUsageMap;

	MiniCallGraph* _ccg;

//	map<unsigned, ControlDepInfo*> _cdep;
	ControlDepAggregator* _controlDepAggr;


	map<BasicBlock*, Constraints*> _incubationBB; //this tells all the BB that incubates process or threads

	ConfUsageInfo _confUsageInfo;

	//this really sucks, only used for C++ for mangled names, i.e., only used when the DEFINE_CPP flag is enabled
	//this map is initialized in the reading of the PInference.conf configuration file
	//map<string, string>* demangledGlobalMap;

	/* this is used in trackDataFlow()
	 * basically, when we meet a call instruction (CallInst), there are two conditions
	 *
	 *  1. we should dive into the call instruction and extract the specifications
	 *  2. we comes from the call instruction, so what we want to this check the return value and go further.
	 *
	 * The first case is very common. So I don't want to explain. Let me explain the second one.
	 * For example, in Data ONTAP,
	 *
	 *  after some stage, we know the mapping from the configuration directive to a global variable
	 *  wafl.default_qtree_mode --> wafl_default_qtree_mode
	 *
	 *  tracing the usage of wafl_default_qtree_mode, we find the following get function
	 *
	 *   103  uint32_t
 	 *	 104  wafl_get_default_qtree_mode(void)
 	 *   105  {
     *   106         return wafl_default_qtree_mode[sk_gethost()];
 	 *	 107  }
	 *
	 *  On the other hand, this function is called at:
	 *   228  mode = wafl_get_default_qtree_mode();
	 *
	 *  So what we want to do is further trace the variable mode.
	 *  in this case, when we meet the return instruction, we send back the function itself,
	 *  and then set the isFromReturn to be true. So, the behavior is to further trace instead of dive into
	 */
	bool isFromReturn;

	bool isFromMiddle;

	//this value tells in which level the specifcations are extracted
	//1: function mapping level;
	//2: global level
	//3: original local structure level
	//4. victim local structure level
	//the larger the number is, the low accuracy and confidence we have for the specification
	int level;
};
}

char Misconfig::ID = 0;
static RegisterPass<Misconfig> X("PInfer", "Configuration Specification Inference Pass", false, false);

#endif
