/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#include "Utils.h"

bool listStringEqual(list<string> a, list<string> b) {

	if(a.size() != b.size()) return false;

	list<string>::iterator ii = a.begin(), ie = a.end();
	list<string>::iterator ji = b.begin(), je = b.end();

	for(; ii != ie && ji != je; ii++, ji++) {
		if((*ii).compare(*ji) != 0) {
			return false;
		}
	}
	return true;
}

string getDemangledName(string orgName) {
	string args = "/usr/bin/c++filt " + orgName + " >" + DEMANGLED_NAME;
	system(args.c_str());

	ifstream infile(DEMANGLED_NAME);
	string line;
	string demangledName;

	if(getline(infile, line)) {
		istringstream iss(line);
		iss >> demangledName;
		return demangledName;

	} else {
		errs() << "[error] Demangled name is wrong!!\n";
		assert(false);
	}

	return NULL;
}

string int2string(int64_t number) {
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

string idx2string(int64_t idx) {
	if(idx == UINT_MAX) {
		return "*";
	} else {
		return int2string(idx);
	}
}

void prettyTimePrint(time_t second) {
//	if(second > 24*3600) {
//		errs() << second/(3600*24) << " day\n";
//
//	} else if(second > 3600) {
//		errs() << second/3600 << " hour\n";
//
//	} else if(second > 60) {
//		errs() << second/60 << " minute\n";
//
//	} else {
//		errs() << second << " second\n";
//	}

	time_t hour = second/3600;
	time_t res = second%3600;
	time_t minute = res/60;
	res = res%60;

	errs() << hour << " hour, " << minute << " min, " << res << " sec.\n";
}

bool keywordMatch(string structname, list<string>* keywords) {
//	string alphabet[]  = {"msg", "wafl_inode", "runtime", "buf", "anon", "wafl_priv", "mystr", "session"};

	std::transform(structname.begin(), structname.end(), structname.begin(), ::tolower);

	list<string>::iterator iter = keywords->begin(), end = keywords->end();
	for(; iter != end; iter++) {
		if(structname.find(*iter) != string::npos) {
			return true;
		}
	}

	return false;
}

//void ListTesting() {
//	list<int> first, second;
//
//	first.push_back (10);
//	first.push_back (7);
//	first.push_back (6);
//	first.push_back (9);
//	first.push_back (72);
//	first.push_back (88);
//
//	second.push_back (34);
//	second.push_back (17);
//	second.push_back (6);
//	second.push_back (0);
//	second.push_back (834);
//	second.push_back (9);
//
//	uniqueMergeList(&first, &second);
//	printList(&first);
//
//}
