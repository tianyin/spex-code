app=$1
START=$(date +%s)
cd /home/tianyin/netapp/ptest-infra/llvm-3.0.src/lib/Transforms/PInfer/
cp conffiles/PInference.conf.$app PInference.conf
opt -load ../../../Debug+Asserts/lib/LLVMMisconfig.so -PInfer bcfiles/$app.bc 2>tmpfiles/$app.tmp
cmp $app.cons consfiles/$app.cons
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "It took $DIFF seconds"
