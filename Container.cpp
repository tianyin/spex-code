/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the container structures and store the configurations   */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#include "Container.h"

//back trace the related getElementPtrInst
bool isFromStruct(Value* v, StructFieldPairs* sfpair) {

	if(isa<GetElementPtrInst>(v)) {
		GetElementPtrInst* geptr = dyn_cast<GetElementPtrInst>(v);

		if(geptr->getNumOperands() == 2) {
			Value* structValue = geptr->getOperand(0);
			return isFromStruct(structValue, sfpair);

		} else if(geptr->getNumOperands() == 3) {
			Value* structValue = geptr->getOperand(0);

			//if it is the aggregation type like char tbuf[HEXMAX+1];
			//%arraydecay = getelementptr inbounds [17 x i8]* %tbuf, i32 0, i32 0, !dbg !408848
			if(isStructType(structValue) == false) {
				return isFromStruct(structValue, sfpair);
			}

			Value* field_index = geptr->getOperand(2);

			if(isa<ConstantInt>(field_index)) {
				ConstantInt* cint = dyn_cast<ConstantInt>(field_index);

				string sName = getStructName(structValue);
				int64_t iIndex = cint->getSExtValue();

				sfpair->update(sName, iIndex);

				//TODO!!! we first do one step (there can be many steps ahead).
				//isFromStruct(structValue)
				return true;
			}
		}

	} else if(isa<LoadInst>(v)) {
		LoadInst* loadinst = dyn_cast<LoadInst>(v);
		Value* src = loadinst->getOperand(0);
		return isFromStruct(src, sfpair);
	}

	return false;
}

/*
 * Check if the <struct, field> pair is of interest
 * Return 1: both the struct and the field match! Perfect match
 *        2: the struct is found, but the field cannot match -> that means definitely we don't care this one
 *        3: both the struct and the field cannot match
 */
int checkStructField(string structName, int field, list<StructFieldPairs*>* m) {
//	StructFieldPairs tmp(structName, field);
	list<StructFieldPairs*>::iterator iter = m->begin(), e = m->end();

	int ret = 3;
	for(; iter != e; iter++) {
		int r = (*iter)->exist(structName, field);
		if( r == 1) {
			return 1;
		} else if( r == 2) {
			ret = 2;
		}
	}
//	errs() << "ret = " << ret << "\n";
	return ret;
}

//Find if the <struct, field> pair is in the localVariable map
map<string, LocalConfValues* >::iterator findStructField(string structName,
		unsigned field, map< string, LocalConfValues* >* m) {

	map<string, LocalConfValues* >::iterator iter = m->begin(), e = m->end();

	for (; iter != e; iter++) {
		LocalConfValues* lcv = iter->second;
		set<StructFieldPairs*>* tmpset = lcv->getStructFieldPairs();
		set<StructFieldPairs*>::iterator setIter = tmpset->begin(), setEnd = tmpset->end();
		for(; setIter != setEnd; setIter++) {

			if ((*setIter)->exist(structName, field) == 1) {
				return iter;
			}
		}
	}
	return e;
}

//bool getStructNField(GetElementPtrInst* geptr, StructFieldPairs* sfpair) {
//
//	Value* structValue = geptr->getOperand(0);
//	Value* field_index = geptr->getOperand(2);
//
//	if(isa<ConstantInt>(field_index)) {
//		ConstantInt* cint = dyn_cast<ConstantInt>(field_index);
//
//		string sName = getStructName(structValue);
//		int64_t iIndex = cint->getSExtValue();
//
//		sfpair->update(sName, iIndex);
//
//		if(isa<GetElementPtrInst>(structValue)) {
//			GetElementPtrInst* src = dyn_cast<GetElementPtrInst>(structValue);
//			getStructNField(src, sfpair);
//		}
//
//		return true;
//
//	}
//
//	return false;
//}

////////////////////////////////////////////////////////////////////////////////
////////////////////   The Kingdom of StructFieldPairs   ///////////////////////
////////////////////////////////////////////////////////////////////////////////

/**
 * Return Value:
 * 1 - find a match!!! good!!! exist!!
 * 2 - the struct name matches but field is not correct.
 * 3 - nothing found!
 */
int StructFieldPairs::exist(string st, unsigned field) {

	assert(_structs.size() == _fields.size());

	bool smbnfm = false; //struct name matches but not field matches

	list<string>::iterator stIter = _structs.begin(), stEnd = _structs.end();
	list<unsigned>::iterator fIter = _fields.begin(), fEnd = _fields.end();

	for(; (stIter != stEnd) && (fIter != fEnd); stIter++, fIter++) {
//			errs() << "*stIter: " << *stIter << "+" << "*fIter:" << *fIter << " <-> " << st << "+" << field;

		if(stIter->compare(st) == 0) {
			if(*fIter == field) {
				return 1;
			} else {
				smbnfm = true;
			}
		}
	}

	if(smbnfm) {
		return 2;
	} else {
		return 3;
	}
}
/**
 * **Assumption**
 * We already know that sfpair has more detailed information!
 * So what to do is to update the current information to be
 * the same as the input one
 *
 */
void StructFieldPairs::update(StructFieldPairs* sfpair) {
	sfpair->printStructs();
	sfpair->printFields();

	errs() << "vs!\n";

	printStructs();
	printFields();

	assert(_structs.size() == _fields.size());
	assert(sfpair->getStructs()->size() == sfpair->getFields()->size());

	//verify the assumption a little bit
	assert(sfpair->getStructs()->size() >= _structs.size());

	_structs.clear();
	_fields.clear();

	const list<string>* sts = sfpair->getStructs();
	const list<unsigned>* fds = sfpair->getFields();

	list<string>::const_iterator stInputIter = sts->begin(), stInputEnd = sts->end();
	list<unsigned>::const_iterator fInputIter = fds->begin(), fInputEnd = fds->end();

	for(; (stInputIter != stInputEnd) && (fInputIter != fInputEnd);
				stInputIter++, fInputIter++) {

		_structs.push_back(*stInputIter);
		_fields.push_back(*fInputIter);
	}

//	list<string>::iterator stIter = _structs.begin(), stEnd = _structs.end();
//	list<unsigned>::iterator fIter = _fields.begin(), fEnd = _fields.end();
//
//	const list<string>* sts = sfpair->getStructs();
//	const list<unsigned>* fds = sfpair->getFields();
//	list<string>::const_iterator stInputIter = sts->begin(), stInputEnd = sts->end();
//	list<unsigned>::const_iterator fInputIter = fds->begin(), fInputEnd = fds->end();
//
//	//1. skip the current ones
//	for(; (stIter != stEnd) && (fIter != fEnd); stIter++, fIter++) {
//		stInputIter++;
//		fInputIter++;
//	}
//
//	//2. pushback the left ones in input sfp
//	for(; (stInputIter != stInputEnd) && (fInputIter != fInputEnd);
//			stInputIter++, fInputIter++) {
//		_structs.push_back(*stInputIter);
//		_fields.push_back(*fInputIter);
//	}
}

/**
 * This function is used to check whether there is any position match
 * e.g.,
 *
 * the current sfp could be
 * <A, 1>, <B, 2>, <C, 3>, <D, 4>
 *
 * an any match could be as follows:
 * <C, 3>, <D, 4>
 *
 * Here we have the **assumption** --
 * the input sfpair's length is no longer than the current one
 */
bool StructFieldPairs::anywhereMatch(StructFieldPairs* sfpair) {
	assert(_structs.size() == _fields.size());
	assert(sfpair->getStructs()->size() == sfpair->getFields()->size());

	//verify the assumption
	assert(_structs.size() >=  sfpair->getStructs()->size());

	list<string>::iterator stIter = _structs.begin(), stEnd = _structs.end();
	list<unsigned>::iterator fIter = _fields.begin(), fEnd = _fields.end();

	const list<string>* sts = sfpair->getStructs();
	const list<unsigned>* fds = sfpair->getFields();
	list<string>::const_iterator stInputIter = sts->begin(), stInputEnd = sts->end();
	list<unsigned>::const_iterator fInputIter = fds->begin(), fInputEnd = fds->end();

	for(; (stIter != stEnd) && (fIter != fEnd); stIter++, fIter++) {
		if(stIter->compare(*stInputIter) == 0) {
			if(*fIter == *fInputIter) {
				stInputIter++;
				fInputIter++;

				if( (stInputIter == stInputEnd) && (fInputIter == fInputEnd) ) {
					return true;
				}
			}
		}
	}

	return false;
}

//bool StructFieldPairs::suffixMatch(StructFieldPairs* sfpair) {
//}

/**
 * This function is used to check whether the input sfpair is a prefix of
 * the current sfp. It's different from any match
 * e.g.,
 *
 * the current sfp could be
 * <A, 1>, <B, 2>, <C, 3>, <D, 4>
 *
 * a prefix match could only start with <A, 1>!
 *
 * Here we have the **assumption** --
 * the input sfpair's length is no longer than the current one
 */
bool StructFieldPairs::prefixMatch(StructFieldPairs* sfpair) {
	assert(_structs.size() == _fields.size());
	assert(sfpair->getStructs()->size() == sfpair->getFields()->size());

	//verify the assumption
	assert(_structs.size() >=  sfpair->getStructs()->size());

	list<string>::iterator stIter = _structs.begin(), stEnd = _structs.end();
	list<unsigned>::iterator fIter = _fields.begin(), fEnd = _fields.end();

	const list<string>* sts = sfpair->getStructs();
	const list<unsigned>* fds = sfpair->getFields();
	list<string>::const_iterator stInputIter = sts->begin(), stInputEnd = sts->end();
	list<unsigned>::const_iterator fInputIter = fds->begin(), fInputEnd = fds->end();

	for( ;
		(stIter != stEnd) && (fIter != fEnd) &&
		(stInputIter != stInputEnd) && (fInputIter != fInputEnd)
		 ;
		stIter++, fIter++,
		stInputIter++, fInputIter++
	) {

//		if(stIter->compare(*stInputIter) != 0 && *fIter != *fInputIter) {
		if(!SFMatch(*stIter, *fIter, *stInputIter, *fInputIter)) {
			return false;
		}
	}

	return true;
}

void StructFieldPairs::test() {
	errs() << "===========================start SFP Testing==========================\n";

	StructFieldPairs sfp1; // [<A, 1>, <B, 2>, <C, 3>, <D, 4>]
	list<string>* st1 = sfp1.getStructs();
	list<unsigned>* sf1 = sfp1.getFields();
	st1->push_back("A");
	st1->push_back("B");
	st1->push_back("C");
	st1->push_back("D");
	sf1->push_back(1);
	sf1->push_back(2);
	sf1->push_back(3);
	sf1->push_back(4);

	StructFieldPairs sfp2; // [<A, 1>, <B, 2>]
	list<string>* st2 = sfp2.getStructs();
	list<unsigned>* sf2 = sfp2.getFields();
	st2->push_back("A");
	st2->push_back("B");
	sf2->push_back(1);
	sf2->push_back(2);

	StructFieldPairs sfp3; // [<B,2>, <C,3>]
	list<string>* st3 = sfp3.getStructs();
	list<unsigned>* sf3 = sfp3.getFields();
	st3->push_back("B");
	st3->push_back("C");
	sf3->push_back(2);
	sf3->push_back(3);

	assert(sfp1.prefixMatch(&sfp2));
	assert(sfp1.anywhereMatch(&sfp2));
	assert(sfp1.anywhereMatch(&sfp3));
	assert(!sfp1.prefixMatch(&sfp3));
	assert(!sfp2.prefixMatch(&sfp3));
	assert(!sfp2.anywhereMatch(&sfp3));

	sfp2.update(&sfp1);
	assert(sfp1.operator ==(sfp2));
	assert(sfp1.prefixMatch(&sfp2));
	assert(sfp1.anywhereMatch(&sfp2));

	//----------------------------

	StructFieldPairs sfp4; // [<A, 1>, <B, 2>, <C, 4>]
	list<string>* st4 = sfp4.getStructs();
	list<unsigned>* sf4 = sfp4.getFields();
	st4->push_back("A");
	st4->push_back("B");
	st4->push_back("C");
	sf4->push_back(1);
	sf4->push_back(2);
	sf4->push_back(4);

	assert(!sfp1.prefixMatch(&sfp4));

	StructFieldPairs sfp5; // [<A, 1>, <*, 2>, <C, *>]
	list<string>* st5 = sfp5.getStructs();
	list<unsigned>* sf5 = sfp5.getFields();
	st5->push_back("A");
	st5->push_back("*");
	st5->push_back("C");
	sf5->push_back(1);
	sf5->push_back(2);
	sf5->push_back(UINT_MAX);

	assert(sfp1.prefixMatch(&sfp5));

	errs() << "[info] Congratulations!!! You passed the SFP checking! Go ahread!!\n";
}

void StructFieldPairs::print() {
	assert(_structs.size() == _fields.size());

	list<string>::iterator stIter = _structs.begin(), stEnd = _structs.end();
	list<unsigned>::iterator fIter = _fields.begin(), fEnd = _fields.end();

	for(; (stIter != stEnd) && (fIter != fEnd); stIter++, fIter++) {
		errs() << "       < " << *stIter << ", " << idx2string(*fIter) << " > ";
	}
	errs() << "\n";
}

void StructFieldPairs::printStructs() {
	list<string>::iterator stIter = _structs.begin(), stEnd = _structs.end();

	for(; stIter != stEnd; stIter++) {
			errs() << *stIter << " | ";
	}
	errs() << "\n";
}

void StructFieldPairs::printFields() {
	list<unsigned>::iterator fIter = _fields.begin(), fEnd = _fields.end();

	for(; fIter != fEnd; fIter++) {
		errs() << idx2string(*fIter) << " | ";
	}
	errs() << "\n";
}

////////////////////////////////////////////////////////////////////////////////
////////////////////   The Kingdom of ConfContainer   //////////////////////////
////////////////////////////////////////////////////////////////////////////////

void ConfContainer::print() {
	if(type == NO_INTEREST) {
		errs() << "[info] NO_INTEREST configuration container.\n";

	} else if(type == LOCAL_BASIC_VAR) {
		errs() << "[info] LOCAL_BASIC_VAR configuration container.\n";
//		errs() << "       " << *lastLocalVar << "\n";

	} else if(type == GLOBAL_BASIC_VAR) {
		errs() << "[info] GLOBAL_BASIC_VAR -- " << getGlobalValueStr() << "\n";

	} else if(type == GLOBAL_STRUCTURE_VAR) {
		errs() << "[info] GLOBAL_STRUCT_VAR -- " << getGlobalValueStr() << "\n";

		list<string>::iterator varIter = varNames.begin(), varEnd = varNames.end();
		list<unsigned>::iterator idxIter = varIndices.begin(), idxEnd = varIndices.end();

		//the first one is the global variable name!
		varIter++;
		idxIter++;

		for(; varIter != varEnd && idxIter != idxEnd; varIter++, idxIter++) {
			errs() << "       (" << *varIter << ", " << idx2string(*idxIter) << ")\n";
		}

	} else if(type == LOCAL_STRUCTURE_VAR) {
		errs() << "[info] LOCAL_STRUCTURE_VAR -- \n";

		list<string>::iterator varIter = varNames.begin(), varEnd = varNames.end();
		list<unsigned>::iterator idxIter = varIndices.begin(), idxEnd = varIndices.end();

		for(; varIter != varEnd && idxIter != idxEnd; varIter++, idxIter++) {
			errs() << "       (" << *varIter << ", " << idx2string(*idxIter) << ")\n";
		}

	} else {
		errs() << "[error] the type is wrong!!!\n";
	}
}

//this is a recursive function
//the termination condition is either we meet a global variable,
//or the back-tracing is ended with a non-GetElementPtrInst
//
//note here, this process does not cross the function boundary (I don't want to do this!!!)
//
//as long as there's one checked v along the path, we are not interested
//because it will be checked in trackDataFlow
bool ConfContainer::getTMPConfContainer(Value* v, ValueCache* vCache) {
	if(vCache->exist(v)) {
		return false;
	}

	if(isa<GlobalValue>(v)) {
		gv = dyn_cast<GlobalValue>(v);
		add2front(gv->getNameStr(), UINT_MAX);
		return true;

	} else if(isa<ConstantExpr>(v)) {
		ConstantExpr* cexpr = dyn_cast<ConstantExpr>(v);

		if(cexpr->isCast()) {
			return getTMPConfContainer(cexpr->getOperand(0), vCache);

		} else if(cexpr->isDereferenceablePointer()) {
			errs() << "88888888888888888888 ConstantExpr getTMPConfContainer\n";

			if(!isa<GlobalValue>(cexpr->getOperand(0))) {
				errs() << *(cexpr->getOperand(0)) << "\n";
			}

			assert(isa<GlobalValue>(cexpr->getOperand(0)));
			gv = dyn_cast<GlobalValue>(cexpr->getOperand(0));

////////////////////////////////////////////////////////////////////////////////////

			int numOfArg = cexpr->getNumOperands();

			for(int i=numOfArg-1; i>=2; i--) {
				Value* ix = cexpr->getOperand(i);
				assert(isa<ConstantInt>(ix));
				add2front("*", dyn_cast<ConstantInt>(ix)->getZExtValue());
			}

			//if it has type!~
			if(isStructType(cexpr->getOperand(0))) {
				varNames.pop_front();
				varNames.push_front(getStructName(cexpr->getOperand(0)));
			}
////////////////////////////////////////////////////////////////////////////////////

			add2front(gv->getNameStr(), UINT_MAX);
			return true;

		} else {
			return false;
		}

	} else if(isa<GetElementPtrInst>(v)) {

		//one example
		/**
		 57386   %arrayidx8 = getelementptr inbounds [2 x [2 x %struct.dirlist]]* @tftpd_dirs, i32 0, i64 %idxprom7, !dbg !57377
		 57387   %arrayidx9 = getelementptr inbounds [2 x %struct.dirlist]* %arrayidx8, i32 0, i64 0, !dbg !57377
		 57388   %name10 = getelementptr inbounds %struct.dirlist* %arrayidx9, i32 0, i32 0, !dbg !57377
		 57389   %arraydecay11 = getelementptr inbounds [513 x i8]* %name10, i32 0, i32 0, !dbg !57377
		 57390   %6 = load i8** %rootdir.addr, align 8, !dbg !57377
		 57391   %call12 = call i8* @strcpy(i8* %arraydecay11, i8* %6) noredzone, !dbg !57377
		 **/
		//the input value is %arraydecay11
		GetElementPtrInst* geptr = dyn_cast<GetElementPtrInst>(v);

		if(geptr->getNumOperands() == 3) {
			string stname = "*";
			unsigned index = UINT_MAX;

			Value* src = geptr->getOperand(0);
			if(isStructType(src)) {
				string structName = getStructName(src);

				//oh my gosh, anticipate case like that (no name struct)
				//%2 = getelementptr { i64, i64 }* %0, i32 0, i32 1
				if(structName.compare("") != 0) {
					stname = structName;
				}
			}

			Value* idx = geptr->getOperand(2);
			if(isa<ConstantInt>(idx)) {
				index = dyn_cast<ConstantInt>(idx)->getSExtValue();
			}

			add2front(stname, index);

			return getTMPConfContainer(src, vCache);

		} else if(geptr->getNumOperands() == 2) {
			return getTMPConfContainer(geptr->getOperand(0), vCache);

		} else if(geptr->getNumOperands() > 3) {
			errs() << "[error] TODO in getConfContainer, the GetElementPtrInst has more than 3 operands\n";
			//TODO
			lastLocalVar = v;
		}

	} else if(isa<CallInst>(v)) {
		/*
		 * [NOTE] it may also from a function call!!!
		 * Example:
		 * 3128   %call40 = call %struct._vfiler* @sk_get_vfiler() noredzone, !dbg !9174
		 * 3129   %ftpd41 = getelementptr inbounds %struct._vfiler* %call40, i32 0, i32 38, !dbg !9174
		 * 3130   %27 = load %struct.ftpd_vars** %ftpd41, align 8, !dbg !9174
		 * 3131   %ftpLogSizeLimit = getelementptr inbounds %struct.ftpd_vars* %27, i32 0, i32 22, !dbg !9174
		 * 3132   store i64 %26, i64* %ftpLogSizeLimit, align 8, !dbg !9174
		 *
		 * [Heuristic]
		 * We only care the function with 0 argument. Otherwise it's toooooo difficult to decide
		 */
		CallInst* callinst = dyn_cast<CallInst>(v);
		if(callinst->getNumArgOperands() == 0) {
			gv = callinst->getCalledFunction();
			add2front(gv->getNameStr(), UINT_MAX);
			return true;
		}

	} else if(isa<LoadInst>(v)) {
		LoadInst* li = dyn_cast<LoadInst>(v);
		return getTMPConfContainer(li->getOperand(0), vCache);

	} else if(isa<CastInst>(v)) {
		CastInst* ci = dyn_cast<CastInst>(v);
		return getTMPConfContainer(ci->getOperand(0), vCache);

	} else {//(isa<AllocaInst>(v)) {
		if(isa<Constant>(v)) {
			return false;
		}

		lastLocalVar = v;

		if(!isa<AllocaInst>(v)) {
			errs() << "[warn] the last back trace local variable is not an AllocaInst -- \n";
//			errs() << "       " << *v << "\n";
		}

		if(!isa<Instruction>(v)) {
			errs() << "[panic] omg, this should be the source of the segfault" << *lastLocalVar << "\n";
		}

		return true;
	}

	return true;
}

void ConfContainer::getContainer(Value* v, ValueCache* vCache) {
	bool isChecked = getTMPConfContainer(v, vCache);

	if(!isChecked) {
		type = NO_INTEREST;
		return;
	}

	//then we have the values ready in varNames, varIndice, and isGlobal
	//here we have to decide
	trim();

	assert(varNames.size() == varIndices.size());
	if(varNames.size() == 0) {
		type = LOCAL_BASIC_VAR;
		return;
	}

	if(gv != NULL) {
		if(varNames.size() == 1) {
			type = GLOBAL_BASIC_VAR;

		} else {
			type = GLOBAL_STRUCTURE_VAR;
		}
	} else {
		//the SF pairs could be like
		//    < *, 0 >  < struct.slt_args, 0 >
		//should delete the ones with *
		while(varNames.size() > 0) {
			string vn = varNames.front();
			if(vn.compare("*") == 0) {
				varNames.pop_front();
				varIndices.pop_front();
			} else {
				break;
			}
		}

		if(varNames.size() > 0) {
			type = LOCAL_STRUCTURE_VAR;
		} else {
			type = LOCAL_BASIC_VAR;
		}
	}
}

//delete the (*, UINT_MAX) both from the head and from the tail
void ConfContainer::trim() {
	int count = 0;
	list<string>::reverse_iterator evIter = varNames.rbegin(), evEnd = varNames.rend();
	list<unsigned>::reverse_iterator eiIter = varIndices.rbegin(), eiEnd = varIndices.rend();
	for(; evIter != evEnd && eiIter != eiEnd; evIter++, eiIter++) {
		if((*evIter).compare("*") == 0 && (*eiIter == UINT_MAX)) {
			count ++;
		} else {
			break;
		}
	}

	for(int i=0; i<count; i++) {
		varNames.pop_back();
		varIndices.pop_back();
	}

	if(varNames.size() > 0) {
		list<string>::iterator hvIter = varNames.begin(), hvEnd = varNames.end();
		list<unsigned>::iterator hiIter = varIndices.begin(), hiEnd = varIndices.end();

		count = 0;
		for(; hvIter != hvEnd && hiIter != hiEnd; hvIter++, hiIter++) {
			if((*hvIter).compare("*") == 0 && (*hiIter == UINT_MAX)) {
				count ++;
			} else {
				break;
			}
		}

		for(int i=0; i<count; i++) {
			varNames.pop_front();
			varIndices.pop_front();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////   The Kingdom of ConfUsageInfo   //////////////////////////
////////////////////////////////////////////////////////////////////////////////

void ConfUsageInfo::addConfUsageInfo(string directive, Value* usage, Context* callchain) {
	errs() << "USAGE HIT!\n";

	Context* tmpcc = new Context(callchain);

	if(_confUsageInfo.count(directive) == 0) { //nothing exist!
		list<Context* >* tmpcclist = new list<Context* >();
		tmpcclist->push_back(tmpcc);

		UsageInfo* tmpui = new UsageInfo();
		tmpui->operator [](usage) = tmpcclist;

		_confUsageInfo[directive] = tmpui;

	} else {
		UsageInfo* tmpui = _confUsageInfo[directive];

		if(tmpui->count(usage) == 0) { //the directive exist but the usage do not.
			list<Context* >* tmpcclist = new list<Context* >();
			tmpcclist->push_back(tmpcc);

			tmpui->operator [](usage) = tmpcclist;

		} else { //both directive and usage exist!
			list<Context* >* tmpcclist = tmpui->operator [](usage);
			tmpcclist->push_back(tmpcc);
		}
	}
}

ConfUsageInfo::~ConfUsageInfo() {
	map<string, UsageInfo* >::iterator uIter = _confUsageInfo.begin(), uEnd = _confUsageInfo.end();
	for(; uIter != uEnd; uIter++) {
		UsageInfo* uinfo = uIter->second;

		map<Value*, list<Context* >* >::iterator lcIter = uinfo->begin(), lcEnd = uinfo->end();
		for(; lcIter != lcEnd; lcIter++) {
			list<Context* >* lc = lcIter->second;

			list<Context* >::iterator ccIter = lc->begin(), ccEnd = lc->end();
			for(; ccIter != ccEnd; ccIter++) {
				Context* cc = *ccIter;
				cc->clear();
				delete cc;
			}

			lc->clear();
			delete lc;
		}

		uinfo->clear();
		delete uinfo;

		_confUsageInfo.clear();
	}
}

/**
 * find which config parameter is associated with the input Value* v
 * ========== param =============
 *  Input:  v
 *  Output: cpset
 * ==============================
 */
bool ConfUsageInfo::getConfParamSet(Value* v, set<string>* cpset) {
	bool ret = false;

	map<string, UsageInfo* >::iterator uIter = _confUsageInfo.begin(), uEnd = _confUsageInfo.end();
	for(; uIter != uEnd; uIter++) {
		string paramName = uIter->first;
		UsageInfo* uinfo = uIter->second;

		if(uinfo->count(v) != 0) {
			cpset->insert(paramName);
			ret = true;
		}
	}

	return ret;
}

void ConfUsageInfo::getUsageSet(string par, set<Value*>* usages) {
	usages->clear();

	UsageInfo* usageInfo = this->_confUsageInfo[par];
	make_key_set(*usageInfo, *usages);
}

void ConfUsageInfo::print() {
	errs() << "------------------------ConfUsageInfo::print()-------------------------\n";

	map<string, UsageInfo* >::iterator uIter = _confUsageInfo.begin(), uEnd = _confUsageInfo.end();
	for(; uIter != uEnd; uIter++) {
		string paramName = uIter->first;
		UsageInfo* uinfo = uIter->second;

		errs() << "====================================================\n";
		errs() << paramName << "\n";

		map<Value*, list<Context* >* >::iterator lcIter = uinfo->begin(), lcEnd = uinfo->end();
		for(; lcIter != lcEnd; lcIter++) {
			Value* v = lcIter->first;

			errs() << "----------------------------------------------------\n";
//			errs() << *v << "\n";

			list<Context* >* lc = lcIter->second;

//			errs() << "+++" << *v << "\n";
//			errs() << "----------------------------------------------------\n";

			if(lc->size() > 0) {
				list<Context* >::iterator ccIter = lc->begin(), ccEnd = lc->end();
				for(; ccIter != ccEnd; ccIter++) {
					Context* cc = *ccIter;
					if(cc->size() > 0) {
						cc->print();
					}
				}
			}
		}
	}
}

void ConfUsageInfo::statistics() {
	errs() << "------------------------ConfUsageInfo::statistics()-------------------------\n";

	int numInst = 0;
	int numGVar = 0;
	int numConst = 0;
	int numArgmt = 0;
	int numOther = 0;

	map<string, UsageInfo* >::iterator uIter = _confUsageInfo.begin(), uEnd = _confUsageInfo.end();
	for(; uIter != uEnd; uIter++) {
		string paramName = uIter->first;
		UsageInfo* uinfo = uIter->second;

		map<Value*, list<Context* >* >::iterator lcIter = uinfo->begin(), lcEnd = uinfo->end();
		for(; lcIter != lcEnd; lcIter++) {
			Value* v = lcIter->first;

			if(isa<Instruction>(v)) {
				numInst ++;
			} else if(isa<GlobalValue>(v)) {
				numGVar ++;
			} else if(isa<ConstantExpr>(v)) {
				numConst ++;
			} else if(isa<Argument>(v)) {
				numArgmt ++;
			} else {
				numOther ++;
				errs() << *v << "\n";
			}
		}
	}

	errs() << "numInst  = " << numInst << "\n"
		   << "numGVar  = " << numGVar << "\n"
		   << "numConst = " << numConst << "\n"
		   << "numArgmt = " << numArgmt << "\n"
		   << "numOther = " << numOther << "\n";
}

void ConfUsageInfo::getExistingFunction2ParamMapping(Func2ConfUsageMap* f2pmap) {

	map<string, UsageInfo* >::iterator uIter = _confUsageInfo.begin(), uEnd = _confUsageInfo.end();
	for(; uIter != uEnd; uIter++) {
		string paramName = uIter->first;
		UsageInfo* uinfo = uIter->second;

		map<Value*, list<Context* >* >::iterator lcIter = uinfo->begin(), lcEnd = uinfo->end();
		for(; lcIter != lcEnd; lcIter++) {
			Value* v = lcIter->first;

			set<Instruction*> instUsageSet;
			if(isa<Instruction>(v)) {
				Instruction* inst = dyn_cast<Instruction>(v);
				instUsageSet.insert(inst);

			} else if(isa<GlobalValue>(v) || isa<ConstantExpr>(v)) {
				//track all the usage
//				GlobalValue* gv = dyn_cast<GlobalValue>(v);
//				errs() << "-- " << gv->getNameStr() << "\n";

				set<Instruction*> iset;
				get1stStopInstructionUsage(v, &iset);

				instUsageSet.insert(iset.begin(), iset.end());

			} else if(isa<Argument>(v)) {
				//
			} else {
				errs() << *v << "\n";
				assert(false);
			}

			set<Instruction*>::iterator isIter = instUsageSet.begin(), isEnd = instUsageSet.end();
			for(; isIter != isEnd; isIter++) {
				Instruction* tmpInst = *isIter;
				Function* func = tmpInst->getParent()->getParent();

//				if(has1RealCallInstUsage(func)) {
//					string funcName = stripStructNamePrefix(func->getNameStr());

					if(f2pmap->count(func) == 0) {
						f2pmap->operator [](func) = new set<string>();
					}
					f2pmap->operator [](func)->insert(paramName);
//				}
			}
		}
	}
}

void ConfUsageInfo::get1stStopInstructionUsage(Value* v, set<Instruction*>* iset) {
//	assert(isa<GlobalVariable>(v) || isa<ConstantExpr>(v));

	Value::use_iterator u = v->use_begin(), end = v->use_end();
	for (; u != end; u++) {

		if(isa<Instruction>(*u)) {
			Instruction* inst = dyn_cast<Instruction>(*u);
			iset->insert(inst);

		} else if(isa<GlobalValue>(*u) || isa<ConstantExpr>(*u)) {
			get1stStopInstructionUsage(*u, iset);

		} else if(isa<Constant>(*u)) {
//			errs() << **u << "\n";

		} else {
			errs() << **u << "\n";
			assert(false);
		}
	}
}

//void Misconfig::getFunc2CVMapping() {
////	string gvarName;
////	string gdirective;
////
////	list<ConfnameGvarPair*>::iterator iter = _lcngv.begin(), end = _lcngv.end();
////	//to get the function to global variables mapping
////	for (; iter != end; iter++) {
////
////		gvarName = (*iter)->gvar;
////		gdirective = (*iter)->confName;
////
////		StringRef srGvarName(gvarName);
////		getFunc2GVMapping(srGvarName, gdirective);
////	}
////
////	CallGraph cg;
////	cg.initialize(*_module);
////	CallGraph::iterator cgIter = cg.begin(), cgEnd = cg.end();
////	for(; cgIter != cgEnd; cgIter++) {
////		errs() << (*cgIter).first->getNameStr() << "\n";
////	}
//
//	Module::iterator modIter = _module->begin(), modEnd = _module->end();
//	set<Function*> doneCache;
//	int i = 0;
//	for (; modIter != modEnd; modIter++) {
//		Function* f = &(*modIter);
//		getOneFunc2CVMapping(f, &doneCache);
//		i++;
//	}
//}

//bool Misconfig::getOneFunc2CVMapping(Function* f, set<Function*>* doneCache) {
//	//omg, some function pointer might cause instructions like
//	//        call void %2(), !dbg !1750
//	// the code is:
//	//
//	// exitfunc_t curr_func = s_exit_func;
//	// /* Prevent recursion */
//	// s_exit_func = 0;
//	// (*curr_func)();
//	if (!f) {
//		return false;
//	}
//
//	if (f->isIntrinsic()) {
//		return false;
//	}
//
//	if (doneCache->find(f) != doneCache->end()) {
//		return true;
//	}
//
//	inst_iterator funcIter = inst_begin(f), funcEnd = inst_end(f);
//	if (funcIter == funcEnd) { // this means it's either a system call or library call!
//		errs() << "-----get sys/lib call----- : " << f->getNameStr() << "\n";
//		return false;
//	} else {
//		errs() << "checking func " << f->getNameStr() << "\n";
//	}
//
//	//ok, now we are sure that it's a normal function (neither system call nor intrinsic)
//	doneCache->insert(f);
//
//	//initialize a set to store global variables
////	if(_mapFunc2CVD.find(f) == _mapFunc2CVD.end()) {
//////		set<string>* gs = new set<string>();
//////		_mapFunc2CVD[f] = gs;
////
////		map<string, set<int>*>* cvd = new map<string, list<int>*>();
////		_mapFunc2CVD[f] = cvd;
////	}
//
//	_mapFunc2CVD.addFunction(f);
//
//	for (; funcIter != funcEnd; funcIter++) {
//		Instruction* inst = &(*funcIter);
//
//		if (isa<LoadInst>(inst)) {
//			Value* v = inst->getOperand(0);
//			string directive;
//
//			if (isConfigurationRelated(v, directive)) {
//				set<Value*> dCache;
//				set<int>* rlist = new set<int>();
//				if (this->gotoBranch(v, rlist, &dCache)) {
////					errs() << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
//					printSet(rlist);
//
////					insert2MapFunc2CVD(f, directive, rlist);
//					_mapFunc2CVD.insert2MapFunc2CVD(f, directive, rlist);
//
//				} else {
////					insert2MapFunc2CVD(f, directive, INT_MAX);
//					_mapFunc2CVD.insert2MapFunc2CVD(f, directive, INT_MAX);
//				}
//			}
//
//		} else if (isa<GetElementPtrInst>(inst)) {
//			GetElementPtrInst* gepi = dyn_cast<GetElementPtrInst>(inst);
//
//			string typeName;
//			unsigned field = UINT_MAX;
//			string directive;
//
//			//errs() << "[info] data flow tracing goes into the GetElementPtrInst!!!\n";
//
//			if (isInterestStructField(gepi, &_mapLVStructField, typeName, field, directive)) {
//
//				set<Value*> dCache;
//				set<int>* rlist = new set<int>();
//				if (this->gotoBranch(gepi, rlist, &dCache)) {
//					printSet(rlist);
//
//					_mapFunc2CVD.insert2MapFunc2CVD(f, directive, rlist);
//
//				} else {
//					_mapFunc2CVD.insert2MapFunc2CVD(f, directive, INT_MAX);
//				}
//
////				(_mapFunc2CVD[f])->insert(directive);
//			}
//
//		} else if (isa<CallInst>(inst)) {
//			CallInst* cInst = dyn_cast<CallInst>(inst);
//
//			if (isa<IntrinsicInst>(cInst)) {
//				continue;
//			}
//
////			errs() << "call inst " << *cInst << "\n";
//
////			Function* callee = cInst->getCalledFunction();
//			Function* callee = getCalledFunction(cInst);
//
////			if(callee && (callee->getNameStr()!=NULL) && callee->getNameStr().compare("fork") == 0) {
////				errs() << "%%%%%%%%%%%%%%%%%%%%%%%hahaha, fork!!!\n";
////			}
//
//			if (getOneFunc2CVMapping(callee, doneCache) == true) {
//				// merge the lists
////				(_mapFunc2CVD[f])->insert((_mapFunc2CVD[callee])->begin(), (_mapFunc2CVD[callee])->end());
//				_mapFunc2CVD.mergeMapFunc2CVD(f, callee);
//			}
//		}
//	}
//
//	return true;
//}

