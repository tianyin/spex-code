/*
 * Common.h
 *
 *  Created on: Aug 30, 2012
 *      Author: tianyin
 */

#ifndef COMMON_H_
#define COMMON_H_

#include "llvm/Support/raw_ostream.h"
#include "llvm/Function.h"
#include "llvm/Instructions.h"
#include "llvm/Support/CFG.h"
#include "llvm/Constants.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Value.h"

#include "Utils.h"

using namespace std;
using namespace llvm;

#define PRINTSRCLOC

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Common Functions
//////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Here we use string instead of Function* as the key because of
 * the multiple-instance problem, i.e., the same function might
 * have multiple instances in the bitcode file
 */
typedef map<Function*, set<string>* > Func2ConfUsageMap;
//typedef map<string, set<string>* > Conf2FuncUsageMap;

bool stripIntrinsic(Function *func, StringRef& strippedName);
//StringRef stripStructType(StringRef name);

string getStructName(Value* v);

string getStructName(Type* tx);

string getStructName(GetElementPtrInst* gepi);

bool isSuccessor(BasicBlock* bb1, BasicBlock* bb2);

list<BasicBlock*>* getSuccessors(BasicBlock* bb);

bool contain(list<BasicBlock*>* lb, BasicBlock* tb);

bool predContain(list<BasicBlock*>* lb, BasicBlock* tb, BasicBlock** hit);

Function* getCalledFunction(CallInst* callInst);

bool isEmptyFunction(Function* f);

bool isStructType(Type* t);

bool isStructType(Value* v);

bool isBasicType(Type* ty);

bool isString2Char2Int(Value* v);

bool isChar2IntCast(Value* v);

bool isFromString(Value* v);

bool isPointer2I8(Type* ty);

bool isPointerPointer2I8(Type* ty);

string getAccurateType(Type* Ty);

bool isNullPointer(Value* v);

string getConstantArray(Value* ca);

Type* getPointedType(Type* t);

bool isSameType(Type* t1, Type* t2);

bool SFMatch(string inst_stname, unsigned inst_idx, string sf_stname, unsigned sf_idx);

string stripTailedDigits(string s);

string stripStructNamePrefix(string);

string stripStructName(string stname);

string stripFunctionNameSuffix(string funcname);

Value* getFunctionArg(Function* func, unsigned idx);

bool has1RealCallInstUsage(Function* F);

int numOfRealCallInstUsage(Function* F);

void getRealCallInstUsage(Function* F, set<CallInst*>* callinstSet);

void get1stStopInstUsageInFunction(Value* v , set<Function*>* fset);

void get1stStopInstUsage(Value* v, set<Instruction*>* iset);

void printInstructionSet(set<Instruction*>* iset);

void printFunctionSet(set<Function*>* functionSet);

//void generateMiniCallGraph(Module* m);

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Common Data Structures
//////////////////////////////////////////////////////////////////////////////////////////////////////

struct Config {
	string globalMappingFile;
	string localMappingFile;
	string outputFile;
	string structFile;
	string application;
	string appSpecFile;
	string funcMappingFile;

	int funcArgIdx;

	Config() {
		funcArgIdx = -1;
	}
};

class ValueCache {
public:
	void insert(Value* v) {
		_cachedValues.insert(v);
	}

	bool exist(Value* v) {
		if(_cachedValues.find(v) == _cachedValues.end()) {
			return false;

		} else {
			return true;
		}
	}

	void clear() {
		_cachedValues.clear();
	}

private:
	set<Value*> _cachedValues;
};

struct Context {

	Context() {
		_doRefCheck = true;
	}

	Context(Context* cc) {
		copyList(&_callchain, &cc->_callchain);
//		_refOps = cc->_refOps;
	}

	void pushBack(Instruction* i) {

		if(i != NULL) {
//			errs() << "call graph put back!!! == " << *i << "\n";
			_callchain.push_back(i);
		}
	}

	size_t size() {
		return _callchain.size();
	}

	void clear() {
		_callchain.clear();
	}

	Value* getLast() {
		return _callchain.back();
	}

	void popLast() {
		_callchain.pop_back();
	}

	void hardPrint() {
		errs() << "---";
		list<Instruction*>::iterator iter = _callchain.begin(), end = _callchain.end();
		for(; iter != end; iter++) {
			errs() << *(*iter) << "->";
		}
		errs() << "---\n";
	}

	void print() {
		errs() << "---";
		list<Instruction*>::iterator iter = _callchain.begin(), end = _callchain.end();
		for(; iter != end; iter++) {
			errs() << (*iter)->getParent()->getParent()->getNameStr() << "->";
		}
		errs() << "---\n";
	}

	list<Instruction*> _callchain;

	bool _doRefCheck;
//	string _refOps;
};

#endif /* COMMON_H_ */
