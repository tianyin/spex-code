/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This is the main analysis part of specification inference            */
/*                                                                      */
/* Author: Jiaqi Zhang                                                  */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#include "MyMetadataInfo.h"

//Initialize the metadata for the global variables from
//llvm.dbg.cu
void MyMetaDataInfo::_initializeMetadata() {
	/*	ValueSymbolTable& vst = this->_module->getValueSymbolTable();
	 MDNode* dbgcuNode = (MDNode*)(vst.lookup("llvm.dbg.cu"));
	 */
	/* llvm.dbg.cu conllects all the metadata nodes of compile unit descriptors
	 * compile unit descriptors provide the root context for objects declared in
	 * a specific compilation unit
	 */
	NamedMDNode* nmd = this->_module->getNamedMetadata("llvm.dbg.cu");
	if (nmd->getNumOperands() == 0) {
		errs() << "CANNOT find llvm.dbg.cu! Failed!\n";
		return;
	}

	int numOps = nmd->getNumOperands();
	for (int i = 0; i < numOps; i++) {
		MDNode* cunitNode = (MDNode*) (nmd->getOperand(i));
		processCompileUnitMDNode(cunitNode);
	}
}

/* DWTag determines the contents in [] after the metadata node
 * For example, DW_TAG_compile_unit. A complete list is in
 * llvm/Support/Dwarf.h
 */
int MyMetaDataInfo::getDWTag(MDNode* node) {
	int tag = -1;
	if (!node) {
		return -1;
	}
	Value* tagV = NULL;
	ConstantInt* cint = NULL;
	tagV = node->getOperand(0);
	if (!tagV) {
		return -1;
	}
	cint = dyn_cast<ConstantInt>(tagV);
	if (cint) {
		int tagComplete = cint->getSExtValue();
		tag = tagComplete - LLVMDebugVersion;
	}
	return tag;
}

//prototype is SlotTracker::processFunction in AsmWriter.cpp
void MyMetaDataInfo::getAllMDNFunc(Function& F) {
	/* Comment previous implementation and use the one in AsmWriter.cpp
	 inst_iterator iter = inst_begin(F), end = inst_end(F);
	 for(;iter!=end;iter++){
	 if(DbgDeclareInst *DDI = dyn_cast<DbgDeclareInst>(*iter)){
	 MDNode* mdnode = DDI->getVariable();
	 }
	 }
	 */
	SmallVector<pair<unsigned, MDNode*>, 4> MDForInst;
	for (Function::iterator BB = F.begin(), E = F.end(); BB != E; ++BB) {
		for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E; ++I) {
			if (isa<CallInst>(I)) {
				CallInst* CI = dyn_cast<CallInst>(I);
				if (Function *F = CI->getCalledFunction()) {
					if (F->getName().startswith("llvm.")) {
						for (unsigned i = 0, e = I->getNumOperands(); i != e; ++i) {
							MDNode *N = dyn_cast_or_null<MDNode>(I->getOperand(i));
							if (N) {
								createMetadataSlot(N);
							}
						}
					}
				}
			}

			I->getAllMetadata(MDForInst);
			for (unsigned i = 0, e = MDForInst.size(); i != e; ++i) {
				createMetadataSlot(MDForInst[i].second);
			}
			MDForInst.clear();
		}
	}
}

/*Strip the wrapper of MDNodes. e.g. !20 = metadata!{ metadata !21}
 * or !20 = metadata !{null}
 * I think, at least for gvar, there are only two levels: first node
 * specify the node that has all the gvar nodes.
 * And now we only deal with gvar in this function
 */
MDNode* MyMetaDataInfo::stripWrapperMDNodes(MDNode* node) {
	int numOps = node->getNumOperands();
	if (numOps == 0) {
		return NULL;
	}
	if (numOps > 1) {
		return node;
	}

	MDNode* gvarsHolder = (MDNode*) (node->getOperand(0));
	//Check if it is just zero
	Value* zeroValue = gvarsHolder->getOperand(0);
	if (dyn_cast_or_null<MDNode>(zeroValue)) {
		//the only metadata node
		return gvarsHolder;
	}
	return NULL;
}

//Recursively get the type string from the MDNode
string MyMetaDataInfo::getTypeFromMD(MDNode* mdnode) {
	int dwTag = getDWTag(mdnode);
	switch (dwTag) {
	case dwarf::DW_TAG_base_type: {
		string typeName = getStringField(mdnode, 2);
		return typeName;
		break;
	}
	case dwarf::DW_TAG_typedef: {
		string typeName = getStringField(mdnode, 2);
		return typeName;
		break;
	}
	case dwarf::DW_TAG_pointer_type: {
		MDNode* derivedFrom = (MDNode*) (mdnode->getOperand(9));
		string typeName = getTypeFromMD(derivedFrom) + " POINTER";
		if (typeName.find("char") != string::npos) {
			typeName = "STRING";
		}
		return typeName;
		break;
	}
	case dwarf::DW_TAG_const_type: {
		MDNode* derivedFrom = (MDNode*) (mdnode->getOperand(9));
		return getTypeFromMD(derivedFrom);
		break;
	}
	case dwarf::DW_TAG_member: {
		MDNode* derivedFrom = (MDNode*) (mdnode->getOperand(9));
		return getTypeFromMD(derivedFrom);
		break;
	}
	default: {
		return "Not Determined DW_TAG type";
		break;
	}
	}
	return NULL;
}

MDNode* MyMetaDataInfo::getStructureMDNodeFromName(string sname) {
	sname = stripStructNamePrefix(sname);

	if (_lStructureMDs == NULL) {
		errs() << "[Notice] the structure MD is NULL\n";
		return NULL;
	}

	list<MDNode*>::iterator iter = _lStructureMDs->begin(), end = _lStructureMDs->end();
	for (; iter != end; iter++) {
		MDNode* mdnode = *iter;
		string mdsname = getStringField(mdnode, 2);

		if (sname.compare(mdsname) == 0) {
			errs() << "::::::mdsname " << mdsname << "\n";
			return mdnode;
		}
	}

	//Cannot find, look for the unamed structure MDNode
	if (_mapUnamedStructMDN.count(sname) == 1) {
		return _mapUnamedStructMDN[sname];
	}

	return NULL;
}

/* Create a metadata node in _mdnMap*/
void MyMetaDataInfo::createMetadataSlot(MDNode *N) {
	if (!N->isFunctionLocal()) {
		mdn_iterator I = _mapMDN.find(N);
		if (I != _mapMDN.end()) {
			return;
		}

		unsigned DestSlot = _mdnNext++;
		_mapMDN[N] = DestSlot;
	}

	for (unsigned i = 0, e = N->getNumOperands(); i != e; ++i) {
		MDNode *Op = dyn_cast_or_null<MDNode>(N->getOperand(i));
		if (Op) {
			createMetadataSlot(Op);
		}
	}
}

list<MDNode*>* MyMetaDataInfo::getAllStructureMD() {
	list<MDNode*>* res = new list<MDNode*>();
	MDNode* mdnode;
	mdn_iterator I = _mapMDN.begin(), E = _mapMDN.end();
	for (; I != E; I++) {
		mdnode = I->first;

		if (getDWTag(mdnode) == dwarf::DW_TAG_structure_type) {
			res->push_back(mdnode);

		} else if (getDWTag(mdnode) == dwarf::DW_TAG_class_type) {
			res->push_back(mdnode);

		} else if (getDWTag(mdnode) == dwarf::DW_TAG_typedef) {
			//When it is typedef node, it still can be structure
			//for example, core_server_config in httpd
			//at the same time, the one we derived from may not
			//have name. So we put these nodes to a map for later use.
			MDNode* derivedFrom = (MDNode*) (mdnode->getOperand(9));
			if (getDWTag(derivedFrom) == dwarf::DW_TAG_structure_type) {
				res->push_back(derivedFrom);
				string typeName = getStringField(derivedFrom, 2);
				if (typeName.empty()) {
					//We are going to put the typename string in the
					//typedef node to the map, but corresponds it to
					//the derived MDNode.
					typeName = getStringField(mdnode, 2);
					_mapUnamedStructMDN[typeName] = derivedFrom;
				}
			}
		}
	}
	if (res->size() == 0) {
		delete res;
		return NULL;
	}
	return res;
}

StringRef MyMetaDataInfo::getStringField(MDNode* node, unsigned field) {
	MDString* MDS = dyn_cast_or_null<MDString>(node->getOperand(field));
	return MDS->getString();
}

/* Processing a DW_TAG_compile_unit metadata node
 * Currently it only deals with global variables in
 * the node. May extend with others if needed
 */
void MyMetaDataInfo::processCompileUnitMDNode(MDNode* cunitNode) {
	if (getDWTag(cunitNode) != dwarf::DW_TAG_compile_unit) {
		errs() << "Error: not compile unit MDNode\n";
		return;
	}
	//The last operand is the list of global variables
	MDNode* gvarMDNode = (MDNode*) (cunitNode->getOperand(13));
	MDNode* gvarListNode = stripWrapperMDNodes(gvarMDNode);
	if (gvarListNode) {
		int numGvars = gvarListNode->getNumOperands();
		for (int i = 0; i < numGvars; i++) {
			processGVMDNode((MDNode*) (gvarListNode->getOperand(i)));
		}
//		errs()<<"Wahaha, so many gvars!\n";
	}
}

/* Processing a DW_TAG_variable (global variable)
 * Currently it only deals with type
 */
void MyMetaDataInfo::processGVMDNode(MDNode* node) {
	if (getDWTag(node) != dwarf::DW_TAG_variable) {
		errs() << "Error: not global variable MDNode\n";
		return;
	}

	int numOps = node->getNumOperands();
	if (numOps != 12) {
		errs() << "Error, global variable MDNode length not equal to 12\n";
		return;
	}
	Value* gvarValue = node->getOperand(11); //The Value* for the global variable
	MDNode* typeMDNode = (MDNode*) (node->getOperand(8));
	/*For debug purpose only!!*/
//	StringRef varName = gvarValue->getName();
//	if(!varName.equals("tunable_max_clients")){
//		return;
//	}
	/*debugging part finished*/

//	errs()<<"~~~~~~~~~~~~~processing metadata of global variable "<<gvarValue->getName()<<" ~~~~~~~~~~~~~~~\n";
	//TODO(done): Maybe get this part (processing typeMDNode) out for another function
	//processType(Value* var, MDNode* typeMDNode). Let's consider later
	processTypeMD(gvarValue, typeMDNode, "");
}

void MyMetaDataInfo::processTypeMD(Value* gvarValue, MDNode* typeMDNode, string curCStr) {

	int dwtag = getDWTag(typeMDNode);
	switch (dwtag) {
	case dwarf::DW_TAG_base_type: {
//			Value* typeNameVal = typeMDNode->getOperand(2);
//			MDString* MDS = dyn_cast_or_null<MDString>(typeMDNode->getOperand(2));
		StringRef typeName = getStringField(typeMDNode, 2);
		if (typeName.equals("char")) {
			if (curCStr.find("POINTER") != string::npos) {
				typeName = "STRING";
			}
		} else {
			typeName = curCStr + typeName.str();
		}
//			errs()<<"Got type name: "<<typeName.str().c_str()<<"\n";
		_mapGVarType[gvarValue] = typeName.str();
		break;
	}

	case dwarf::DW_TAG_pointer_type: {
		MDNode* derivedFrom = (MDNode*) (typeMDNode->getOperand(9));
		string typeName = curCStr + "POINTER";
		processTypeMD(gvarValue, derivedFrom, typeName);
		/*
		 int basedwtag = getDWTag(derivedFrom);
		 if(basedwtag != dwarf::DW_TAG_base_type){
		 errs()<<"[NOT IMPLEMENTED] seems pointer to structure, or pointer to pointer.\n";
		 break;
		 }

		 StringRef typeName = getStringField(derivedFrom, 2);
		 StringRef finalTypeName = typeName.str()+"POINTER";
		 if(typeName.equals("char")){
		 finalTypeName = "STRING";
		 }
		 errs()<<"Got type name: "<<finalTypeName.str().c_str()<<"\n";
		 _mapGVarType[gvarValue] = finalTypeName;
		 */
		break;
	}
	case dwarf::DW_TAG_const_type: {
		MDNode* derivedFrom = (MDNode*) (typeMDNode->getOperand(9));
		string typeName = curCStr + "CONST";
		processTypeMD(gvarValue, derivedFrom, typeName);
		break;
	}
	default: {
		//TODO: deal with this later (to add more dwarfs.
		//Now comment the output
		/*			errs()<<"Error: Unknown DW Tag in type description\n";
		 StringRef typeName = curCStr+" UNKNOWN further";
		 errs()<<"Got type name: "<<typeName.str().c_str()<<"\n";
		 */
		break;
	}
	}
}

//I want to get the metadata nodes of all the structures
// (storing the exact type of each member).
void MyMetaDataInfo::gatherAllMDNodes() {
	Module::named_metadata_iterator I = _module->named_metadata_begin();
	Module::named_metadata_iterator	E = _module->named_metadata_end();

	for (; I != E; ++I) {
		NamedMDNode *NMD = I;
		for (unsigned i = 0, e = NMD->getNumOperands(); i != e; ++i) {
			createMetadataSlot(NMD->getOperand(i));
		}
	}

	for (Module::iterator mi = _module->begin(); mi != _module->end(); mi++) {
		getAllMDNFunc(*mi);
	}

	errs() << "[info] Got number of MDNodes: " << _mapMDN.size() << "\n";
}

string MyMetaDataInfo::getGlobalVariableTypeName(GlobalVariable* gv) {
	string typeName = "";

	if (_mapGVarType.count(gv) == 0) { //here it might be the structure
		errs() << "[warn] didn't find this global variable in mapGvarType\n";

	} else {
		typeName = _mapGVarType[gv];
		errs() << "[TYPE] " << typeName.c_str() << "\n";
		//errs()<<"Getting from _mapGVarType using value "<<gvar<<"\n";
	}

	return typeName;
}

////Check type from MetaNode, which stores more about the debug information
////according to llvm manual
//Constraints* Misconfig::checkGVMetaType(GlobalVariable* v){
//	ArrayRef<Value*> arr(v);
//
//	//copy form Metadata.cpp:195, getMDNode()
//	//seems doesn't work
//	/*
//	LLVMContextImpl* pImpl = v->getContext().pImpl;
//	FoldingSetNodeID ID;
//	ID.AddPointer(v);
//	MDNode* mdNode = pImpl->MDNodeSet.FindNodeOrInsertPos(ID, InsertPoint);
//	if(mdNode){
//		errs()<<"good to find the mdNode\n";
//	}*/
//	/* Cannot find either
//	Constraints* newCons = new Constraints();
//	MDNode* mdNode = MDNode::getIfExists(v->getContext(), arr);
//	if(mdNode){
//		errs()<<"Good to find the mdnode\n";
//	}*/
////	MDNode* dbgNode = v->getMetadata(LLVMContext::MD_dbg);
//	ValueSymbolTable& vst = this->_module->getValueSymbolTable();
////	vst.dump();
////	ValueSymbolTable::iterator viter = vst.begin(), vend = vst.end();
////	for(;viter!=vend;viter++){
////
////	}
//	return NULL;
//}
