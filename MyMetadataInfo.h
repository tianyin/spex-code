/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This is the main analysis part of specification inference            */
/*                                                                      */
/* Author: Jiaqi Zhang			                                        */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#ifndef MYMETADATAINFO_H_
#define MYMETADATAINFO_H_

#include "llvm/Analysis/DebugInfo.h"
#include "llvm/Metadata.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/Module.h"
#include "llvm/DerivedTypes.h"
#include "llvm/Instructions.h"
#include "llvm/Constants.h"

#include "Utils.h"
#include "Common.h"

using namespace std;
using namespace llvm;

typedef DenseMap<MDNode*, unsigned>::iterator mdn_iterator;

struct MyMetaDataInfo {

	list<MDNode*>* _lStructureMDs; //list of the MDNodes of DW_TAG_structure_type

	DenseMap<MDNode*, unsigned> _mapMDN; //Map for MDNodes. copied from AsmWriter.cpp

	unsigned _mdnNext; //current count in mdnMap

	//This is used for the structure types that don't have name, because only
	//their typedef derived metadata is used with a name. e.g. "core_server_config"
	map<string, MDNode*> _mapUnamedStructMDN;

	map<Value*, string> _mapGVarType; //The map of global variable and its type (from metadata)
	map<Value*, string> _mapLVarType;

	//////////////////////////////////////////////////////////////////////////////////////////

	MyMetaDataInfo(Module* m) {
		_mdnNext = 0;
		_module = m;
	}

	void clean() {
		_mapLVarType.clear();
		_mapGVarType.clear();

		_mapMDN.clear();
		_mapUnamedStructMDN.clear();
		_lStructureMDs->clear();
	}

	void _initialize() {
		gatherAllMDNodes();

		_lStructureMDs = getAllStructureMD();

		_initializeMetadata(); //Now I just still keep this to make sure there is no regression, although
							   //maybe it is not useful now since we already got gatherAllMDNodes();
	}

//	Constraints* checkGVMetaType(GlobalVariable*);

	void processGVMDNode(MDNode*);

	int getDWTag(MDNode*);

	void processTypeMD(Value*, MDNode*, string);

	void processCompileUnitMDNode(MDNode*);

	MDNode* stripWrapperMDNodes(MDNode*);

	void gatherAllMDNodes();

	StringRef getStringField(MDNode*, unsigned);

	MDNode* getStructureMDNodeFromName(string);

	list<MDNode*>* getAllStructureMD();

	void createMetadataSlot(MDNode*);

	void getAllMDNFunc(Function&);

	string getTypeFromMD(MDNode* mdnode);

	string getGlobalVariableTypeName(GlobalVariable* gv);

private:
	Module *_module;

	void _initializeMetadata();
};

#endif /* MYMETADATAINFO_H_ */
