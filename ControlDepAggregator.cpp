/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Tianyin Xu, Jiaqi Zhang                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#include "ControlDepAggregator.h"

using namespace std;

//to find the conflicts between the two PerBlockControlDepInfo,
//i.e., A->B && !A->B, and fill B into the mustNotParams set
void ControlDepAggregator::checkConflicts(
		PerBlockControlDepInfo* pbdi_t,
		PerBlockControlDepInfo* pbdi_f) {

	if(pbdi_t && pbdi_f) {
		set<string>* confpar1 = &(pbdi_t->domedParameters);
		set<string>* confpar2 = &(pbdi_f->domedParameters);

		set<string> intersection;
		setIntersection(confpar1, confpar2, &intersection);

		copySet(&mustNotParams, &intersection);
	}
}

//void ControlDepAggregator::preprocess() {
//
////	//1. transfer the functions to configuration parameters
////	set<string> refSet;
////	map<string, PerBlockControlDepInfo*>::iterator mIter = cDepInfoMap.begin();
////	map<string, PerBlockControlDepInfo*>::iterator mEnd = cDepInfoMap.end();
////	for(; mIter != mEnd; mIter++) {
////		refSet.insert(mIter->first);
////		PerBlockControlDepInfo* cdepi = mIter->second;
////
////		set<Function*>* callees = &(cdepi->domedCallees);
////		set<Function*>::iterator cIter = callees->begin(), cEnd = callees->end();
////		for(; cIter != cEnd; cIter++) {
////			set<string>* tmp = f2cMap->operator [](*cIter);
////			copySet(&(cdepi->domedParameters), tmp);
////		}
////	}
//
//	set<string> refSet;
//	make_key_set(cDepInfoMap, refSet);
//
//	//2. fill the must-not-list, i.e., A->B && !A->B
//	set<string> checkedSet;
//	set<string>::iterator refIter = refSet.begin(), refEnd = refSet.end();
//	for(; refIter != refEnd; refIter++) {
//		string ref = *refIter;
//		checkedSet.insert(ref);
//
//		string peerRef = ref.find("!") == string::npos ? "!" + ref : ref.replace(0,1,"");
//		if(refSet.count(peerRef) != 0 &&          //we have the peer, i.e., A && !A
//				checkedSet.count(peerRef) == 0) { //we didn't check the peer
//
//			set<string>* confpar1 = &(cDepInfoMap[ref]->domedParameters);
//			set<string>* confpar2 = &(cDepInfoMap[peerRef]->domedParameters);
//
//			setIntersection(confpar1, confpar2, &mustNotParams);
//		}
//	}
//}

/**
 * Let me describe how to get the results, i.e., the parameters depends on the current parameter.
 *
 * Basically, all the information is stored in the ControlDepInfo structure, see the comments in common.h
 *
 * O.K., having the information maintained in ControlDepInfo,
 *
 * 1. get all the potential parameters according to "dominatedFunctionCalls" & "dominatedParameters"
 * 2. delete those ones in the "conflictParams"
 * 3. check whether the usage of each parameter is self-contained within the structure, see the comments in common.h
 * 4. the rests are considered to be parameters with real dependencies
 */
void ControlDepAggregator::getRealDepParams(
		ConfUsageInfo* confUsages,
		Func2ConfUsageMap* f2cMap,
		MiniCallGraph* mcg,
		set<string>* results) {

	//0. PREPARATION
	//0.1 get the whole function coverage set:
	set<Function*> funcCoverage;
	mcg->BFS(&domedCallees, &funcCoverage);
//	copySet(&funcCoverage,  &domingFunctions);
//	copySet(&funcCoverage, &domedCallees);

//	printFunctionSet(&funcCoverage);

	//0.2
	//so after this, we can only focus on those parameters not in the backlist
	excludeSet(&domedParameters, &mustNotParams);

	//1. get all the potential parameters & related functions
	map<string, set<Function*>*> potentials;

	//1.1. iterate all the configuration parameters
	set<string>::iterator pIter = domedParameters.begin(), pEnd = domedParameters.end();
	for(; pIter != pEnd; pIter++) {
		potentials[*pIter] = new set<Function*>();
	}

	//1.2 iterate all the functions
//	set<Function*>::iterator calleeIter = domedCallees.begin(), calleeEnd = domedCallees.end();
	set<Function*>::iterator calleeIter = funcCoverage.begin(), calleeEnd = funcCoverage.end();
	for(; calleeIter != calleeEnd; calleeIter++) {
		set<string>* tmp = f2cMap->operator [](*calleeIter);
		set<string>::iterator tmpIter = tmp->begin(), tmpEnd = tmp->end();

		for(; tmpIter != tmpEnd; tmpIter++) {
			if(domedParameters.count(*tmpIter) != 0) {
				potentials[*tmpIter]->insert(*calleeIter);
			}
		}
	}

//	if(potentials.count("write_enable") != 0) {
//		errs() << "write_enable" << "\n";
//		printFunctionSet(potentials.operator []("write_enable"));
//	}

	//2. recursively check the usage of all the functions
	map<string, set<Function*>*>::iterator uIter = potentials.begin(), uEnd = potentials.end();
	for(; uIter != uEnd; uIter++) {
		string param = uIter->first;
		set<Function*>* fset = uIter->second;

		if(fset->size() != 0) {
			bool x = isSelfContained(param, fset, &funcCoverage, &domingBB, f2cMap, mcg);
			if(!x) {
				//wow!!! record!!!
//				results->insert(param);
				domedParameters.erase(param);
			}
		}
	}

	errs() << "---------------after step 2---------------\n";
//	printSet(&domedParameters);
	errs() << "------------\n";
//	printFunctionSet(&domingFunctions);

	//3. check the usage of the remaining configuration parameters.
	set<string>::iterator dpIter = domedParameters.begin(), dpEnd = domedParameters.end();
	for(; dpIter != dpEnd; dpIter++) {
		string param = *dpIter;
		set<Value*> usages;
		confUsages->getUsageSet(param, &usages);

		set<Instruction*> instUsage;

		set<Value*>::iterator cuIter = usages.begin(), cuEnd = usages.end();
		for(; cuIter != cuEnd; cuIter++) {
			Value* usage = *cuIter;

			if(isa<Instruction>(usage)) {
				Instruction* ins = dyn_cast<Instruction>(usage);
				instUsage.insert(ins);

			} else {
//				set<Function*> fset;
//				get1stStopInstUsageInFunction(usage, &fset);
//				usageInFunctionSet.insert(fset.begin(), fset.end());

				set<Instruction*> iset;
				get1stStopInstUsage(usage, &iset);
				instUsage.insert(iset.begin(), iset.end());
			}
		}

//		printFunctionSet(&usageInFunctionSet);
		errs() << param << " inst usage size: " << instUsage.size() << "\n";

		//suppose it's a true dep and then examine
		bool isContained = true;
		set<Instruction*>::iterator iIter = instUsage.begin(), iEnd = instUsage.end();
		for(; iIter != iEnd; iIter++) {
			Instruction* ui = *iIter;
			BasicBlock* ub = ui->getParent();
			Function* uf = ub->getParent();

			if(uf->getNameStr().compare("install_str_setting") == 0 ||
					uf->getNameStr().compare("tunables_load_defaults") == 0) {
				continue;
			}

//			errs() << *ui << " | " << uf->getNameStr() << "\n";

			if(funcCoverage.count(uf) == 0 && domingBB.count(ub) == 0) {
				isContained = false;
				break;
			}
		}

		if(isContained) {
			//ok, we are safe finally
			results->insert(param);
		}
	}
}


bool ControlDepAggregator::isSelfContained(string param,
		set<Function*>* fset,
		set<Function*>* coverageSet, set<BasicBlock*>* bbSet,
		Func2ConfUsageMap* f2cMap, MiniCallGraph* ccg) {

	set<Function*> checked;

	set<Function*>::iterator funcIter = fset->begin(), funcEnd = fset->end();
	for(; funcIter != funcEnd; funcIter++) {
		Function* currFunc = *funcIter;

//		errs() << currFunc->getNameStr() << "\n";

		if(!isRecursiveUsageContained(param, currFunc, coverageSet, bbSet, f2cMap, ccg, &checked)) {
			return false;
		}
	}
	return true;
}

bool ControlDepAggregator::isRecursiveUsageContained(string param,
		Function* F,
		set<Function*>* coverageSet, set<BasicBlock*>* bbSet,
		Func2ConfUsageMap* f2cMap, MiniCallGraph* ccg,
		set<Function*>* cache) {

	if(cache->count(F) != 0) {
		//this means it is checked before, and the checking was successful
		//(otherwise, it's returned already)
		return true;
	} else {
		cache->insert(F);
	}

	set<string>* confset = f2cMap->operator [](F);
	if(confset->count(param) == 0) {
		//that means this function is not relevant
		return true;
	}

	//1. make sure it's usage is contained
	set<CallInst*> iset;
	getRealCallInstUsage(F, &iset);

	set<CallInst*>::iterator iIter = iset.begin(), iEnd = iset.end();
	for(; iIter != iEnd; iIter++) {
		BasicBlock* tmpBB = (*iIter)->getParent();
		Function* tmpFunction = tmpBB->getParent();

		if(bbSet->count(tmpBB) == 0 && coverageSet->count(tmpFunction) == 0) {
			return false;
		}
	}

	//2. make sure its callee's usage it contained
	MiniGraphNode* ccgNode = ccg->_miniCallGraph->operator [](F);
	set<MiniGraphNode*>* callees = &(ccgNode->_callees);

	set<MiniGraphNode*>::iterator calleeIter = callees->begin(), calleeEnd = callees->end();
	for(; calleeIter != calleeEnd; calleeIter++) {
		Function* currFunction = (*calleeIter)->_function;
		if(isRecursiveUsageContained(param, currFunction,
				coverageSet, bbSet,
				f2cMap, ccg, cache) == false) {
			return false;
		}
	}

	return true;
}
