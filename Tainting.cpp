/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#include "Tainting.h"

//////////////////////////////////////////////////////////////////////////
//DFNode
//////////////////////////////////////////////////////////////////////////

bool DFNode::operator==(DFNode& rhs) {
	if(this->_field < 0){
		return (this->_value == rhs._value);
	}

	return (this->_value == rhs._value) &&
			(this->_field == rhs._field);
}

bool DFNode::equals(DFNode* rhs) {

	if(this->_field < 0) {
		return (this->_value == rhs->_value);
	}

	return (this->_value == rhs->_value) &&
			(this->_field == rhs->_field);
}

list<DFNode*>::iterator findDFNodeList(list<DFNode*>* l, DFNode* n) {

	list<DFNode*>::iterator iter = l->begin(), end = l->end();

	for (; iter != end; iter++) {
		if ((*iter)->equals(n)) {
			return iter;
		}
	}

	return end;
}

/*
 * return the DFNode list (tainted set) set that contains all the tainted lists that contain a DFNode.
 * Note that there definitely can be the situation where multiple taintes lists contain one common
 * value.
 * return null if not found
 */
set<InterestNodes*>* findINodeList(InterestNodes** arrINodes, DFNode* n, int arrSize) {
	set<InterestNodes*>* res = new set<InterestNodes*>();

	for (int i = 0; i < arrSize; i++) {
		InterestNodes* tmp = arrINodes[i];
		if (findDFNodeList(tmp, n) != tmp->end()) {
			res->insert(tmp);
//			errs() << "--- " << i << "\n";
		}
	}

	if (res->size() == 0) {
		delete res;
		return NULL;
	}

	return res;
}

/* Put the DFNode to the set of tainted lists.
 */
void pushToTaintedListSet(set<InterestNodes*>* setTaintedLists, DFNode* n) {
	set<InterestNodes*>::iterator iter = setTaintedLists->begin(), end = setTaintedLists->end();
	for (; iter != end; iter++) {
		(*iter)->push_back(n);
	}
}

//void pushToTaintedSetSet(set<InterestTaintedSet*>* taintedSetSet, Value* v) {
//	set<InterestTaintedSet*>::iterator iter = taintedSetSet->begin(), end = taintedSetSet->end();
//	for (; iter != end; iter++) {
//		(*iter)->insert(v);
//	}
//}

void printInterestNodes(InterestNodes* iNodeList) {
	InterestNodes::iterator iter = iNodeList->begin(), end = iNodeList->end();
	for(; iter != end; iter++) {
		(*iter)->println();
	}
}

bool IsDFNodeInList(list<DFNode*>* l, DFNode* n) {

	if (findDFNodeList(l, n) != l->end()){
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////
//ParamAffectingMap
//////////////////////////////////////////////////////////////////////////

ParamAffectingMap::~ParamAffectingMap() {
	map<string, map<unsigned, set<unsigned>*>* >::iterator afmIter = _affectedmap.begin(),
			afmEnd = _affectedmap.end();

	for(; afmIter != afmEnd; afmIter++) {
		map<unsigned, set<unsigned>*>* amap = (*afmIter).second;

		map<unsigned, set<unsigned>*>::iterator asetIter = amap->begin(), asetEnd = amap->end();
		for(; asetIter != asetEnd; asetIter++) {
			delete (*asetIter).second;
		}
	}
}

void ParamAffectingMap::insert(string func, unsigned src, unsigned dst) {
	if(_affectedmap.count(func)) {
		map<unsigned, set<unsigned>*>* amap = _affectedmap[func];

		if(amap->count(src)) {
			set<unsigned>* aset = amap->operator [](src);
			aset->insert(dst);

		} else {
			set<unsigned>* aset = new set<unsigned>();
			aset->insert(dst);
			amap->operator [](src) = aset;
		}

	} else {
		//even the func does not exist
		map<unsigned, set<unsigned>*>* amap = new map<unsigned, set<unsigned>*>();
		set<unsigned>* aset = new set<unsigned>();
		aset->insert(dst);
		amap->operator [](src) = aset;
		_affectedmap[func] = amap;
	}
}

void ParamAffectingMap::insert(string func, unsigned src, set<unsigned>* dsts) {
	if(_affectedmap.count(func)) {
		map<unsigned, set<unsigned>*>* amap = _affectedmap[func];

		if(amap->count(src)) {
			set<unsigned>* aset = amap->operator [](src);
			copySet(aset, dsts);

		} else {
			set<unsigned>* aset = new set<unsigned>();
			copySet(aset, dsts);
			amap->operator [](src) = aset;
		}

	} else {
		//even the func does not exist
		map<unsigned, set<unsigned>*>* amap = new map<unsigned, set<unsigned>*>();
		set<unsigned>* aset = new set<unsigned>();
		copySet(aset, dsts);
		amap->operator [](src) = aset;
		_affectedmap[func] = amap;
	}
}

set<unsigned>* ParamAffectingMap::getAfftectedSet(string func, unsigned src) {

	if(_affectedmap.count(func)) {
		map<unsigned, set<unsigned>*>* amap = _affectedmap[func];

		if(amap->count(src)) {//////////////////////////////////////////////////////////////////////////
			//ParamAffectingMap
			//////////////////////////////////////////////////////////////////////////
			set<unsigned>* aset = amap->operator [](src);
			return aset;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
