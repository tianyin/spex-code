/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#ifndef _DATAFLOW_H_
#define _DATAFLOW_H_

#include <list>
#include <map>
#include <set>

#include "llvm/Value.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"

#include "Utils.h"

using namespace std;
using namespace llvm;

/**
 * There are two important building block of the inference engine that use the tainting techniques.
 * 1. to decide the affected argument inside a function, e.g., I know one argument of a function call
 *    like func(A, B), A might be assigned some value by B inside the function.
 * This part of implementation is in InferenceEngine.cpp. It's not well saparated
 *
 * 2. to decide whether a value v inside a function comes from one of the function's argument.
 *    It is the implementation of isFromFunctionArgument()
 */
//bool isFromFunctionArgument(Value* v, set<unsigned>* argsrc);

class DFNode{
	friend list<DFNode*>::iterator findDFNodeList(list<DFNode*>*, DFNode*);

public:
//	DFNode(Value* value, int field=-1) :
//		_value(value), _field(field) {}

	DFNode(Value* value) {
		_value = value;
		_field = -1;
	}

	void setField(int field) {
		_field = field;
	}

	bool hasField() {
		return _field>=0;
	}

	bool operator==(DFNode& rhs);

	bool equals(DFNode* rhs);

	void println() {
		errs() << *_value << " | " << _field << "\n";
	}

private:
	Value *_value;
	int _field;
};

typedef list<DFNode*> InterestNodes;

//typedef set<Value*> InterestTaintedSet;

void printInterestNodes(InterestNodes* iNodeList);

list<DFNode*>::iterator findDFNodeList(list<DFNode*>*, DFNode*);

set<InterestNodes*>* findINodeList(InterestNodes** arrINodes, DFNode* n, int arrSize);

bool IsDFNodeInList(list<DFNode*>*, DFNode*);

void pushToTaintedListSet(set<InterestNodes*>* setTaintedLists, DFNode* n);

//Define how the function arguments affect each other, or affect return value
//typedef pair<int, int> AffectPair;
//typedef list<AffectPair* > Affvoid pushToTaintedListSet(set<InterestNodes*>* setTaintedLists, DFNode* n);* vectingList;

//typedef map< pair<string, int>, list<int>* > AffectingMap;

class ArgMap {
public:

	ArgMap(unsigned numOfArg) {

		for(unsigned i=0; i<numOfArg; i++) {
			_argmap[i] = new set<Value*>();
		}
	}

	~ArgMap() {
		map<unsigned, set<Value*>* >::iterator mIter = _argmap.begin(), mEnd = _argmap.end();
		for(; mIter != mEnd; mIter++) {
			delete mIter->second;
		}
	}

	set<unsigned>* findCorrespondingTaintedSet(Value* v) {
		set<unsigned>* res = new set<unsigned>();

		map<unsigned, set<Value*>* >::iterator mIter = _argmap.begin(), mEnd = _argmap.end();
		for(; mIter != mEnd; mIter++) {
			set<Value*>* its = (*mIter).second;
			if(its->count(v)) {
				res->insert((*mIter).first);
			}
		}

		if(res->size() == 0) {
			delete res;
			return NULL;

		} else {
			return res;
		}
	}

	void simplePush2TaintedSet(unsigned idx, Value* v) {
		assert(idx < _argmap.size());
		_argmap[idx]->insert(v);
	}

	void push2TaintedSet(unsigned idx, Value* v) {
		simplePush2TaintedSet(idx, v);

		Value* curv = v;
		while(isa<LoadInst>(curv)) {
			curv = dyn_cast<LoadInst>(curv)->getOperand(0);
			simplePush2TaintedSet(idx, curv);
		}
	}

	void simplePush2TaintedSets(set<unsigned>* taintedSets, Value* v) {
		set<unsigned>::iterator iter = taintedSets->begin(), end = taintedSets->end();
		for (; iter != end; iter++) {
			_argmap[(*iter)]->insert(v);
		}
	}

	void push2TaintedSets(set<unsigned>* taintedSets, Value* v) {
		simplePush2TaintedSets(taintedSets, v);

		Value* curv = v;
		while(isa<LoadInst>(curv)) {
			curv = dyn_cast<LoadInst>(curv)->getOperand(0);
			simplePush2TaintedSets(taintedSets, curv);
		}
	}

private:
	map<unsigned, set<Value*>* > _argmap;
};

class ParamAffectingMap {

public:
	~ParamAffectingMap();

	void insert(string func, unsigned src, unsigned dst);
	void insert(string func, unsigned src, set<unsigned>* dsts);

	set<unsigned>* getAfftectedSet(string func, unsigned src);

private:
	map<string, map<unsigned, set<unsigned>*>* > _affectedmap;
};

#endif
