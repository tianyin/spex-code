/* -------------------------------------------------------------------- */
/* OPERA Misconfig project                                              */
/*                                                                      */
/* This file is the main analysis part to infer constraints             */
/*                                                                      */
/* Author: Jiaqi Zhang, Tianyin Xu                                      */
/* Check git repository for history information                         */
/* -------------------------------------------------------------------- */

#ifndef _CONSTRAINTS_H_
#define _CONSTRAINTS_H_

#include "llvm/Value.h"
#include "Utils.h"

#include <list>
#include <set>
#include <iostream>
#include <fstream>
#include "llvm/Support/raw_ostream.h"

using namespace std;
using namespace llvm;

#define PRINT_START_SPEC = "----------Start Spec---------------\n";
#define PRINT_END_SPEC =   "----------End Spec---------------\n";

/**
 * Make sure the output format in dump() or dumpToFile() function,
 * because later we have to be able to do a cleaning job to regulate these
 * specifications (i.e., constraints)
 *
 * The format of each specification is supposed be two lines
 *
 * 1. the 1st line tells the specification type (i.e., SYSCALL, STRUCT, TYPE, etc),
 *    with some detailed information in a bracket
 *
 * 2. the 2nd line gives more detailed information
 */

///*The constraint type we currently know*/
enum ConstraintType {
	DEFAULT,

	//types and options
	TYPE,
	OPTION,
	REFERENCE_VALUE,
	DEFAULT_VALUE,

	//semantics
	SYSCALL,
	STRUCT,

	//dependency
	MIN_MAX_DEPENDENCY,
	IF_DEPENDENCY,

	//currently not used
	NULLPOINTER,
	RECURSIVECALL
};

/**
 * Base class of constraints. Every other specific constraint type
 * is derived from this class
 *
 * [NOTE] In the source code, the terminology "constraint" and "specification"
 * is equalavent. "Specification" is more accurate word, but because we used
 * "constraints" in the beginning of these code. So we are not able to change it.
 */
class Constraint {
	//type of the constraint. mainly how we inferred the option type
//	ConstraintType _ctype;

	//The file where we find this constraint
	string _filename;

	//The line number where we find this constraint
	int _linenum;

//	//tell when the constraint is created, from the funcMapping? or Global tracing? Global Structure tracing?
//	//Local Structure tracing? etc. the higher level the less accuracy because of the "general case problem"
//	int _extractLevel;

public:
	unsigned char SubclassID;   // Subclass identifier (for isa/dyn_cast)

//	enum ConstraintTy { //a more coarse granularity type corresponding to each inherented class
//		GeneralConstraint,
//		SysCallConstraint,
//		TypeConstraint,
//		StructConstraint,
//		RangeConstraint,
//		IfDependencyConstraint,
//		MinMaxDependencyConstraint,
//		OptionConstraint,
//		DefaultValueConstraint
//	};

	Constraint() { /*didn't use initializer because of debugging purpose*/
		SubclassID = DEFAULT;
	}

	virtual ~Constraint() {}

//	void setLevel(int l) {
//		_extractLevel = l;
//	}

	void setFileName(string& filename) {
		_filename = filename;
	}
	void setLineNum(int linenum) {
		_linenum = linenum;
	}
	string getFileName() {
		return _filename;
	}
	int getLineNum() {
		return _linenum;
	}

	virtual bool compareTo(Constraint* c) {
		if(this->SubclassID == c->SubclassID) {
			return true;
		} else {
			return false;
		}
	}

	virtual bool operator==(Constraint* c) {
		return compareTo(c);
	}

	virtual bool operator<(Constraint* c) {
		return compareTo(c);
	}

	virtual void dump() {
		errs() << "[panic] impossible to be here (dump)\n";
		assert(false);
	}

	virtual void dumpToFile(std::ofstream& os) {
		errs() << "[panic] impossible to be here (dump2file)\n";
		assert(false);
	}

};

class TypeConstraint:public Constraint {

public:
	TypeConstraint(string type) : _type(type) {

		this->SubclassID = TYPE;
	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const TypeConstraint *) { return true; }
	static inline bool classof(const Constraint *c) {
		return c->SubclassID == TYPE;
	}

	bool compareTo(Constraint* c) {
		if (isa<TypeConstraint>(c)) {
			TypeConstraint* tc = dyn_cast<TypeConstraint>(c);

			if (this->_type.compare(tc->_type) == 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	void dump() {
		errs() << "Option Type: " << _type << "\n";
	}

	void dumpToFile(std::ofstream& os) {
		os << "----------Start Spec---------------\n";
		os << "Constraint Type: TYPE\n";
		os << "Option Type: " << _type << "\n";
		os << "----------End Spec---------------\n";
	}

private:
	string _type;
};

class OptionConstraint:public Constraint {

public:
	OptionConstraint(string funcName, string arg, string opty) :
		_funcName(funcName),
		_constantArg(arg),
		_optionType(opty) {

		this->SubclassID = OPTION;
	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const OptionConstraint *) { return true; }
	static inline bool classof(const Constraint *c) {
		return c->SubclassID == OPTION;
	}

	bool compareTo(Constraint* c) {
		if (isa<OptionConstraint>(c)) {
			OptionConstraint* oc = dyn_cast<OptionConstraint>(c);

			if (this->_funcName.compare(oc->_funcName) == 0 &&
					this->_constantArg.compare(oc->_constantArg) == 0 &&
					this->_optionType.compare(oc->_optionType) == 0) {

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	void dump() {
		errs() << "Constraint Type: " << _optionType << " (" << _funcName << ")\n";
		errs() << "Constant Argument: " << _constantArg << "\n";
	}

	void dumpToFile(std::ofstream& os) {
		os << "----------Start Spec---------------\n";
		os << "Constraint Type: " << _optionType << " (inferred from [" << _funcName << "])\n";
		os << "Constant Value: " << _constantArg << "\n";
		os << "----------End Spec---------------\n";
	}

private:
	string _funcName;
	string _constantArg;
	string _optionType; //e.g., enum, delimiter, or sth sth
};

class SysCallConstraint: public Constraint {

public:
	SysCallConstraint(string funcName, unsigned argPos, string smtc, bool traceFurther = false) :
			_funcName(funcName),
			_argPos(argPos),
			_traceFurther(traceFurther) {

		this->SubclassID = SYSCALL;
		this->_semantic = smtc;
	}

	bool compareTo(Constraint* c) {
		if (isa<SysCallConstraint>(c)) {
			SysCallConstraint* scc = dyn_cast<SysCallConstraint>(c);

			if (this->_funcName.compare(scc->_funcName) == 0 &&
					this->_argPos == scc->_argPos) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const SysCallConstraint *) { return true; }
	static inline bool classof(const Constraint *c) {
		return c->SubclassID == SYSCALL;
	}

	void dump() {
		if(_semantic.compare("IGN") != 0) {
			errs() << "Constraint Type: SYSCALL (" << _funcName << " : " << _argPos << ")\n";
			errs() << "Semantics: " << _semantic << "\n";
		}
	}

	void dumpToFile(std::ofstream& os) {
		if(_semantic.compare("IGN") != 0) {
			os << "----------Start Spec---------------\n";
			os << "Constraint Type: SYSCALL (" << _funcName << " : " << _argPos << ")\n";
			os << "Semantics: " << _semantic << "\n";
			os << "----------End Spec---------------\n";
		}
	}

private:
	string _funcName;
	unsigned _argPos;

	bool _traceFurther; //Specifying whether we stop, or need to track along the returning value
						//	(e.g. atol() in the User directive example
	string _semantic;
};

/* This is the constraint derived from structures such as 
 * addr_in and time_val
 * It would be great if user can provide us with this info,
 * but at least we know these two now:)
 */
class StructConstraint: public Constraint {
public:
	StructConstraint(string structName, unsigned field, string smtc) :
			_structName(structName),
			_field(field),
			_semantic(smtc) {

		SubclassID = STRUCT;
	}

	bool compareTo(Constraint* c) {
		if (isa<StructConstraint>(c)) {
			StructConstraint* sc = dyn_cast<StructConstraint>(c);

			if (this->_structName.compare(sc->_structName) == 0 &&
					this->_field == sc->_field) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const StructConstraint *) { return true; }

	static inline bool classof(const Constraint *c) {
		return c->SubclassID == STRUCT;
	}

	void dump() {
		errs() << "Constraint Type: STRUCT: " << _structName << "\n";
		errs() << "Option Type: " << _semantic << "\n";
	}

	void dumpToFile(std::ofstream& os) {
		os << "----------Start Spec---------------\n";
		os << "Constraint Type: STRUCT (" << _structName << " + " << _field << ")\n";
		os << "Option Type: " << _semantic << "\n";
		os << "----------End Spec---------------\n";
	}

private:
	string _structName;
	unsigned _field;
	string _semantic;
};

class IfDependencyConstraint: public Constraint {

public:
	IfDependencyConstraint(string ded, /*int cref,*/ string ding, string ring) {
//	IfDependencyConstraint(string ding, string ded, string ring) {

		SubclassID = IF_DEPENDENCY;

		_dominatedParameter = ded;
//		_cReference = cref;
		_dominatingParameter = ding;
		_dominatingReference = ring;
	}

//	bool isEmpty() {
//		if(depMap.size() == 0)
//			return true;
//		return false;
//	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const IfDependencyConstraint *) { return true; }
	static inline bool classof(const Constraint *c) {
		return c->SubclassID == IF_DEPENDENCY;
	}

//	void insert(string d, int r) {
//		list<pair<string, int>*>::iterator iter = depMap.begin(), end = depMap.end();
//		for( ; iter != end; iter++) {
//			if((*iter)->first.compare(d) == 0 &&
//					(*iter)->second == r) {
//				return;
//			}
//		}
//		depMap.push_back(new pair<string, int>(d, r));
//	}

	bool compareTo(Constraint* c) {
		if (isa<IfDependencyConstraint>(c)) {
			IfDependencyConstraint* idc = dyn_cast<IfDependencyConstraint>(c);

			if (this->_dominatedParameter.compare(idc->_dominatedParameter) == 0 &&
					this->_dominatingParameter.compare(idc->_dominatingParameter) == 0 &&
//					this->_cReference == idc->_cReference) &&
					this->_dominatingReference.compare(idc->_dominatingReference) == 0) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	void dump() {
		errs() << "----------Start Spec---------------\n";
		errs() << "Constraint Type: IF DEPENDENCY\n";
		errs() << "[" << _dominatedParameter << "] depends on [" << _dominatingParameter << ", "	<< _dominatingReference << "]\n";
		errs() << "----------End Spec---------------\n";
	}

	void dumpToFile(std::ofstream& os) {
		os << "----------Start Spec---------------\n";
		os << "Constraint Type: IF DEPENDENCY\n";
		os << "[" << _dominatedParameter << "] depends on [" << _dominatingParameter << ", "	<< _dominatingReference << "]\n";
		os << "----------End Spec---------------\n";
	}

private:
	string _dominatedParameter;
//	int _cReference;
	string _dominatingParameter;
	string _dominatingReference;
};

class MinMaxDependencyConstraint: public Constraint {

public:

	MinMaxDependencyConstraint(string mindv, string maxdv, CmpInst::Predicate pred) {
		SubclassID = MIN_MAX_DEPENDENCY;

		_minDirective = mindv;
		_maxDirective = maxdv;
		_cmpTy = getComparatorByPredicate(pred);
	}

//	bool isEmpty() {
//		if(depMap.size() == 0)
//			return true;
//		return false;
//	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const MinMaxDependencyConstraint*) { return true; }

	static inline bool classof(const Constraint *c) {
		return c->SubclassID == MIN_MAX_DEPENDENCY;
	}

//	void insert(string d, int r) {
//		list<pair<string, int>*>::iterator iter = depMap.begin(), end = depMap.end();
//		for( ; iter != end; iter++) {
//			if((*iter)->first.compare(d) == 0 &&
//					(*iter)->second == r) {
//				return;
//			}
//		}
//		depMap.push_back(new pair<string, int>(d, r));
//	}

	bool compareTo(Constraint* c) {
		if (isa<MinMaxDependencyConstraint>(c)) {
			MinMaxDependencyConstraint* mmdc = dyn_cast<MinMaxDependencyConstraint>(c);

			if (this->_minDirective.compare(mmdc->_minDirective) == 0 &&
					this->_maxDirective.compare(mmdc->_maxDirective) == 0 &&
					this->_cmpTy.compare(mmdc->_cmpTy) == 0) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	void dump() {
		errs() << "----------Start Spec---------------\n";
		errs() << "Constraint Type: MIN-MAX DEPENDENCY: \n";

		errs() << "[" << _minDirective << "] " << _cmpTy << " [" << _maxDirective << "]\n";
		errs() << "----------End Spec---------------\n";
	}

	void dumpToFile(std::ofstream& os) {
		os << "----------Start Spec---------------\n";
		os << "Constraint Type: MIN-MAX DEPENDENCY: \n";

		os << "[" << _minDirective << "] " << _cmpTy << " [" << _maxDirective << "]\n";
		os << "----------End Spec---------------\n";
	}

private:
	string _minDirective;
	string _maxDirective;
	string _cmpTy;

	string getComparatorByPredicate(CmpInst::Predicate pred) {
		string cmptr;

		switch (pred) {
		case CmpInst::ICMP_EQ:
			cmptr = "==";
			break;
		case CmpInst::ICMP_NE:
			cmptr = "!=";
			break;
		case CmpInst::ICMP_SGE:
		case CmpInst::ICMP_UGE:
			cmptr = ">=";
			break;
		case CmpInst::ICMP_SGT:
		case CmpInst::ICMP_UGT:
			cmptr = ">";
			break;
		case CmpInst::ICMP_SLE:
		case CmpInst::ICMP_ULE:
			cmptr = "<=";
			break;
		case CmpInst::ICMP_SLT:
		case CmpInst::ICMP_ULT:
			cmptr = "<";
			break;
		default:
			errs() << "[error] there is sth else in Min-Max constraints\n";
			cmptr = "[err]";
			break;
		}

		return cmptr;
	}
};

//template<typename T>
class ReferenceConstraint: public Constraint {

public:
	ReferenceConstraint(string compareValue) : _compareValue(compareValue) {

		SubclassID = REFERENCE_VALUE;
	}

	bool compareTo(Constraint* c) {
		if (isa<ReferenceConstraint>(c)) {
			ReferenceConstraint* rc = dyn_cast<ReferenceConstraint>(c);

//			if (this->_compareType.compare(rc->_compareType) == 0 &&
			if (this->_compareValue.compare(rc->_compareValue) == 0) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const ReferenceConstraint *) { return true; }
	static inline bool classof(const Constraint *c) {
		return c->SubclassID == REFERENCE_VALUE;
	}

	void dump() {
		errs() << "Constraint Type: RANGE: \n";
//		errs() << "Type: " << _compareType << "\n";
		errs() << "Value: " << _compareValue << "\n";
	}

	void dumpToFile(std::ofstream& os) {
		os << "----------Start Spec---------------\n";
		os << "Constraint Type: RANGE \n";
//		os << "Type: " << _compareType << "\n";
		os << "Value: " << _compareValue << "\n";
		os << "----------End Spec---------------\n";
	}

private:
	string _compareValue;
//	string _compareType;
};

class DefaultValueConstraint: public Constraint {

public:
	DefaultValueConstraint(string defVal) :
			_defaultValue(defVal) {

		SubclassID = DEFAULT_VALUE;
	}

	bool compareTo(Constraint* c) {
		if (isa<DefaultValueConstraint>(c)) {
			DefaultValueConstraint* dvc = dyn_cast<DefaultValueConstraint>(c);

			if (this->_defaultValue.compare(dvc->_defaultValue) == 0) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	/// Methods for support type inquiry through isa, cast, and dyn_cast:
	static inline bool classof(const DefaultValueConstraint*) { return true; }

	static inline bool classof(const Constraint *c) {
		return c->SubclassID == DEFAULT_VALUE;
	}

	void dump() {
		errs() << "Constraint Type: DEFAULT VALUE: \n";
		errs() << "Type: " << _defaultValue << "\n";
	}

	void dumpToFile(std::ofstream& os) {
		os << "----------Start Spec---------------\n";
		os << "Constraint Type: DEFAULT VALUE \n";
		os << "Type: " << _defaultValue << "\n";
		os << "----------End Spec---------------\n";
	}

private:
	string _defaultValue;
};

//typedef set<Constraint*> Constraints

struct Constraints {
	list<Constraint*> constraints;

	Constraints() {}

	Constraints(Constraints* c) {
		list<Constraint*>::iterator iter = c->begin(), end = c->end();
		for(; iter != end; iter++) {
			constraints.push_back(*iter);
		}
	}

//	~Constraints() {
//		constraints.clear();
//	}

	void insert(Constraint* c) {
		list<Constraint*>::iterator iter = constraints.begin(), end = constraints.end();

		for(; iter != end; iter++) {
			if(c->compareTo(*iter)) {
				return;
			}
		}
		constraints.push_back(c);
	}

	list<Constraint*>::iterator begin() {
		return constraints.begin();
	}

	list<Constraint*>::iterator end() {
		return constraints.end();
	}

	int size() {
		return constraints.size();
	}

	list<Constraint*>::iterator find(Constraint* c) {
		list<Constraint*>::iterator iter = constraints.begin(), end = constraints.end();

		for(; iter != end; iter++) {
			if(c->compareTo(*iter)) {
				return iter;
			}
		}
		return iter;
	}

	void printAll() {
		list<Constraint*>::iterator iter = constraints.begin(), end = constraints.end();

		for(; iter != end; iter++) {
			(*iter)->dump();
			errs() << "--\n";
		}
	}

	void insert(Constraints* cons) {
		if(cons) {
			list<Constraint*>::iterator bIter = cons->begin(), bEnd = cons->end();
			for (; bIter != bEnd; bIter++) {
				Constraint* tmp = *bIter;
				if (find(tmp) == end()) {
					constraints.push_back(tmp);
				}
			}
		}
	}

//	void uniqueMerge(Constraints* cons) {
//		uniqueMergeList(&constraints, &(cons->constraints));
//	}

//	void cleanAll() {
//		while(constraints.size() > 0) {
//			Constraint* c = constraints.front();
//			constraints.pop_front();
//				if( c ) {
//					errs() << "--" << c << "\n";
//					c->dump();
//					delete c;
//				}
//		}
//	}
};

void dumpConstraint(Constraint* constraint);

void dumpConstraints(Constraints* constraints);

#endif
