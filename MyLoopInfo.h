/*
 * MyLoopInfo.h
 *
 *  Created on: May 21, 2012
 *      Author: tianyin
 */

#ifndef MYLOOPINFO_H_
#define MYLOOPINFO_H_

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Value.h"

using namespace std;
using namespace llvm;

class MyLoopInfo {
	LoopInfoBase<BasicBlock, Loop> LI;
    friend class LoopBase<BasicBlock, Loop>;
	DominatorTree dt;

public:
    LoopInfoBase<BasicBlock, Loop>& getBase() { return LI; }

	/// iterator/begin/end - The interface to the top-level loops in the current
	/// function.
	///
	typedef LoopInfoBase<BasicBlock, Loop>::iterator iterator;

	inline iterator begin() const {
		return LI.begin();
	}

	inline iterator end() const {
		return LI.end();
	}

	bool empty() const {
		return LI.empty();
	}

	/// getLoopFor - Return the inner most loop that BB lives in.  If a basic
	/// block is in no loop (for example the entry node), null is returned.
	///
	inline Loop *getLoopFor(const BasicBlock *BB) const {
		return LI.getLoopFor(BB);
	}

	/// operator[] - same as getLoopFor...
	///
	inline const Loop *operator[](const BasicBlock *BB) const {
		return LI.getLoopFor(BB);
	}

	/// getLoopDepth - Return the loop nesting level of the specified block.  A
	/// depth of 0 means the block is not inside any loop.
	///
	inline unsigned getLoopDepth(const BasicBlock *BB) const {
		return LI.getLoopDepth(BB);
	}

	// isLoopHeader - True if the block is a loop header node
	inline bool isLoopHeader(BasicBlock *BB) const {
		return LI.isLoopHeader(BB);
	}

	/// runOnFunction - Calculate the natural loop information.
	///
	bool runOnFunction(Function &F) {
		releaseMemory();
		dt.runOnFunction(F);
		LI.Calculate(dt.getBase());    // Update
		return false;
	}

	void releaseMemory() {
		LI.releaseMemory();
	}

	/// removeLoop - This removes the specified top-level loop from this loop info
	/// object.  The loop is not deleted, as it will presumably be inserted into
	/// another loop.
	inline Loop *removeLoop(iterator I) {
		return LI.removeLoop(I);
	}

	/// changeLoopFor - Change the top-level loop that contains BB to the
	/// specified loop.  This should be used by transformations that restructure
	/// the loop hierarchy tree.
	inline void changeLoopFor(BasicBlock *BB, Loop *L) {
		LI.changeLoopFor(BB, L);
	}

	/// changeTopLevelLoop - Replace the specified loop in the top-level loops
	/// list with the indicated loop.
	inline void changeTopLevelLoop(Loop *OldLoop, Loop *NewLoop) {
		LI.changeTopLevelLoop(OldLoop, NewLoop);
	}

	/// addTopLevelLoop - This adds the specified loop to the collection of
	/// top-level loops.
	inline void addTopLevelLoop(Loop *New) {
		LI.addTopLevelLoop(New);
	}

	/// removeBlock - This method completely removes BB from all data structures,
	/// including all of the Loop objects it is nested in and our mapping from
	/// BasicBlocks to loops.
	void removeBlock(BasicBlock *BB) {
		LI.removeBlock(BB);
	}

	/// replacementPreservesLCSSAForm - Returns true if replacing From with To
	/// everywhere is guaranteed to preserve LCSSA form.
	bool replacementPreservesLCSSAForm(Instruction *From, Value *To) {
		// Preserving LCSSA form is only problematic if the replacing value is an
		// instruction.
		Instruction *I = dyn_cast<Instruction>(To);
		if (!I)
			return true;
		// If both instructions are defined in the same basic block then replacement
		// cannot break LCSSA form.
		if (I->getParent() == From->getParent())
			return true;
		// If the instruction is not defined in a loop then it can safely replace
		// anything.
		Loop *ToLoop = getLoopFor(I->getParent());
		if (!ToLoop)
			return true;
		// If the replacing instruction is defined in the same loop as the original
		// instruction, or in a loop that contains it as an inner loop, then using
		// it as a replacement will not break LCSSA form.
		return ToLoop->contains(getLoopFor(From->getParent()));
	}

};

#endif /* MYLOOPINFO_H_ */
